<?php

namespace app\services\events;

use yii\base\Event;

/**
 * Class SmsError
 * @package app\services\events
 */
class SmsError extends Event
{
    const ERROR_PHONE_NOT_FOUND = 207;
    const ERROR_INVALID_API_KEY = 200;
    const ERROR_NO_MONEY = 201;
    const ERROR_LIMIT = 206;

    /**
     * @var int
     */
    public $smsErrorCode;

    /**
     * Возвращает ошибку текстом
     * @return string
     */
    public function getErrorText()
    {
        if(in_array($this->smsErrorCode, array_keys(self::getErrorsLabels())) == false){
            return '';
        }

        return self::getErrorsLabels()[$this->smsErrorCode];
    }

    /**
     * @return array
     */
    public static function getErrorsLabels()
    {
        return [
            self::ERROR_PHONE_NOT_FOUND => 'Телефона не существует',
            self::ERROR_INVALID_API_KEY => 'Неверный API-ключ',
            self::ERROR_NO_MONEY => 'Недостаточно средств',
            self::ERROR_LIMIT => 'Превышен лимит сообщений',
        ];
    }
}