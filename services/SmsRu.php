<?php

namespace app\services;

use app\models\Settings;
use app\models\Sms;
use app\models\users\Users;
use app\services\events\SmsError;
use yii\base\Component;
use yii\base\InvalidConfigException;

/**
 * Class SmsRu
 * @package app\services
 *
 * @property double $balance
 * @property \stdClass $limit
 */
class SmsRu extends Component
{
    const SUCCESS_STATUS = 'OK';
    const EVENT_LOW_BALANCE = 'low_balance';
    const EVENT_ERROR = 'error';

    /**
     * @var string
     */
    public $apiKey;

    /**
     * @var double
     */
    private $_balance;

    /**
     * @var double
     */
    private $_minBalance = 100;

    /**
     * @var array
     */
    private $_limit;

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        if($this->apiKey == null)
            $this->apiKey = Settings::findByKey('sms_ru_api_key_setting')->value;

        parent::init();
    }

    /**
     * Возвращает баланс
     * @return double
     */
    public function getBalance()
    {
        if($this->_balance == null)
        {
            $response = json_decode(file_get_contents("https://sms.ru/my/balance?api_id={$this->apiKey}&json=1"))->balance;
            $this->_balance = $response;
        }

        return $this->_balance;
    }

    /**
     * Возвращает лимит
     * @return \stdClass
     */
    public function getLimit()
    {
        if($this->_limit == null)
        {
            $response = json_decode(file_get_contents("https://sms.ru/my/limit?api_id={$this->apiKey}&json=1"));
            $this->_limit = $response;
        }

        return $this->_limit;
    }

    /**
     * Узнает стоимость СМС на ОДИН номер
     * @param string $number
     * @param string $message
     * @return array
     */
    public function getCost($number, $message)
    {
        $editMessage = str_replace(' ', '+', $message);
        $editNumber = str_replace(' ', '', $number);
        $ch = curl_init("https://sms.ru/sms/cost?api_id={$this->apiKey}&to={$editNumber}&msg={$editMessage}&json=1");


        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $result = json_decode(curl_exec($ch));

        curl_close($ch);

        $number = array_shift(json_decode(json_encode($result->sms), true));

        return $number;
    }

    /**
     * Отправляет сообщение на ОДИН номер
     * @param string $number
     * @param string $message
     * @return array
     */
    public function send($number, $message)
    {
        $cost = $this->getCost($number, $message)['cost'];

        $editMessage = str_replace(' ', '+', $message);
        $editNumber = str_replace(' ', '', $number);
        $ch = curl_init("https://sms.ru/sms/send?api_id={$this->apiKey}&to={$editNumber}&msg={$editMessage}&json=1");


        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $result = json_decode(curl_exec($ch));

        curl_close($ch);

        $response = array_shift(json_decode(json_encode($result->sms), true));

        if($response['status'] != self::SUCCESS_STATUS){
            $this->trigger(self::EVENT_ERROR, new SmsError(['smsErrorCode' => $response['status_code']]));
        }

        $user = Users::find()->where(['telephone' => $number])->one();

        $sms = new Sms([
            'user_id' => $user != null ? $user->id : null,
            'phone_number' => $number,
            'sms_text' => $message,
            'status' => $response['status'] == self::SUCCESS_STATUS ? Sms::STATUS_SUCCESS : Sms::STATUS_ERROR,
            'status_code' => $response['status_code'],
            'cost' => $cost,
        ]);

        $sms->save(false);

        return $response;
    }

    /**
     * Проверяет больше ли баланс минимальной отметки баланса
     * @return boolean
     */
    public function isBalanceNormal()
    {
        $balance = $this->balance;

        return $balance > $this->_minBalance;
    }
}