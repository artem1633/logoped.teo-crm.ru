<?php

namespace app\helpers;




class LabelChek
{

    public static function render($value)
    {
        return $value ?  '<span style="color:green" class="glyphicon glyphicon-ok"></span>' :  '<span style="color:red" class="glyphicon glyphicon-remove"></span>';

    }

    public static function getValue()
    {
        return [0=>'Нет',1=>'Да'];
    }


}