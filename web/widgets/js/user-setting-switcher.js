$('.checkbox-switch').change(function(){
    var checkbox = $(this);
    var url = $(this).data('url');
    var attribute = $(this).attr('name');

    if($(this).is(':checked')){
        var value = 1;
    } else {
        var value = 0;
    }

    var csrfParam = window.yii.getCsrfParam();
    var csrfToken = window.yii.getCsrfToken();

    $.ajax({
        method: 'POST',
        url: url,
        data: {
            'attribute': attribute,
            'value': value,
            csrfParam: csrfToken,
        },
        error: function(response){
            var balanceUpUrl = window.location.origin+'/balance';
            checkbox.parent().find('.checkbox-label').addClass('text-danger');
            checkbox.parent().find('.checkbox-label').text('Ошибка ('+response.responseJSON.status+'): '+response.responseJSON.message+'. ');
            checkbox.parent().find('.checkbox-label').append('<a href="'+balanceUpUrl+'" target="_blank">Пополнить</a>');
            checkbox.parent().find('.checkmark').hide();
        }
    });
});