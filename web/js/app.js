var NotificationManager = function(){

};

NotificationManager.showAlert = function(text, type){
    var alertClass = 'alert alert-'+type;

    var template = $('[data-template="alert"]').clone();
    template.show();
    template.removeAttr('data-template');
    template.attr('class', alertClass);
    template.html(text);

    $('section.content').prepend(template);
};