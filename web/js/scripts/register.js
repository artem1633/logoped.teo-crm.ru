$(document).ready(function(){
    var
        token = "b9be7b7c829ded21856aa2d40e573275ae06f432",
        type  = "ADDRESS",
        city   = $("#registerform-town");

// удаляет районы города
    function removeDistricts(suggestions) {
        return suggestions.filter(function(suggestion) {
            return suggestion.data.city_district === null;
        });
    }

// Ограничиваем область поиска от города до населенного пункта
    city.suggestions({
        token: token,
        type: type,
        hint: false,
        bounds: "city-settlement",
        onSuggestionsFetch: removeDistricts
    });

// Определяем город по IP-адресу
    city.suggestions().getGeoLocation()
        .done(function(locationData) {
            var sgt = {
                value: null,
                date: locationData
            };
            console.log(locationData);
            city.suggestions().setSuggestion(sgt);
            city.val(cityToString(locationData));
        });

    function join(arr /*, separator */) {
        var separator = arguments.length > 1 ? arguments[1] : ", ";
        return arr.filter(function(n){return n}).join(separator);
    }

    function cityToString(address) {
        return join([
            join([address.city_type, address.city], " "),
            join([address.settlement_type, address.settlement], " ")
        ]);
    }
});