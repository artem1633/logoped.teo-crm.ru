<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * Main application asset bundle.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        //'css/all.min.css',
        //'css/bootstrap.min.css',
        //'css/font-awesome.min.css',
        //'css/jquery.fancybox.min.css',
        //'css/owl.carousel.min.css',
        'css/theme-default.css',
        'css/site.css',
        'css/style.css',
        'css/screen.css',

        // Dadata suggestions
        'https://cdn.jsdelivr.net/npm/suggestions-jquery@18.11.1/dist/css/suggestions.min.css',
        //'css/style-92.css',
        //'css/media.css',
        //'css/adminlte.css',
    ];
    public $js = [
        //'js/all.min.js',
        //'js/common.js',
        //'js/main.js',
        //'js/sparklines.js',
        //'js/plugins/jquery/jquery.min.js',
        'js/bootstrap.min.js',
        'js/plugins/jquery/jquery-ui.min.js',
        //'js/plugins/bootstrap/bootstrap.min.js',
        'js/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js',
        'js/plugins.js',
        'js/actions.js',
        'js/app.js',

        // Dadata suggestions
        'https://cdn.jsdelivr.net/npm/suggestions-jquery@18.11.1/dist/js/jquery.suggestions.min.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
