<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * Main application asset bundle.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class RegisterAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'register/css/bootstrap.min.css',
        'register/fonts/roboto.css',
        'register/css/owl.carousel.min.css',
        'register/all.min.css',
        'register/css/style.css',
        'register/css/media.css',

        // Dadata suggestions
        'https://cdn.jsdelivr.net/npm/suggestions-jquery@18.11.1/dist/css/suggestions.min.css',
    ];
    public $js = [
        'register/js/jquery-3.1.0.min.js',
        'register/js/bootstrap.min.js',
        'register/js/owl.carousel.min.js',
        'register/js/all.min.js',
        'register/js/common.js',

        // Dadata suggestions
        'https://cdn.jsdelivr.net/npm/suggestions-jquery@18.11.1/dist/js/jquery.suggestions.min.js',
        'scripts/register.js'
    ];
    public $depends = [
        //'yii\web\YiiAsset',
        //'yii\bootstrap\BootstrapAsset',
    ];
}
