<?php

use yii\db\Migration;

/**
 * Class m190704_001430_alter_free_emails_table
 */
class m190703_181430_alter_free_emails_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropColumn('free_emails', 'recipient');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->addColumn('free_emails', 'recipient', $this->text()->after('id')->comment('Получатели'));
    }
}
