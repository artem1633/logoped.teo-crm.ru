<?php

use yii\db\Migration;

/**
 * Class m190613_170141_apply_letters_images
 */
class m190613_170141_apply_letters_images extends Migration
{

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $bigLetterlist = ['А','Б','В','Г','Д','Е','Ё','Ж','З','И','Й','К','Л','М','Н','О','П','Р',
            'С','Т','У','Ф','Х','Ц','Ч','Ш','Щ','Ъ','Ы','Ь','Э','Ю','Я'];

        $smallLetterlist = ['а','б','в','г','д','е','ё','ж','з','и','й','к','л','м','н','о','п','р',
            'с','т','у','ф','х','ц','ч','ш','щ','ъ','ы','ь','э','ю','я'];


        for($i = 0; $i < count($smallLetterlist); $i++) {
            $symbol = $smallLetterlist[$i];
            if ($symbol == 'ё') {
                $model = \app\models\FixLettersImages::findOne(7);
            } else {
                $model = \app\models\FixLettersImages::find()->where(['symbol' => $symbol])->one();
            }

            if ($model == null) {
                continue;
            }

            $path = "web/Буквы/Маленькие/$symbol/Легко";

            $imagesCount = count(array_diff(scandir($path), ['.', '..']));

            for ($j = 0; $j < $imagesCount; $j++) {
                $easy = new \app\models\FixLettersImagesAttachments([
                    'type' => \app\models\FixLettersImagesAttachments::TYPE_EASY,
                    'fix_letter_image_id' => $model->id,
                    'path' => "Буквы/Маленькие/$symbol/Легко/$j.png",
                ]);
                $easy->save(false);
            }


            $path = "web/Буквы/Маленькие/$symbol/Средне";

            $imagesCount = count(array_diff(scandir($path), ['.', '..']));

            for ($j = 0; $j < $imagesCount; $j++) {
                $easy = new \app\models\FixLettersImagesAttachments([
                    'type' => \app\models\FixLettersImagesAttachments::TYPE_MIDDLE,
                    'fix_letter_image_id' => $model->id,
                    'path' => "Буквы/Маленькие/$symbol/Средне/$j.png",
                ]);
                $easy->save(false);
            }


            $path = "web/Буквы/Маленькие/$symbol/Сложно";

            $imagesCount = count(array_diff(scandir($path), ['.', '..']));

            for ($j = 0; $j < $imagesCount; $j++) {
                $easy = new \app\models\FixLettersImagesAttachments([
                    'type' => \app\models\FixLettersImagesAttachments::TYPE_HARD,
                    'fix_letter_image_id' => $model->id,
                    'path' => "Буквы/Маленькие/$symbol/Сложно/$j.png",
                ]);
                $easy->save(false);
            }
        }

        for($i = 0; $i < count($bigLetterlist); $i++) {
            $symbol = $bigLetterlist[$i];
            if ($symbol == 'Ё') {
                $model = \app\models\FixLettersImages::findOne(40);
            } else if($symbol == 'Е'){
                $model = \app\models\FixLettersImages::findOne(39);
            } else {
                $model = \app\models\FixLettersImages::find()->where(['symbol' => $symbol])->orderBy('id desc')->one();
            }

            if ($model == null) {
                continue;
            }

            $path = "web/Буквы/Большие/$symbol/Легко";

            $dir = array_values(array_diff(scandir($path), ['.', '..']));
            $imagesCount = count($dir);

            for ($j = 0; $j < $imagesCount; $j++) {
                $easy = new \app\models\FixLettersImagesAttachments([
                    'type' => \app\models\FixLettersImagesAttachments::TYPE_EASY,
                    'fix_letter_image_id' => $model->id,
                    'path' => "Буквы/Большие/$symbol/Легко/$dir[$j]",
                ]);
                $easy->save(false);
            }


            $path = "web/Буквы/Большие/$symbol/Средне";

            $dir = array_values(array_diff(scandir($path), ['.', '..']));
            $imagesCount = count($dir);

            for ($j = 0; $j < $imagesCount; $j++) {
                $easy = new \app\models\FixLettersImagesAttachments([
                    'type' => \app\models\FixLettersImagesAttachments::TYPE_MIDDLE,
                    'fix_letter_image_id' => $model->id,
                    'path' => "Буквы/Большие/$symbol/Средне/$dir[$j]",
                ]);
                $easy->save(false);
            }


            $path = "web/Буквы/Большие/$symbol/Сложно";

            $dir = array_values(array_diff(scandir($path), ['.', '..']));
            $imagesCount = count($dir);

            for ($j = 0; $j < $imagesCount; $j++) {
                $easy = new \app\models\FixLettersImagesAttachments([
                    'type' => \app\models\FixLettersImagesAttachments::TYPE_HARD,
                    'fix_letter_image_id' => $model->id,
                    'path' => "Буквы/Большие/$symbol/Сложно/$dir[$j]",
                ]);
                $easy->save(false);
            }
        }

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        \app\models\FixLettersImagesAttachments::deleteAll(['IS NOT', 'id', null]);
    }
}
