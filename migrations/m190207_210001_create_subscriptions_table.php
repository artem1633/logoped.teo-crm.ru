<?php

use yii\db\Migration;

/**
 * Handles the creation of table `subscriptions`.
 */
class m190207_210001_create_subscriptions_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('subscriptions', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->comment('Пользователь'),
            'type' => $this->integer()->comment('Тип подписки'),
            'payed_datetime' => $this->dateTime()->comment('Дата и время последней оплаты')
        ]);
        $this->addCommentOnTable('subscriptions', 'Подписки');

        $this->createIndex(
            'idx-subscriptions-user_id',
            'subscriptions',
            'user_id'
        );

        $this->addForeignKey(
            'fk-subscriptions-user_id',
            'subscriptions',
            'user_id',
            'users',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey(
            'fk-subscriptions-user_id',
            'subscriptions'
        );

        $this->dropIndex(
            'idx-subscriptions-user_id',
            'subscriptions'
        );

        $this->dropTable('subscriptions');
    }
}
