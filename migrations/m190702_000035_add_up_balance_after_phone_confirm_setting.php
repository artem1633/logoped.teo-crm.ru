<?php

use yii\db\Migration;

/**
 * Class m190702_000035_add_up_balance_after_phone_confirm_setting
 */
class m190702_000035_add_up_balance_after_phone_confirm_setting extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->insert('settings', [
            'key' => 'up_balance_after_phone_confirm_amount',
            'label' => 'Бонус при подтверждении телефона',
            'value' => '100',
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
    }
}
