<?php

use yii\db\Migration;

/**
 * Handles adding exception_state to table `free_emails`.
 */
class m190704_205937_add_exception_state_column_to_free_emails_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('free_emails', 'state', $this->integer()->after('subject')->comment('Состояние рассылки'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('free_emails', 'state');
    }
}
