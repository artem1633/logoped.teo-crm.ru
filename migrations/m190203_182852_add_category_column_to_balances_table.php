<?php

use yii\db\Migration;

/**
 * Handles adding category to table `balances`.
 */
class m190203_182852_add_category_column_to_balances_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('balances', 'category', $this->string()->comment('Категория'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('balances', 'category');
    }
}
