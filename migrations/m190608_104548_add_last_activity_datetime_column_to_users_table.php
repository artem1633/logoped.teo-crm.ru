<?php

use yii\db\Migration;

/**
 * Handles adding last_activity_datetime to table `users`.
 */
class m190608_104548_add_last_activity_datetime_column_to_users_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('users', 'last_activity_datetime', $this->dateTime()->comment('Дата и время последней активности'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('users', 'last_activity_datetime');
    }
}
