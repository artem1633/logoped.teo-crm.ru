<?php

use yii\db\Migration;

/**
 * Handles adding saved_datetime to letters table.
 */
class m190209_132246_add_saved_datetime_column_to_letters_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('find_letter', 'saved_datetime', $this->dateTime()->comment('Время последнего сохранения'));
        $this->addColumn('fix_letter', 'saved_datetime', $this->dateTime()->comment('Время последнего сохранения'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('find_letter', 'saved_datetime');
        $this->dropColumn('fix_letter', 'saved_datetime');
    }
}
