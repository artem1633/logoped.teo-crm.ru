<?php

use yii\db\Migration;

/**
 * Handles the creation of table `settings`.
 */
class m190123_155749_create_settings_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        };
        $this->createTable('settings', [
            'id' => $this->primaryKey(),
            'cost_subscription_find_letter' => $this->decimal(8,3)->comment('Стоимость подписки: Найди букву'),
            'cost_subscription_fix_letter' => $this->decimal(8,3)->comment('Стоимость подписки: Почини букву'),
            'cost_allowance_find_letterA3' => $this->decimal(8,3)->comment('Стоимость пособия: Найди букву А3'),
            'cost_allowance_find_letterA4' => $this->decimal(8,3)->comment('Стоимость пособия: Найди букву А4'),
            'cost_allowance_fix_letterA3' => $this->decimal(8,3)->comment('Стоимость пособия: Почини букву А3'),
            'cost_allowance_fix_letterA4' => $this->decimal(8,3)->comment('Стоимость пособия: Почини букву А4'),
            'cost_storage_find_letter' => $this->decimal(8,3)->comment('Стоимость хранения созданных пособий в день: Найди букву'),
            'cost_storage_fix_letter' => $this->decimal(8,3)->comment('Стоимость хранения созданных пособий в день: Почини букву'),
            'max_letter_find_letter' => $this->integer()->comment('Максимальное количество букв: Найди букву'),
            'max_letter_fix_letter' => $this->integer()->comment('Максимальное количество букв: Почини букву'),
            'bonus_register' => $this->integer()->comment('При регистрации начислять приветственный баланс'),
        ],$tableOptions);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('settings');
    }
}
