<?php

use yii\db\Migration;

/**
 * Handles the creation of table `fix_letter`.
 */
class m190123_155132_create_fix_letter_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        };
        $this->createTable('fix_letter', [
            'id' => $this->primaryKey(),
            'user_id'=>$this->integer(),
            'page_format'=>$this->integer(1)->comment('Формат листа'),
            'page_count'=>$this->integer()->comment('Количество листов'),
            'letters_count'=>$this->integer()->comment('Количество букв'),
            'letters_composition'=>$this->string(1000)->comment('Состав букв'),
            'complexity'=>$this->smallInteger()->comment('Сложность'),
            'amount'=>$this->decimal(8,3)->comment('Стоимость'),
            'save'=>$this->smallInteger()->comment('Хранить'),

        ],$tableOptions);

        $this->createIndex('idx-fix_letter-user_id','fix_letter','user_id');
        $this->addForeignKey('fk-fix_letter-user_id','fix_letter','user_id','users','id','CASCADE');

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('fix_letter');
    }
}
