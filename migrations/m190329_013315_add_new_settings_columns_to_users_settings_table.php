<?php

use yii\db\Migration;

/**
 * Handles adding new settings columns to table `users_settings`.
 */
class m190329_013315_add_new_settings_columns_to_users_settings_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('users_settings', 'after_balance_up', $this->boolean()->defaultValue(false)->after('fix_letter_data_subscribe')->comment('После поплнения баланса'));
        $this->addColumn('users_settings', 'after_letter_create', $this->boolean()->defaultValue(false)->after('after_balance_up')->comment('После создания пособия'));
        $this->addColumn('users_settings', 'balance_up_reminder', $this->boolean()->defaultValue(false)->after('after_letter_create')->comment('Напоминание о пополнении баланса'));
        $this->addColumn('users_settings', 'news', $this->boolean()->defaultValue(false)->after('balance_up_reminder')->comment('Новости и акции сервиса'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('users_settings', 'after_balance_up');
        $this->dropColumn('users_settings', 'after_letter_create');
        $this->dropColumn('users_settings', 'balance_up_reminder');
        $this->dropColumn('users_settings', 'news');
    }
}
