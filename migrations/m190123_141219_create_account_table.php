<?php

use yii\db\Migration;

/**
 * Handles the creation of table `account`.
 */
class m190123_141219_create_account_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        };
        $this->createTable('account', [
            'id' => $this->primaryKey(),
            'user_id'=>$this->integer(),
            'firstname'=>$this->string(45)->comment('Имя'),
            'lastname'=>$this->string(45)->comment('Фамилия'),
            'parentname'=>$this->string(45)->comment('Отчество'),
            'town'=>$this->string(45)->comment('Город'),
            'position'=>$this->string(45)->comment('Должность'),
            'balance'=>$this->decimal(8,3)->comment('Баланс'),

        ],$tableOptions);


        $this->createIndex('idx-account-user_id','account','user_id');
        $this->addForeignKey('fk-account-user_id','account','user_id','users','id','CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('account');
    }
}
