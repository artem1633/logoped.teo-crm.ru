<?php

use yii\db\Migration;

/**
 * Handles the creation of table `slider`.
 */
class m190126_040955_create_slider_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        };
        $this->createTable('sliders', [
            'id' => $this->primaryKey(),
            'title' => $this->string(255)->comment('Заголовок'),
            'text' => $this->text()->comment('Текст'),
            'fone' => $this->string(255)->comment('Фон'),
            'order' => $this->integer()->comment('Сортировка'),
            'view' => $this->boolean()->comment('Показ'),
            'view_time' => $this->integer()->comment('Время показа в секундах'),
        ],$tableOptions);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('slider');
    }
}
