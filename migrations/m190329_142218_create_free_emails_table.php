<?php

use yii\db\Migration;

/**
 * Handles the creation of table `free_emails`.
 */
class m190329_142218_create_free_emails_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('free_emails', [
            'id' => $this->primaryKey(),
            'recipient' => $this->text()->comment('Получатели'),
            'subject' => $this->string()->comment('Тема письма'),
            'content' => $this->text()->comment('Содержание'),
            'send_datetime' => $this->dateTime()->comment('Дата и время отправки'),
            'created_at' => $this->dateTime(),
        ]);
        $this->addCommentOnTable('free_emails', 'Почтовые сообщения для массовой рассылки');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('free_emails');
    }
}
