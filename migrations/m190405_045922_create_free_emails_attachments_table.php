<?php

use yii\db\Migration;

/**
 * Handles the creation of table `free_emails_attachments`.
 */
class m190405_045922_create_free_emails_attachments_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('free_emails_attachments', [
            'id' => $this->primaryKey(),
            'email_id' => $this->integer()->comment('Письмо'),
            'name' => $this->string()->comment('Имя файла'),
            'path' => $this->string()->comment('Путь до файла'),
            'created_at' => $this->dateTime(),
        ]);

        $this->createIndex(
            'idx-free_emails_attachments-email_id',
            'free_emails_attachments',
            'email_id'
        );

        $this->addForeignKey(
            'fk-free_emails_attachments-email_id',
            'free_emails_attachments',
            'email_id',
            'free_emails',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey(
            'fk-free_emails_attachments-email_id',
            'free_emails_attachments'
        );

        $this->dropIndex(
            'idx-free_emails_attachments-email_id',
            'free_emails_attachments'
        );

        $this->dropTable('free_emails_attachments');
    }
}
