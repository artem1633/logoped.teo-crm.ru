<?php

use yii\db\Migration;

/**
 * Handles the creation of table `fix_letter_data`.
 */
class m190307_130147_create_fix_letter_data_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('fix_letter_data', [
            'id' => $this->primaryKey(),
            'fix_letter_id' => $this->integer()->comment('Пособие'),
            'symbol' => $this->string(10)->comment('Буква'),
            'type' => $this->string()->comment('Стиль написания'),
        ]);
        $this->addCommentOnTable('fix_letter_data', 'Буквы для пособия «Почини букву»');

        $this->createIndex(
            'idx-fix_letter_data-fix_letter_id',
            'fix_letter_data',
            'fix_letter_id'
        );

        $this->addForeignKey(
            'fk-fix_letter_data-fix_letter_id',
            'fix_letter_data',
            'fix_letter_id',
            'fix_letter',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey(
            'fk-fix_letter_data-fix_letter_id',
            'fix_letter_data'
        );

        $this->dropIndex(
            'idx-fix_letter_data-fix_letter_id',
            'fix_letter_data'
        );

        $this->dropTable('fix_letter_data');
    }
}
