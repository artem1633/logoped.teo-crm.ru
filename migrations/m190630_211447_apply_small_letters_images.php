<?php

use yii\db\Migration;

/**
 * Class m190630_211447_apply_small_letters_images
 */
class m190630_211447_apply_small_letters_images extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $smallLetterlist = ['а','б','в','г','д','е','ё','ж','з','и','й','к','л','м','н','о','п','р',
            'с','т','у','ф','х','ц','ч','ш','щ','ъ','ы','ь','э','ю','я'];

        for($i = 0; $i < count($smallLetterlist); $i++) {
            $symbol = $smallLetterlist[$i];
            if ($symbol == 'ё') {
                $model = \app\models\FixLettersImages::findOne(7);
            } else {
                $model = \app\models\FixLettersImages::find()->where(['symbol' => $symbol])->one();
            }

            if ($model == null) {
                continue;
            }

            $path = "web/Буквы/Маленькие печатные/$symbol/Легко";

            $images = array_diff(scandir($path), ['.', '..']);

            foreach($images as $image) {
                $easy = new \app\models\FixLettersImagesAttachments([
                    'type' => \app\models\FixLettersImagesAttachments::TYPE_EASY,
                    'fix_letter_image_id' => $model->id,
                    'path' => "Буквы/Маленькие печатные/$symbol/Легко/$image",
                    'drawing' => \app\models\FixLettersImagesAttachments::DRAWING_PRINTED,
                ]);
                $easy->save(false);
            }


            $path = "web/Буквы/Маленькие печатные/$symbol/Средне";

            $images = array_diff(scandir($path), ['.', '..']);

            foreach($images as $image) {
                $easy = new \app\models\FixLettersImagesAttachments([
                    'type' => \app\models\FixLettersImagesAttachments::TYPE_MIDDLE,
                    'fix_letter_image_id' => $model->id,
                    'path' => "Буквы/Маленькие печатные/$symbol/Средне/$image",
                    'drawing' => \app\models\FixLettersImagesAttachments::DRAWING_PRINTED,
                ]);
                $easy->save(false);
            }


            $path = "web/Буквы/Маленькие печатные/$symbol/Сложно";

            $images = array_diff(scandir($path), ['.', '..']);

            foreach($images as $image) {
                $easy = new \app\models\FixLettersImagesAttachments([
                    'type' => \app\models\FixLettersImagesAttachments::TYPE_HARD,
                    'fix_letter_image_id' => $model->id,
                    'path' => "Буквы/Маленькие печатные/$symbol/Сложно/$image",
                    'drawing' => \app\models\FixLettersImagesAttachments::DRAWING_PRINTED,
                ]);
                $easy->save(false);
            }
        }
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {

    }
}
