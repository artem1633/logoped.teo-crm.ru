<?php

use yii\db\Migration;

/**
 * Handles adding confirmed to table `users`.
 */
class m190327_113309_add_confirmed_columns_to_users_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('users', 'email_confirmed', $this->boolean()->defaultValue(false)->comment('Подтвержден ли email (login)'));
        $this->addColumn('users', 'telephone_confirmed', $this->boolean()->defaultValue(false)->comment('Подтвержден ли телефон (telephone)'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('users', 'email_confirmed');
        $this->dropColumn('users', 'telephone_confirmed');
    }
}
