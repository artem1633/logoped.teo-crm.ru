<?php

use yii\db\Migration;

/**
 * Class m190601_105511_fix_find_letter_images_table
 */
class m190601_105511_fix_find_letter_images_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('find_letters_images', 'type', $this->string()->comment('Стиль написания'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
    }

}
