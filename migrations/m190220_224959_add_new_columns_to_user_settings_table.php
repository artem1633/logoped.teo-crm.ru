<?php

use yii\db\Migration;

/**
 * Handles adding new columns to table `user_settings`.
 */
class m190220_224959_add_new_columns_to_user_settings_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('users_settings', 'find_letter_data_subscribe', $this->boolean()->after('fix_letter_data_keeping')->defaultValue(false)->comment('Месячная подписка хранения пособий «Найти букву»'));
        $this->addColumn('users_settings', 'fix_letter_data_subscribe', $this->boolean()->after('find_letter_data_subscribe')->defaultValue(false)->comment('Месячная подписка хранения пособий «Почини букву»'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('users_settings', 'find_letter_data_subscribe');
        $this->dropColumn('users_settings', 'fix_letter_data_subscribe');
    }
}
