<?php

use yii\db\Migration;

/**
 * Class m190131_125613_rebuild_settings_table
 */
class m190131_125613_rebuild_settings_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropTable('settings');

        $this->createTable('settings', [
            'id' => $this->primaryKey(),
            'key' => $this->string()->notNull()->unique()->comment('Ключ'),
            'type' => $this->string()->defaultValue('text')->comment('Тип поля'),
            'value' => $this->string()->comment('Значение'),
            'label' => $this->text()->comment('Комментарий'),
        ]);
        $this->addCommentOnTable('settings', 'Настройки системы');

        $this->insert('settings',array(
            'key' => 'cost_subscription_find_letter',
            'value' => '0',
            'label' => 'Стоимость подписки: Найди букву',
        ));

        $this->insert('settings',array(
            'key' => 'cost_subscription_fix_letter',
            'value' => '0',
            'label' => 'Стоимость подписки: Почини букву',
        ));

        $this->insert('settings',array(
            'key' => 'cost_allowance_find_letterA3',
            'value' => '0',
            'label' => 'Стоимость пособия: Найди букву А3',
        ));

        $this->insert('settings',array(
            'key' => 'cost_allowance_find_letterA4',
            'value' => '0',
            'label' => 'Стоимость пособия: Найди букву А4',
        ));

        $this->insert('settings',array(
            'key' => 'cost_allowance_fix_letterA3',
            'value' => '0',
            'label' => 'Стоимость пособия: Почини букву А3',
        ));

        $this->insert('settings',array(
            'key' => 'cost_allowance_fix_letterA4',
            'value' => '0',
            'label' => 'Стоимость пособия: Почини букву А4',
        ));

        $this->insert('settings',array(
            'key' => 'cost_storage_find_letter',
            'value' => '0',
            'label' => 'Стоимость хранения созданных пособий в день: Найди букву',
        ));

        $this->insert('settings',array(
            'key' => 'cost_storage_fix_letter',
            'value' => '0',
            'label' => 'Стоимость хранения созданных пособий в день: Почини букву',
        ));

        $this->insert('settings',array(
            'key' => 'max_letter_find_letter',
            'value' => '0',
            'label' => 'Максимальное количество букв: Найди букву',
        ));

        $this->insert('settings',array(
            'key' => 'max_letter_fix_letter',
            'value' => '0',
            'label' => 'Максимальное количество букв: Почини букву',
        ));

        $this->insert('settings',array(
            'key' => 'register_bonus',
            'value' => '0',
            'label' => 'При регистрации начислять приветственный баланс',
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // Не имеет смысла писать так как при повторном исполнении миграции таблица удаляется
    }
}
