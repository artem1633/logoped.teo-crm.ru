<?php

use yii\db\Migration;

/**
 * Handles the creation of table `find_letters_images_attachments`.
 */
class m190419_093955_create_find_letters_images_attachments_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('find_letters_images_attachments', [
            'id' => $this->primaryKey(),
            'find_letter_image_id' => $this->integer()->comment('Буква'),
            'path' => $this->string()->comment('Путь'),
            'type' => $this->string()->comment('Тип'),
        ]);

        $this->createIndex(
            'idx-find_letters_images_attachments-find_letter_image_id',
            'find_letters_images_attachments',
            'find_letter_image_id'
        );

        $this->addForeignKey(
            'fk-find_letters_images_attachments-find_letter_image_id',
            'find_letters_images_attachments',
            'find_letter_image_id',
            'fix_letters_images',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey(
            'fk-find_letters_images_attachments-find_letter_image_id',
            'find_letters_images_attachments'
        );

        $this->dropIndex(
            'idx-find_letters_images_attachments-find_letter_image_id',
            'find_letters_images_attachments'
        );

        $this->dropTable('find_letters_images_attachments');
    }
}
