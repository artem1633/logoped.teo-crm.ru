<?php

use yii\db\Migration;

/**
 * Handles the creation of table `find_letters_images`.
 */
class m190419_093820_create_find_letters_images_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('find_letters_images', [
            'id' => $this->primaryKey(),
            'symbol' => $this->string(10)->comment('Символ'),
        ]);

        $letters = ['а','б','в','г','д','е','ё','ж','з','и','й','к','л','м','н','о','п','р',
            'с','т','у','ф','х','ц','ч','ш','щ','ъ','ы','ь','э','ю','я'];

        foreach ($letters as $letter)
        {
            $this->insert('find_letters_images', [
                'symbol' => $letter
            ]);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('find_letters_images');
    }
}
