<?php

use yii\db\Migration;

/**
 * Handles adding drawing to table `fix_letters_images_attachments`.
 */
class m190621_121547_add_drawing_column_to_fix_letters_images_attachments_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('fix_letters_images_attachments', 'drawing', $this->integer()->defaultValue(0)->comment('Начертание'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('fix_letters_images_attachments', 'drawing');
    }
}
