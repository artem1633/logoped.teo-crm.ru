<?php

use yii\db\Migration;

/**
 * Class m190625_190812_apply_find_letter
 */
class m190625_190812_apply_find_letter extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $bigLetterlist = ['А','Б','В','Г','Д','Е','Ё','Ж','З','И','Й','К','Л','М','Н','О','П','Р',
            'С','Т','У','Ф','Х','Ц','Ч','Ш','Щ','Ъ','Ы','Ь','Э','Ю','Я'];

        $smallLetterlist = ['а','б','в','г','д','е','ё','ж','з','и','й','к','л','м','н','о','п','р',
            'с','т','у','ф','х','ц','ч','ш','щ','ъ','ы','ь','э','ю','я'];

        for($i = 0; $i < count($smallLetterlist); $i++) {
            $symbol = $smallLetterlist[$i];

            if ($symbol == 'ё') {
                $model = \app\models\FindLettersImages::findOne(7);
            } else if($symbol == "е") {
                $model = \app\models\FindLettersImages::findOne(6);
            }
            else {
                $model = \app\models\FindLettersImages::find()->where(['symbol' => $symbol])->one();
            }

            if ($model == null) {
                continue;
            }

            $path = "web/1/$symbol";

            $images = array_diff(scandir($path), ['.', '..']);

            foreach ($images as $image)
            {
                $imageModel = new \app\models\FindLettersImagesAttachments([
                    'find_letter_image_id' => $model->id,
                    'type' => \app\models\FindLettersImagesAttachments::TYPE_WROTE_DOWN,
                    'path' => "1/$symbol/$image",
                ]);
                $imageModel->save(false);
            }
        }

        for($i = 0; $i < count($bigLetterlist); $i++) {
            $symbol = $bigLetterlist[$i];

            if ($symbol == 'Ё') {
                $model = \app\models\FindLettersImages::findOne(7);
            } else if($symbol == "Е") {
                $model = \app\models\FindLettersImages::findOne(6);
            }
            else {
                $model = \app\models\FindLettersImages::find()->where(['symbol' => $symbol])->orderBy('id desc')->one();
            }

            if ($model == null) {
                continue;
            }

            $path = "web/WroteUpLetters/$symbol";

            $images = array_diff(scandir($path), ['.', '..']);

            foreach ($images as $image)
            {
                $imageModel = new \app\models\FindLettersImagesAttachments([
                    'find_letter_image_id' => $model->id,
                    'type' => \app\models\FindLettersImagesAttachments::TYPE_WROTE_UP,
                    'path' => "WroteUpLetters/$symbol/$image",
                ]);
                $imageModel->save(false);
            }
        }

        for($i = 0; $i < count($smallLetterlist); $i++) {
            $symbol = $smallLetterlist[$i];

            if ($symbol == 'ё') {
                $model = \app\models\FindLettersImages::findOne(7);
            } else if($symbol == "е") {
                $model = \app\models\FindLettersImages::findOne(6);
            }
            else {
                $model = \app\models\FindLettersImages::find()->where(['symbol' => $symbol])->one();
            }

            if ($model == null) {
                continue;
            }

            $path = "web/PrintedDownLetters/$symbol";

            $images = array_diff(scandir($path), ['.', '..']);

            foreach ($images as $image)
            {
                $imageModel = new \app\models\FindLettersImagesAttachments([
                    'find_letter_image_id' => $model->id,
                    'type' => \app\models\FindLettersImagesAttachments::TYPE_PRINTED_DOWN,
                    'path' => "PrintedDownLetters/$symbol/$image",
                ]);
                $imageModel->save(false);
            }
        }

        for($i = 0; $i < count($bigLetterlist); $i++) {
            $symbol = $bigLetterlist[$i];

            if ($symbol == 'Ё') {
                $model = \app\models\FindLettersImages::findOne(7);
            } else if($symbol == "Е") {
                $model = \app\models\FindLettersImages::findOne(6);
            }
            else {
                $model = \app\models\FindLettersImages::find()->where(['symbol' => $symbol])->orderBy('id desc')->one();
            }

            if ($model == null) {
                continue;
            }

            $path = "web/PrintedUpLetters/$symbol";

            $images = array_diff(scandir($path), ['.', '..']);

            foreach ($images as $image)
            {
                $imageModel = new \app\models\FindLettersImagesAttachments([
                    'find_letter_image_id' => $model->id,
                    'type' => \app\models\FindLettersImagesAttachments::TYPE_PRINTED_UP,
                    'path' => "PrintedUpLetters/$symbol/$image",
                ]);
                $imageModel->save(false);
            }
        }
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        Yii::$app->db->createCommand()->truncateTable('find_letters_images_attachments')->execute();
    }
}
