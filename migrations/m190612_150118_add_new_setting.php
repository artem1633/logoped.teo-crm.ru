<?php

use yii\db\Migration;

/**
 * Class m190612_150118_add_new_setting
 */
class m190612_150118_add_new_setting extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $setting = new \app\models\Settings([
            'label' => 'Уведомление о не вхождении в сервис (время в днях)',
            'key' => 'no_enter_notify_time',
            'value' => '2',
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
    }
}
