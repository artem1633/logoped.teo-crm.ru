<?php

use yii\db\Migration;

/**
 * Handles the creation of table `promo_codes_usages`.
 */
class m190630_225209_create_promo_codes_usages_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('promo_codes_usages', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->comment('Пользователь'),
            'promo_code_id' => $this->integer()->comment('Промокод'),
            'promo_code_name' => $this->string()->comment('Наименование промокода'),
            'count' => $this->integer()->comment('Кол-во использований'),
            'last_usage_datetime' => $this->dateTime()->comment('Дата и время последнего использования'),
            'created_at' => $this->dateTime(),
        ]);
        $this->addCommentOnTable('promo_codes_usages', 'Ведет учет использований промокодов');

        $this->createIndex(
            'idx-promo_codes_usages-user_id',
            'promo_codes_usages',
            'user_id'
        );

        $this->addForeignKey(
            'fk-promo_codes_usages-user_id',
            'promo_codes_usages',
            'user_id',
            'users',
            'id',
            'SET NULL'
        );

        $this->createIndex(
            'idx-promo_codes_usages-promo_code_id',
            'promo_codes_usages',
            'promo_code_id'
        );

        $this->addForeignKey(
            'fk-promo_codes_usages-promo_code_id',
            'promo_codes_usages',
            'promo_code_id',
            'promo_code',
            'id',
            'SET NULL'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey(
            'fk-promo_codes_usages-promo_code_id',
            'promo_codes_usages'
        );

        $this->dropIndex(
            'idx-promo_codes_usages-promo_code_id',
            'promo_codes_usages'
        );

        $this->dropForeignKey(
            'fk-promo_codes_usages-user_id',
            'promo_codes_usages'
        );

        $this->dropIndex(
            'idx-promo_codes_usages-user_id',
            'promo_codes_usages'
        );

        $this->dropTable('promo_codes_usages');
    }
}
