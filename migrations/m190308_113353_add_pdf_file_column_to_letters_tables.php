<?php

use yii\db\Migration;

/**
 * Class m190308_113353_add_pdf_file_column_to_letters_tables
 */
class m190308_113353_add_pdf_file_column_to_letters_tables extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('find_letter', 'pdf_file', $this->string()->comment('Путь до сгенерированого пособия'));
        $this->addColumn('fix_letter', 'pdf_file', $this->string()->comment('Путь до сгенерированого пособия'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('find_letter', 'pdf_file');
        $this->dropColumn('fix_letter', 'pdf_file');
    }
}
