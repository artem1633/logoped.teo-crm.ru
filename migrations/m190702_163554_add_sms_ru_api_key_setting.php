<?php

use yii\db\Migration;

/**
 * Class m190702_163554_add_sms_ru_api_key_setting
 */
class m190702_163554_add_sms_ru_api_key_setting extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->insert('settings', [
            'key' => 'sms_ru_api_key_setting',
            'label' => 'API-ключ от сервиса sms.ru',
            'value' => '',
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
    }
}
