<?php

use yii\db\Migration;

/**
 * Handles the creation of table `sms`.
 */
class m190701_171736_create_sms_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('sms', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->comment('Пользователь, которому отправлена SMS'),
            'phone_number' => $this->string()->comment('Номер телефона'),
            'sms_text' => $this->text()->comment('Текст sms'),
            'status' => $this->integer()->comment('Статус'),
            'status_code' => $this->integer()->comment('Код статуса'),
            'cost' => $this->float()->comment('Стоимость'),
            'created_at' => $this->dateTime(),
        ]);

        $this->createIndex(
            'idx-sms-user_id',
            'sms',
            'user_id'
        );

        $this->addForeignKey(
            'fk-sms-user_id',
            'sms',
            'user_id',
            'users',
            'id',
            'SET NULL'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey(
            'fk-sms-user_id',
            'sms'
        );

        $this->dropIndex(
            'idx-sms-user_id',
            'sms'
        );

        $this->dropTable('sms');
    }
}
