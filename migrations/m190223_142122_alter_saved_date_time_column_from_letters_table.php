<?php

use yii\db\Migration;

/**
 * Class m190223_142122_alter_saved_date_time_column_from_letters_table
 */
class m190223_142122_alter_saved_date_time_column_from_letters_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropColumn('find_letter', 'saved_datetime');
        $this->dropColumn('fix_letter', 'saved_datetime');

        $this->addColumn('find_letter', 'using_end_datetime', $this->dateTime()->comment('Время окончания пользования'));
        $this->addColumn('fix_letter', 'using_end_datetime', $this->dateTime()->comment('Время окончания пользования'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('find_letter', 'using_end_datetime');
        $this->dropColumn('fix_letter', 'using_end_datetime');

        $this->addColumn('find_letter', 'saved_datetime', $this->dateTime()->comment('Время последнего сохранения'));
        $this->addColumn('fix_letter', 'saved_datetime', $this->dateTime()->comment('Время последнего сохранения'));
    }
}
