<?php

use yii\db\Migration;

/**
 * Class m190608_002103_delete_type_column_from_find_letters_images_table
 */
class m190608_002103_delete_type_column_from_find_letters_images_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropColumn('find_letters_images', 'type');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
    }
}
