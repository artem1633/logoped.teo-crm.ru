<?php

use yii\db\Migration;

/**
 * Handles the creation of table `free_email_recipients`.
 */
class m190703_182043_create_free_email_recipients_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('free_email_recipients', [
            'id' => $this->primaryKey(),
            'email_id' => $this->integer()->comment('Запись о рассылки'),
            'recipient_email' => $this->string()->comment('Почта на которую будет отправлена тело почты'),
            'content' => $this->text()->comment('Содержание сообщения'),
            'status' => $this->integer()->comment('Статус'),
            'send_datetime' => $this->dateTime()->comment('Дата и время попытки отправки'),
        ]);

        $this->createIndex(
            'idx-free_email_recipients-email_id',
            'free_email_recipients',
            'email_id'
        );

        $this->addForeignKey(
            'fk-free_email_recipients-email_id',
            'free_email_recipients',
            'email_id',
            'free_emails',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey(
            'fk-free_email_recipients-email_id',
            'free_email_recipients'
        );

        $this->dropIndex(
            'idx-free_email_recipients-email_id',
            'free_email_recipients'
        );

        $this->dropTable('free_email_recipients');
    }
}
