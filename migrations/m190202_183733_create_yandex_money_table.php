<?php

use yii\db\Migration;

/**
 * Handles the creation of table `yandex_money`.
 */
class m190202_183733_create_yandex_money_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('yandex_money', [
            'id' => $this->primaryKey(),
            'secret' =>  $this->string(250)->null()->comment('Секрет'),
            'wallet' => $this->string(20)->null()->comment('Кошелек'),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('yandex_money');
    }
}
