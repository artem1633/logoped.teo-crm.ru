<?php

use yii\db\Migration;

/**
 * Handles the creation of table `users_settings`.
 */
class m190207_204026_create_users_settings_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('users_settings', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->comment('Пользователь'),
            'find_letter_data_keeping' => $this->boolean()->defaultValue(false)->comment('Хранение пособий «Найти букву»'),
            'fix_letter_data_keeping' => $this->boolean()->defaultValue(false)->comment('Хранение пособий «Починить букву»'),
            'updated_at' => $this->dateTime(),
            'created_at' => $this->dateTime(),
        ]);
        $this->addCommentOnTable('users_settings', 'Индивидуальные настройки пользователей');

        $this->createIndex(
            'idx-users_settings-user_id',
            'users_settings',
            'user_id'
        );

        $this->addForeignKey(
            'fk-users_settings-user_id',
            'users_settings',
            'user_id',
            'users',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey(
            'fk-users_settings-user_id',
            'users_settings'
        );

        $this->dropIndex(
            'idx-users_settings-user_id',
            'users_settings'
        );

        $this->dropTable('users_settings');
    }
}
