<?php

use yii\db\Migration;

/**
 * Class m190423_141955_add_symbols_data_column_find_letter_table
 */
class m190423_125055_add_symbols_data_column_find_letter_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('find_letter', 'symbols_data', $this->text()->comment('JSON данные о символах'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('find_letter', 'symbols_data');
    }
}
