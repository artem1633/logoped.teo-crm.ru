<?php

use yii\db\Migration;

/**
 * Handles the creation of table `phones_confirmations`.
 */
class m190701_200712_create_phones_confirmations_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('phones_confirmations', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->comment('Пользователь'),
            'phone' => $this->string()->comment('Телефон на который отправлен код'),
            'code' => $this->string(),
            'status' => $this->boolean()->comment('Статус (Неподтвержден/Подтвержден)'),
            'created_at' => $this->dateTime(),
        ]);
        $this->addCommentOnTable('phones_confirmations', 'Записи кодов для подтверждения телефонов пользователей');

        $this->createIndex(
            'idx-phones_confirmations-user_id',
            'phones_confirmations',
            'user_id'
        );

        $this->addForeignKey(
            'fk-phones_confirmations-user_id',
            'phones_confirmations',
            'user_id',
            'users',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey(
            'fk-phones_confirmations-user_id',
            'phones_confirmations'
        );

        $this->dropIndex(
            'idx-phones_confirmations-user_id',
            'phones_confirmations'
        );

        $this->dropTable('phones_confirmations');
    }
}
