<?php

use yii\db\Migration;

/**
 * Handles the creation of table `fix_letters_images_attachments`.
 */
class m190412_215955_create_fix_letters_images_attachments_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('fix_letters_images_attachments', [
            'id' => $this->primaryKey(),
            'fix_letter_image_id' => $this->integer()->comment('Буква'),
            'path' => $this->string()->comment('Путь'),
            'type' => $this->string()->comment('Тип'),
        ]);

        $this->createIndex(
            'idx-fix_letters_images_attachments-fix_letter_image_id',
            'fix_letters_images_attachments',
            'fix_letter_image_id'
        );

        $this->addForeignKey(
            'fk-fix_letters_images_attachments-fix_letter_image_id',
            'fix_letters_images_attachments',
            'fix_letter_image_id',
            'fix_letters_images',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey(
            'fk-fix_letters_images_attachments-fix_letter_image_id',
            'fix_letters_images_attachments'
        );

        $this->dropIndex(
            'idx-fix_letters_images_attachments-fix_letter_image_id',
            'fix_letters_images_attachments'
        );

        $this->dropTable('fix_letters_images_attachments');
    }
}
