<?php

use yii\db\Migration;

/**
 * Handles adding exception_text to table `free_email_recipients`.
 */
class m190704_193744_add_exception_text_column_to_free_email_recipients_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('free_email_recipients', 'exception_text', $this->text()->after('status')->comment('Текст исключения возникшего во премя ошибки'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('free_email_recipients', 'exception_text');
    }
}
