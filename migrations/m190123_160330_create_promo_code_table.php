<?php

use yii\db\Migration;

/**
 * Handles the creation of table `promo_code`.
 */
class m190123_160330_create_promo_code_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        };
        $this->createTable('promo_code', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->comment('Название промокода'),
            'description' => $this->string(500)->comment('Комментарий') ,
            'start_date' => $this->date()->comment('Дата начала действия'),
            'end_date' => $this->date()->comment('Дата окончания действия') ,
            'endless' => $this->smallInteger(1)->comment('Бессрочно'),
            'action' => $this->smallInteger(1)->comment('Действие'),
            'action_value' => $this->decimal(8,2)->comment('Значение')
        ],$tableOptions);

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('promo_code');
    }
}
