<?php

use yii\db\Migration;

/**
 * Handles the creation of table `fonts`.
 */
class m190226_231114_create_fonts_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('fonts', [
            'id' => $this->primaryKey(),
            'font_family' => $this->string()->comment('Наименование'),
            'eot_file_path' => $this->string()->comment('EOT путь'),
            'eot_ie_file_path' => $this->string()->comment('EOT IE путь'),
            'woff2_file_path' => $this->string()->comment('WOFF2 путь'),
            'woff_file_path' => $this->string()->comment('WOFF путь'),
            'ttf_file_path' => $this->string()->comment('TTF путь'),
            'svg_file_path' => $this->string()->comment('SVG путь'),
            'font_weight' => $this->string()->comment('Ширина'),
            'font_style' => $this->string()->comment('Стиль'),
        ]);
        $this->addCommentOnTable('fonts', 'Шрифты (font face)');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('fonts');
    }
}
