<?php

use yii\db\Migration;

/**
 * Handles the creation of table `fix_letters_images`.
 */
class m190412_215403_create_fix_letters_images_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('fix_letters_images', [
            'id' => $this->primaryKey(),
            'symbol' => $this->string(10)->comment('Символ'),
        ]);

        $letters = ['а','б','в','г','д','е','ё','ж','з','и','й','к','л','м','н','о','п','р',
        'с','т','у','ф','х','ц','ч','ш','щ','ъ','ы','ь','э','ю','я', 'А','Б','В','Г','Д','Е','Ё','Ж','З','И','Й','К','Л','М','Н','О','П','Р',
        'С','Т','У','Ф','Х','Ц','Ч','Ш','Щ','Ъ','Ы','Ь','Э','Ю','Я'];

        foreach ($letters as $letter)
        {
            $this->insert('fix_letters_images', [
                'symbol' => $letter
            ]);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('fix_letters_images');
    }
}
