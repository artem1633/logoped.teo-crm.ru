<?php

use yii\db\Migration;

/**
 * Handles the creation of table `balances`.
 */
class m190123_155356_create_balances_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        };

        $this->createTable('balances', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer(),
            'amount' => $this->decimal(8,3)->comment('Сумма платежа'),
            'pay_date' => $this->dateTime()->comment('Дата платежа'),
            'promo_code_id' => $this->integer()->comment('Промокод'),
            'amount_bonus' => $this->decimal(8,3)->comment('Сумма бонуса'),
            'dk' => $this->smallInteger()->defaultValue(0)->comment('дебет/кредит'),
            'comment' => $this->string(255)->comment('Описание')

        ],$tableOptions);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('balances');
    }
}
