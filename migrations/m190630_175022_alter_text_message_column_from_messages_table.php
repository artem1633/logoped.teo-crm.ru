<?php

use yii\db\Migration;

/**
 * Class m190630_175022_alter_text_message_column_from_messages_table
 */
class m190630_175022_alter_text_message_column_from_messages_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->alterColumn('messages', 'text_message', $this->text()->comment('Текст сообщения'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->alterColumn('messages', 'text_message', $this->string()->comment('Текст сообщения'));
    }
}
