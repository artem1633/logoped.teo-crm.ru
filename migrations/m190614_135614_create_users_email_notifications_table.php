<?php

use yii\db\Migration;

/**
 * Handles the creation of table `users_email_notifications`.
 */
class m190614_135614_create_users_email_notifications_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('users_email_notifications', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->comment('Пользователь'),
            'long_time_no_enter' => $this->boolean()->defaultValue(false)->comment('Долгое время не заходил в систему'),
        ]);

        $this->createIndex(
            'idx-users_email_notifications-user_id',
            'users_email_notifications',
            'user_id'
        );

        $this->addForeignKey(
            'fk-users_email_notifications-user_id',
            'users_email_notifications',
            'user_id',
            'users',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey(
            'fk-users_email_notifications-user_id',
            'users_email_notifications'
        );

        $this->dropIndex(
            'idx-users_email_notifications-user_id',
            'users_email_notifications'
        );

        $this->dropTable('users_email_notifications');
    }
}
