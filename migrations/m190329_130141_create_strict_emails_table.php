<?php

use yii\db\Migration;

/**
 * Handles the creation of table `strict_emails`.
 */
class m190329_130141_create_strict_emails_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('strict_emails', [
            'id' => $this->primaryKey(),
            'key' => $this->string()->comment('Ключ'),
            'subject' => $this->string()->comment('Тема письма'),
            'content' => $this->text()->comment('Содержание'),
        ]);
        $this->addCommentOnTable('strict_emails', 'Почтовые сообщения для событий (регистрация, востановление пароля, и т.п.)');

        $this->insert('strict_emails', [
            'key' => 'register',
            'subject' => 'Успешная регистрация',
            'content' => 'Доброго времени суток, {fio}. Вы успешно прошли регистрацию. Для подтверждения вашего E-mail пройдите по ссылке {email_confirm}',
        ]);

        $this->insert('strict_emails', [
            'key' => 'email_confirm',
            'subject' => 'Подтверждение E-mail',
            'content' => 'Для подтверждения вашего E-mail пройдите по ссылке {email_confirm}',
        ]);

        $this->insert('strict_emails', [
            'key' => 'forget_password',
            'subject' => 'Востановление пароля',
            'content' => 'Для востановления пароля пройдите по ссылке {forget_password}',
        ]);

        $this->insert('strict_emails', [
            'key' => 'letter_created_not_payed',
            'subject' => 'Заказ создан, но не оплачен',
            'content' => '',
        ]);

        $this->insert('strict_emails', [
            'key' => 'long_time_not_online',
            'subject' => 'Давно не заходили на сервис',
            'content' => '',
        ]);

        $this->insert('strict_emails', [
            'key' => 'letter',
            'subject' => 'Заказ {letter}',
            'content' => '',
        ]);

        $this->insert('strict_emails', [
            'key' => 'up_balance_for_storage',
            'subject' => 'Недостаточно средств',
            'content' => 'Пополните баланс для дальнейшего хранения пособий',
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('strict_emails');
    }
}
