<?php

use yii\db\Migration;

/**
 * Handles the creation of table `messages`.
 */
class m190123_155652_create_messages_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        };
        $this->createTable('messages', [
            'id' => $this->primaryKey(),
            'text_message' => $this->string('255')->comment('Текст сообщения'),
            'start_date' => $this->date()->comment('Дата начала показа'),
            'end_date' => $this->date()->comment('Дата конца показа'),
            'button1' =>$this->string()->comment('Кнопка 1 '),
            'button2' =>$this->string()->comment('Кнопка 2 '),
        ],$tableOptions);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('messages');
    }
}
