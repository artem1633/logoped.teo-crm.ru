<?php

use yii\db\Migration;

/**
 * Handles the creation of table `find_letter_data`.
 */
class m190305_120849_create_find_letter_data_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('find_letter_data', [
            'id' => $this->primaryKey(),
            'find_letter_id' => $this->integer()->comment('Пособие'),
            'symbol' => $this->string(10)->comment('Буква'),
            'type' => $this->string()->comment('Стиль написания'),
            'amount' => $this->integer()->comment('Кол-во'),
        ]);
        $this->addCommentOnTable('find_letter_data', 'Буквы для пособия «Найди букву»');

        $this->createIndex(
            'idx-find_letter_data-find_letter_id',
            'find_letter_data',
            'find_letter_id'
        );

        $this->addForeignKey(
            'fk-find_letter_data-find_letter_id',
            'find_letter_data',
            'find_letter_id',
            'find_letter',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey(
            'fk-find_letter_data-find_letter_id',
            'find_letter_data'
        );

        $this->dropIndex(
            'idx-find_letter_data-find_letter_id',
            'find_letter_data'
        );

        $this->dropTable('find_letter_data');
    }
}
