<?php

use yii\db\Migration;

/**
 * Handles the creation of table `payment_orders`.
 */
class m190202_184003_create_payment_orders_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('payment_orders', [
            'id' => $this->primaryKey(),
            'date_order' =>  'timestamp DEFAULT NOW() COMMENT "Дата и время заказа выплаты"',
            'date_payment' =>  $this->timestamp()->null()->comment('Дата и время выплаты'),
            'amount' => $this->decimal(10,2)->notNull()->defaultValue(0)->comment('Сумма выплаты'),
            'status' => $this->integer()->notNull()->defaultValue(0)->comment('Статус выплаты'),
            'type' => $this->integer()->notNull()->comment('Тип выплаты'),
            'requisites' => $this->string()->null()->comment('Реквизиты'),
            'description' => $this->string()->null()->comment('Коментарии'),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('payment_orders');
    }
}
