<?php

use yii\db\Migration;

/**
 * Class m190621_182011_apply_printed_letters_images
 */
class m190621_182011_apply_printed_letters_images extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $bigLetterlist = ['А','Б','В','Г','Д','Е','Ё','Ж','З','И','Й','К','Л','М','Н','О','П','Р',
            'С','Т','У','Ф','Х','Ц','Ч','Ш','Щ','Ъ','Ы','Ь','Э','Ю','Я'];


        for($i = 0; $i < count($bigLetterlist); $i++) {
            $symbol = $bigLetterlist[$i];
            if ($symbol == 'Ё') {
                $model = \app\models\FixLettersImages::findOne(40);
            } else if($symbol == "Е") {
                $model = \app\models\FixLettersImages::findOne(39);
            }
            else {
                $model = \app\models\FixLettersImages::find()->where(['symbol' => $symbol])->orderBy('id desc')->one();
            }

            if ($model == null) {
                continue;
            }

            $path = "web/Буквы/Печатные/Большие/$symbol/Легко";

            $imagesCount = count(array_diff(scandir($path), ['.', '..']));

            for ($j = 0; $j < $imagesCount; $j++) {
                $easy = new \app\models\FixLettersImagesAttachments([
                    'type' => \app\models\FixLettersImagesAttachments::TYPE_EASY,
                    'fix_letter_image_id' => $model->id,
                    'drawing' => \app\models\FixLettersImagesAttachments::DRAWING_PRINTED,
                    'path' => "Буквы/Печатные/Большие/$symbol/Легко/$j.png",
                ]);
                $easy->save(false);
            }


            $path = "web/Буквы/Печатные/Большие/$symbol/Средне";

            $imagesCount = count(array_diff(scandir($path), ['.', '..']));

            for ($j = 0; $j < $imagesCount; $j++) {
                $easy = new \app\models\FixLettersImagesAttachments([
                    'type' => \app\models\FixLettersImagesAttachments::TYPE_MIDDLE,
                    'fix_letter_image_id' => $model->id,
                    'drawing' => \app\models\FixLettersImagesAttachments::DRAWING_PRINTED,
                    'path' => "Буквы/Печатные/Большие/$symbol/Средне/$j.png",
                ]);
                $easy->save(false);
            }


            $path = "web/Буквы/Печатные/Большие/$symbol/Сложно";

            $imagesCount = count(array_diff(scandir($path), ['.', '..']));

            for ($j = 0; $j < $imagesCount; $j++) {
                $easy = new \app\models\FixLettersImagesAttachments([
                    'type' => \app\models\FixLettersImagesAttachments::TYPE_HARD,
                    'fix_letter_image_id' => $model->id,
                    'drawing' => \app\models\FixLettersImagesAttachments::DRAWING_PRINTED,
                    'path' => "Буквы/Печатные/Большие/$symbol/Сложно/$j.png",
                ]);
                $easy->save(false);
            }
        }
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {

    }
}
