<?php

namespace app\modules\api\controllers;

use app\models\FindLetter;
use app\models\FixLetter;
use app\models\subscriptions\Subscriptions;
use app\models\users\Users;
use yii\filters\AccessControl;
use yii\rest\ActiveController;

/**
 * Class LettersController
 * @package app\modules\api\controllers
 */
class LettersController extends ActiveController
{
    public $modelClass = 'app\models\Api';
    public $model = null;

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'except' => ['confirm-pay'],
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    /**
     * Удаляет записи, у которых истек срок хранения
     */
    public function actionDeleteStoringExpired()
    {
        $find = FindLetter::find()->storingExpired()->all();
        var_dump($find);
        foreach ($find as $model) {
            $model->delete();
        }

        $fix = FixLetter::find()->storingExpired()->all();
        var_dump($fix);
        foreach ($fix as $model) {
            $model->delete();
        }
    }

    /**
     * Обрабатывает подписки пользователей
     */
    public function actionUsersSubscriptionsHandling()
    {
        $users = Users::find()->all();

        foreach ($users as $user)
        {
            /** @var Users $user */
            foreach ($user->subscription->getSubscriptionsList() as $type => $subscription)
            {
                /** @var Subscriptions $subscription */
                if($subscription->typeInstance->getIsSubscribed() && $subscription->typeInstance->getIsPayed() == false){
                    $subscription->typeInstance->pay();
                }
            }
        }
    }
}