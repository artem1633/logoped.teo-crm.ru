<?php

namespace app\modules\api\controllers;

use app\components\helpers\TagHelper;
use Yii;
use app\models\Settings;
use app\models\StrictEmails;
use app\models\users\Users;
use yii\filters\AccessControl;
use yii\rest\ActiveController;

/**
 * Class UsersController
 * @package app\modules\api\controllers
 */
class UsersController extends ActiveController
{
    public $modelClass = 'app\models\Api';
    public $model = null;

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    /**
     * Удаляет записи, у которых истек срок хранения
     */
    public function actionNotifyExitUsers()
    {
        $daysExit = Settings::findByKey('no_enter_notify_time')->value;
        $controlDate = date('Y-m-d', time() - $daysExit * 86400);

        $users = Users::find()->where('>','last_activity_datetime', $controlDate)->andWhere(['type' => 1])->all();

        /** @var \app\models\users\Users $user */
        foreach ($users as $user)
        {
            if($user->emailMarkers->isMarked('long_time_no_enter')){
                continue;
            }

            $strictMail = StrictEmails::findByKey('long_time_not_online');
            $html = $strictMail->content;

            Yii::$app->mailer->compose()
                ->setFrom('logoped.service@mail.ru')
                ->setTo($user->login)
                ->setSubject($strictMail->subject)
                ->setHtmlBody(TagHelper::handle($html, $user))
                ->send();

            $user->emailMarkers->mark('long_time_no_enter');
        }
    }
}