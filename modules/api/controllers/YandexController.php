<?php

namespace app\modules\api\controllers;

use app\models\Balances;
use app\models\PromoCode;
use app\models\users\Users;
use Yii,
    yii\rest\ActiveController,
    yii\filters\AccessControl,
    yii\web\NotFoundHttpException,
    app\models\YandexMoney;


/**
 * Class YandexController
 * @package app\modules\api\controllers
 *
 * @property Client $model
 */
class YandexController extends ActiveController
{
    public $modelClass = 'app\models\Api';
    public $model = null;

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'except' => ['confirm-pay'],
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    /**
     * @return array
     */
    public function actions()
    {
        if (is_null($this->model) && !Yii::$app->user->isGuest) {
//            $this->model = $this->findClient(Yii::$app->user->identity->entity);
        }
        return parent::actions();
    }

    /**
     * @param \yii\base\Action $action
     * @return bool
     */
    public function beforeAction($action)
    {
        $this->enableCsrfValidation = false;
        return parent::beforeAction($action);
    }



    /**
     * @return string
     */
    public function actionConfirmPay()
    {
        $this->enableCsrfValidation = false;
        if (Yii::$app->request->isPost) {
            $post = Yii::$app->request->post();
            $configure = $this->findModel(1);
            if ($post['label']) echo $post['label'];
            if ($user = Users::findOne($post['label'])) {
                $sha1_res = sha1($post['notification_type']
                    . '&' . $post['operation_id']
                    . '&' . $post['amount']
                    . '&' . $post['currency']
                    . '&' . $post['datetime']
                    . '&' . $post['sender']
                    . '&' . $post['codepro']
                    . '&' . $configure->secret
                    . '&' . $post['label']);

                if ($sha1_res == $post['sha1_hash']) {
                    $account = $user->account;

                    $profit = floatval($post['withdraw_amount']);

                    if($post['comment'] != null){
                        var_dump('here');
                        $promo = PromoCode::find()->where(['name' => $post['comment']])->active()->one();

                        var_dump($promo);

                        if($promo != null)
                        {
                            $profit += $promo->action_value;
                        }
                    }

                    $account->balance += $profit;

                    var_dump($post['codepro']);
                    var_dump($post);

                    $balance = new Balances([
                        'user_id' => $user->id,
                        'dk' => Balances::DEBIT,
                        'amount' => floatval($post['withdraw_amount']),
                        'pay_date' => date('Y-m-d H:i:s'),
//                        'comment' => $post['comment'],
                    ]);

                    var_dump($balance->save(false));

                    if ($account->save()) {
                        $balance = new Balances([
                            'user_id' => $user->id,
                            'dk' => Balances::DEBIT,
                            'amount' => floatval($post['withdraw_amount']),
                            'pay_date' => date('Y-m-d H:i:s'),
                            'comment' => 'Поплнение личного ссчета. Квитанция №' . $post['operation_id']
                        ]);

                        $balance->save(false);
                    }

                }
            }
        }
    }

    /**
     * @param $id
     * @return null|static
     * @throws NotFoundHttpException
     */
    protected function findModel($id)
    {
        if (($model = YandexMoney::findOne($id)) !== null) {
            return $model;
        } else {
            return new YandexMoney;
        }
    }
}
