<?php

namespace app\modules\api\controllers;

use app\components\helpers\TagHelper;
use app\models\FreeEmailRecipients;
use app\models\FreeEmails;
use Yii;
use app\models\Settings;
use app\models\StrictEmails;
use app\models\users\Users;
use yii\filters\AccessControl;
use yii\rest\ActiveController;

/**
 * Class MailingController
 * @package app\modules\api\controllers
 */
class MailingController extends ActiveController
{
    public $modelClass = 'app\models\Api';
    public $model = null;

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    /**
     * Отсылает сообщения
     */
    public function actionSend()
    {
        $mails = FreeEmailRecipients::find()->where(['status' => FreeEmailRecipients::STATUS_WAIT])->limit(1)->all();

        foreach ($mails as $mail)
        {
            $email = FreeEmails::findOne($mail->email_id);

            $result = Yii::$app->mailer->compose()
                ->setTo($mail->recipient_email)
                ->setFrom('logoped.service@mail.ru')
                ->setSubject($email->subject)
                ->setHtmlBody($email->content)
                ->send();

//            $result = true;

            var_dump($result);

            if($result) {
                $mail->status = FreeEmailRecipients::STATUS_SUCCESS;
                $mail->save(false);
            }
        }
    }
}