<?php

use yii\helpers\Html;
use app\models\Questionary;
use yii\widgets\Pjax;

$_csrf = \Yii::$app->request->getCsrfToken();
$session = Yii::$app->session;
$question = $session['question'];
$turn = $session['turn'];
if (!file_exists('avatars/'.$session['answers'][$turn - 1]['answer']) || $session['answers'][$turn - 1]['answer'] === null) {
    $path = 'https://' . $_SERVER['SERVER_NAME'].'/images/nouser.png';
} else {
    $path = 'https://' . $_SERVER['SERVER_NAME'] . '/avatars/' . $session['answers'][$turn - 1]['answer'];
}
/*echo "<pre>";
print_r($question);
echo "</pre>";
die;*/

?>      

<div class="test-container lightmode">        
    <div class="test-box animated fadeInDown">
        <div class="test-body">

        <div class="row">
            <?php Pjax::begin(['enablePushState' => false, 'id' => 'question-pjax']) ?>
            <div class="col-md-9">
                <div class="panel panel-warning">
                    <div class="panel-heading ui-draggable-handle">
                        <h3 class="panel-title"><?=$turn?> - вопрос</h3>
                    </div>
                    <div class="panel-body">
                        <div>
                            <div class="col-md-12">
                                <h3><?=$question->name?> </h3>
                                <br>
                            </div>
                            <h4><?=$question->description?></h4>
                            <br>
                            <?php
                                    if($question->type == 0)
                                    { 
                            ?>
                                        <div class="col-md-12">
                                            <textarea name="default<?=$question->id?>" id="default<?=$question->id?>" <?= $question->require ? 'required="required"' : '' ?> class="form-control" onchange="$.get('/page/default/set-values', {'id':<?=$question->id?>, 'turn': <?=$turn?>, 'value':$('#default<?=$question->id?>').val()}, function(data){} );" rows="1"><?=$session['answers'][$turn - 1]['answer']?></textarea>
                                        </div>
                            <?php
                                    }
                                    if($question->type == 1)
                                    {
                            ?>
                                        <div class="col-md-12">
                                            <textarea name="textarea<?=$question->id?>" id="textarea<?=$question->id?>" <?= $question->require ? 'required="required"' : '' ?> class="form-control" onchange="$.get('/page/default/set-values', {'id':<?=$question->id?>, 'turn': <?=$turn?>, 'value':$('#textarea<?=$question->id?>').val()}, function(data){} );" rows="1"><?=$session['answers'][$turn - 1]['answer']?></textarea>
                                        </div>
                            <?php
                                    }
                                    if($question->type == 2)
                                    {
                            ?>
                                        <div class="col-md-12">
                                            <input type="number" id="number<?=$question->id?>" name="number<?=$question->id?>" value="<?=$session['answers'][$turn - 1]['answer']?>" onchange="$.get('/page/default/set-values', {'id':<?=$question->id?>, 'turn': <?=$turn?>, 'value':$('#number<?=$question->id?>').val()}, function(data){} );" <?= $question->require ? 'required="required"' : '' ?> class="validate[required,maxSize[8] form-control">
                                        </div>
                            <?php
                                    }
                                    if($question->type == 3)
                                    {
                            ?>
                                        <?php
                                            $i = 0;
                                            foreach (json_decode($question->individual) as $value) 
                                            { 
                                                $i++;
                                                if( $session['answers'][$turn - 1]['answer'] == $i ) $checked = 'checked=""';
                                                else $checked = '';
                                        ?>
                                            <div class="row">
                                                <div class="col-md-1 col-xs-1 col-sm-1">
                                                    <input type="radio" name="radio<?=$question->id?>[]" <?=$checked?> value="<?=$value->value?>" <?= $question->require ? 'required="required"' : '' ?> style="width:20px;height:20px;" 
                                                        onchange="
                                                            var cboxes = document.getElementsByName('radio<?=$question->id?>[]');
                                                            var len = cboxes.length;
                                                            for (var i=0; i<len; i++) {
                                                                if( cboxes[i].checked ){
                                                                    $.get('/page/default/set-values', {'id':<?=$question->id?>, 'turn': <?=$turn?>, 'value': i + 1}, function(data){} );
                                                                }
                                                            }
                                                        "
                                                    >
                                                </div>
                                                <div class="col-md-11 col-xs-11 col-sm-11">
                                                    <label class="control-label"><?=$value->value?></label>
                                                </div>
                                            </div>
                                        <?php } ?>
                            <?php
                                    }
                                    if($question->type == 4)
                                    {
                            ?>
                                        <?php
                                            $i = 0; 
                                            foreach (json_decode($question->multiple) as $value) { 
                                                $i++;
                                                $array = explode(',', $session['answers'][$turn - 1]['answer']);
                                                if( in_array($i, $array) ) $checked = 'checked=""';
                                                else $checked = '';
                                        ?>
                                            <div class="row">
                                                <div class="col-md-1 col-sm-1 col-xs-1">
                                                    <input type = "checkbox" <?=$checked?> name="checkbox<?=$question->id?>[]" 
                                                    onchange=" 
                                                        var cboxes = document.getElementsByName('checkbox<?=$question->id?>[]');
                                                        var len = cboxes.length; var y = 0; var result = '';
                                                        for (var i=0; i<len; i++) {
                                                            if( cboxes[i].checked ){
                                                                var id = i + 1;
                                                                if(y === 0) result = id;
                                                                else result += ',' + id;
                                                                y++;
                                                            }
                                                        }
                                                        $.get('/page/default/set-values', {'id':<?=$question->id?>, 'turn': <?=$turn?>, 'value':result}, function(data){} );
                                                    " 
                                                    value="<?=$value->question?>" <?= $question->require ? 'required="required"' : '' ?> style="width:20px; height:20px;" >
                                                </div>
                                                <label style="margin-top: 5px;" class="col-md-11 control-label"><?=$value->question?></label>
                                            </div>
                                        <?php } ?>
                            <?php
                                    }
                                    if($question->type == 5)
                                    {
                            ?>
                                        <div class="col-md-12">
                                            <input type="date" name="date<?=$question->id?>" id="date<?=$question->id?>" value="<?=$session['answers'][$turn - 1]['answer']?>" onchange="$.get('/page/default/set-values', {'id':<?=$question->id?>, 'turn': <?=$turn?>, 'value':$('#date<?=$question->id?>').val()}, function(data){} );" <?= $question->require ? 'required="required"' : '' ?> class="form-control" name="date[]">
                                        </div>                     
                            <?php       } 
                                    if($question->type == 6)
                                    {
                            ?>
                                    <div class="col-md-12">
                                        <center>
                                            <div id="logo_file<?=$question->id?>">
                                                <img style="width:250px; height:250px;"  src="<?=$path?>">
                                            </div>
                                            <br>
                                            <div id="logo<?=$question->id?>"></div>
                                            <button style="display:block;width:120px; height:30px;" name="button23<?=$question->id?>[]" id="button23<?=$question->id?>" onclick="document.getElementById('fileInput<?=$question->id?>').click()">Загрузить аватар</button>
                                            <input type="file" name="fileInput<?=$question->id?>[]" style="display:none" id="fileInput<?=$question->id?>" class="poster23_image<?=$question->id?>" accept="image/*" />
                                        </center>
                                    </div> 
                            <?php
                                    }
                            ?>
                        
                        </div>
                    </div>     
                    <div class="panel-footer">
                        <?php /*$turn > 1 ? Html::a( Questionary::getPreviousArrow() . 'Предыдущий' , ['/anketa/test', 'turn' => $turn - 1], [ 'class' => 'btn btn-info' ]) : ''*/ ?>
                        <?php if($turn > 1) { ?>
                            <a class="btn btn-info" onclick="$.get('/page/default/change', {'id' : '-1', 'turn' : <?=$turn - 1?> }, function(data){ $.pjax.reload({container:'#test-pjax', async: false}); $.pjax.reload({container:'#question-pjax', async: false}); } );" >
                                <?= Questionary::getPreviousArrow() . 'Предыдущий' ?>
                            </a>
                        <?php } ?>
                        <?php /*$session['max_test_count'] > $turn ? Html::a( 'Следующий' . Questionary::getNextArrow() , ['/anketa/test', 'turn' => $turn + 1], [ 'class' => 'btn btn-info pull-right' ]) : ''*/ ?>
                        <?php if($session['max_test_count'] > $turn) { ?>
                            <a class="btn btn-info pull-right"  onclick="$.get('/page/default/change', {'id' : '-1', 'turn' : <?=$turn + 1?> }, function(data){ $.pjax.reload({container:'#test-pjax', async: false}); $.pjax.reload({container:'#question-pjax', async: false}); } );" >
                                <?= ' Следующий' . Questionary::getNextArrow() ?>
                            </a>
                        <?php } ?>
                    </div>                            
                </div>
            </div>
            <?php Pjax::end() ?>
            <?php Pjax::begin(['enablePushState' => false, 'id' => 'test-pjax']) ?>
                <div class="col-md-3">
                    <div class="panel panel-default">
                        <div class="panel-body" style="background-color: #3FC5FA; color: white;">
                            <div class="profile-controls">
                                <a class="profile-control-right" style="cursor: pointer;" title="Закончить тест" onclick="$.get('/page/default/end', {'end' : '1' }, function(data){ $.pjax.reload({container:'#test-pjax', async: false}); $.pjax.reload({container:'#question-pjax', async: false}); } );" >
                                    <span class="glyphicon glyphicon-off" style="font-size: 25px; color: white;"></span>
                                </a>
                                <div class="widget-big-int pull-right" style="margin-left: 50px; font-size: 18px; font-weight: bold; color:#fff;" id="demo"></div>
                                <!-- <div class="widget-big-int plugin-clock pull-right" style="font-size: 20px">19<span>:</span>34</div> -->
                            </div>
                        </div>                                
                        <div class="panel-body" style="background-color: #3FC5FA; min-height: 500px;">   
                            <ul class="test-count">
                                <?php
                                    $i = 0;
                                    foreach ($session['answers'] as $value) {
                                        $button = '';
                                        $i++;
                                        if($value['answer'] !== null) $button = 'btn-success';
                                ?>
                                <li style="color: #fff;font-weight: bold;">
                                    <a class="btn test_button <?=$button?>"  onclick="$.get('/page/default/change', {'id' : <?=$value['question_id']?>, 'turn' : <?=$i?> }, function(data){ $.pjax.reload({container:'#test-pjax', async: false}); $.pjax.reload({container:'#question-pjax', async: false}); } );" >
                                        <?=$i?>
                                    </a>
                                        &nbsp; <?=$i?> - вопрос
                                    <?php // Html::a( $i, ['/anketa/test', 'turn' => $i], [ 'class' => 'btn test_button ' . $button ]) ?>
                                    <!-- &nbsp; <?php //$i?> - вопрос -->
                                </li>
                                <br>
                                <?php
                                    }
                                ?>
                            </ul>  
                            <br>
                            <div class="present-lod">
                            <b>Пройдено:</b>
                            </div>
                             <div class="loading-pres">
                                <div class="progress-bar progress-bar-striped progress-bar-success active" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: <?=$session['process']?>%"><?=$session['process']?>%</div>
                            </div> 
                        </div>
                    </div>
                </div>
                <?php Pjax::end() ?>
        </div>
    </div>
</div>
</div>

<?php 
$this->registerJs(<<<JS

    var fileCollection = new Array();

    /*Загрузка файла*/

    $(function(){
        //file input field trigger when the drop box is clicked
        $("#logo$question->id").click(function(){
            $("#fileInput$question->id").click();
        });
        
        //prevent browsers from opening the file when its dragged and dropped
        $(document).on('drop dragover', function (e) {
            e.preventDefault();
        });

        //call a function to handle file upload on select file
        $('input[name^=\'fileInput$question->id\']').on('change', fileUpload);
    });

    function fileUpload(event){
        //notify user about the file upload status
        $("#logo$question->id").html(event.target.value+" загрузка...");
        
        //get selected file
        files = event.target.files;
        
        //form data check the above bullet for what it is  
        var data = new FormData();                                   

        //file data is presented as an array
        for (var i = 0; i < files.length; i++) {
            var file = files[i];
            if(!file.type.match('image.*')) {              
                //check file type
                $("#logo$question->id").html("Пожалуйста, выберите файл изображения.");
            }else{
                //append the uploadable file to FormData object
                data.append('file', file, file.name);
                data.append('_csrf', '{$_csrf}');
                data.append('id', '{$question->id}');
                data.append('turn', '{$turn}');
                
                //create a new XMLHttpRequest
                var xhr = new XMLHttpRequest();     
                
                //post file data for upload
                xhr.open('POST', '/page/default/set-logo', true);  
                xhr.send(data);
                xhr.onload = function () {
                    //get response and show the uploading status
                    var response = JSON.parse(xhr.responseText);
                    if(xhr.status === 200 && response.status == 'ok'){
                        $("#logo$question->id").html("Файл был успешно загружен. Нажмите, чтобы загрузить другой.");
                    }else {
                        $("#logo$question->id").html("Возникла проблема. Пожалуйста, попробуйте еще раз.");
                    }
                };
            }
        }
    }

    $(document).on('change', '.poster23_image$question->id', function(e){
        var files = e.target.files;
        $.each(files, function(i, file){
            fileCollection.push(file);
            var reader = new FileReader();
            reader.readAsDataURL(file);
            reader.onload = function(e){
                var template = '<img style="width:250px; height:250px;" src="'+e.target.result+'"> ';
                $('#logo_file$question->id').html('');
                $('#logo_file$question->id').append(template);
            };
        });
    });

JS
);
?>


<input type="hidden" id="time" name="time" value="<?=time()?>">
    <script>
    // Set the date we're counting down to
    var countDownDate = <?=$session['test_time']?>;
    // Update the count down every 1 second
    var x = setInterval(function() {

      // Get todays date and time
      $.get('/page/default/get-time',{ },function(data) { $('#time').val(data); } );
      var now = document.getElementById('time').value;
      //alert(countDownDate + ' ' + now);
      //alert(now);
      // Find the distance between now and the count down date
      var distance = (countDownDate - now) *1000;

      // Time calculations for days, hours, minutes and seconds
      //var days = Math.floor(distance / (1000 * 60 * 60 * 24));
      var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
      var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
      var seconds = Math.floor((distance % (1000 * 60)) / 1000);

      // Display the result in the element with id="demo"
      document.getElementById("demo").innerHTML = hours + "ч "
      + minutes + "м " + seconds + "с ";

      // If the count down is finished, write some text 
      if (distance < 0) {
        clearInterval(x);
        document.getElementById("demo").innerHTML = "Время закончено";
        $.get('/page/default/end', {'end' : '1' }, function(data){ $.pjax.reload({container:'#test-pjax', async: false}); $.pjax.reload({container:'#question-pjax', async: false}); } );
        //window.location.href = "/page/default/test?end=1";
      }
    }, 1000);
    </script>
