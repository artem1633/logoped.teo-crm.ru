<?php

namespace app\widgets;

use app\models\users\Users;
use Yii;
use app\widgets\assets\UserSettingSwitcherAsset;
use yii\base\InvalidConfigException;
use yii\base\Widget;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\HttpException;

/**
 * Class UserSettingSwitcher
 * @package app\widgets
 */
class UserSettingSwitcher extends Widget
{
    /**
     * @var string
     */
    public $attribute;

    /**
     * @var string
     */
    public $url;

    /**
     * @var string
     */
    public $label;

    /**
     * @var int
     */
    public $userId;

    /**
     * @var boolean
     */
    public $disabled = false;

    /**
     * @var array
     */
    public $labelOptions = [
        'class' => 'checkbox-container',
    ];

    private $userSettings;

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();

        if($this->url == null)
        {
            if($this->userId == null){
                $this->url = Url::toRoute(['users/set-setting-value-ajax']);
            } else {
                $this->url = Url::toRoute(['users/set-setting-value-ajax', 'userId' => $this->userId]);
            }
        } else {
            $this->url = Url::toRoute($this->url);
        }

        if($this->attribute == null)
        {
            throw new InvalidConfigException('Invalid configuration');
        }

        if($this->userId == null){
            $this->userSettings = Yii::$app->user->identity->settings;
        } else {
            $user = Users::findOne($this->userId);

            if($user == null){
                throw new HttpException(500, 'User not found');
            }

            $this->userSettings = $user->settings;
        }
    }

    /**
     * {@inheritdoc}
     */
    public function run()
    {
        $attribute = $this->attribute;
        $value = $this->userSettings->$attribute == 1 ? true : false;

        if($this->disabled){
            $this->labelOptions['class'] = 'checkbox-container checkbox-disabled';
        }

        $checkbox = Html::checkbox($this->attribute, $value, ['class' => 'checkbox-switch', 'data-url' => $this->url, 'disabled' => $this->disabled]).'<span class="checkmark"></span>';

        $html = Html::tag('label', '<span class="checkbox-label">'.$this->label.'</span>'.$checkbox, $this->labelOptions);

        UserSettingSwitcherAsset::register(Yii::$app->view);
        return $html;
    }
}