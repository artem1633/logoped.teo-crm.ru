<?php

namespace app\widgets\assets;

use yii\web\AssetBundle;

class UserSettingSwitcherAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@widgets';

    public $js = [
        'js/user-setting-switcher.js'
    ];

    public $css = [
        'css/user-setting-switcher.css'
    ];
}