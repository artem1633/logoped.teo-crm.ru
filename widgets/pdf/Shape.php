<?php

namespace app\widgets\pdf;

/**
 * Class Shape
 * @package app\widgets\pdf
 */
class Shape extends BasePdfWidget
{
    public $directory = 'pdf_data/shapes/';
    public $topMax = 100;
    public $leftMax = 150;

    public $top = null;
    public $left = null;

    /**
     * {@inheritdoc}
     */
    public function run()
    {
        parent::run();

        if($this->top == null){
            $top = rand(0, $this->topMax);
        } else {
            $top = $this->top;
        }
        if($this->left == null){
            $left = rand(0, $this->leftMax);
        } else {
            $left = $this->left;
        }

        $path = $this->directory.'shape-'.rand(1, 20).'.png';

        return '<div style="background-image: url(\''.$path.'\'); background-size: 100%; width: 200px; height: 200px; position: absolute; top: '.$top.'mm; left: '.$left.'mm;"></div>';
    }

}