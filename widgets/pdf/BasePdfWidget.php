<?php

namespace app\widgets\pdf;

use yii\base\Widget;

/**
 * Class BasePdfWidget
 * @package app\widgets\pdf
 */
class BasePdfWidget extends Widget
{
    public $top = null;
    public $left = null;

    /**
     * Генерирует необходимое число ($count) виджетов
     * @param int $count
     * @param array $config
     * @return string
     */
    public static function generate($count, $config = [])
    {
        $output = '';

        for ($i = 0; $i < $count; $i++){
            $output .= self::widget($config);
        }

        return $output;
    }
}