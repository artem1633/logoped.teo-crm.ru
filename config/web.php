<?php

$params = require __DIR__ . '/params.php';
$db = require __DIR__ . '/db.php';

$config = [
    'id' => 'basic',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'language' => 'ru-RU',
    'name' => 'Logoped',
    'timeZone' => 'Europe/Moscow',
    'defaultRoute' => '/site/index',
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm-asset',
        '@widgets' => 'widgets'
    ],
    'modules' => [
        'gridview' =>  [
            'class' => '\kartik\grid\Module'
        ],
        'page' => [
            'class' => 'app\modules\page\Admin',
        ],
        'api' => [
            'class' => 'app\modules\api\Api',
        ],
        'admin' => [
            'class' => 'app\modules\admin\Admin',
        ],
    ],
    'components' => [
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => 'JWJ1zc3fLI4fg2xS8uQzcM8aHDJ2WdFo',
        ],
        'fonts' => [
            'class' => 'app\components\Fonts',
            'fontsPath' => 'css/fonts.css'
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'view' => [
             'theme' => [
                 'pathMap' => [
                    //'@app/views' => '@vendor/dmstr/yii2-adminlte-asset/example-views/yiisoft/yii2-app'
                    '@app/views' => '@app/views/yii2-app'
                 ],
             ],
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            'useFileTransport' => false,
            'transport' => [
                'class' => 'Swift_SmtpTransport',
                'host' => 'smtp.mail.ru',
                'username' => 'logoped.service@mail.ru',
                'password' => 'L0j:2h0v1',
                'port' => '465',
                'encryption' => 'ssl',
            ],
        ],
        'user' => [
            'identityClass' => 'app\models\users\User',
            'enableAutoLogin' => true,
            'loginUrl' => '/site/login'
        ],
        'errorHandler' => [
            'errorAction' => 'site/avtorizatsiya',
        ],
        'formatter' => [
            'class'    => 'yii\i18n\Formatter',
            'defaultTimeZone' => 'Europe/Moscow',
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'smsRu' => [
            'class' => 'app\services\SmsRu',
        ],
        'db' => $db,
        
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
          'rules' => [
//                [
//                    'pattern' => '<link>',
//                    'route' => 'page',
//                ],
//                'company/page'=> 'page/default/company',
//                'anketa/test'=> '/page/default/test',
              'admin/<controller>/<action>' => 'admin/<controller>/<action>',
           ],
        ],
        
    ],
    'params' => $params,
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        'allowedIPs' => ['127.0.0.1', '::1'],
    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        'allowedIPs' => ['*'],
    ];
}

return $config;
