<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\FixLettersImages */
?>
<div class="fix-letters-images-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'symbol',
        ],
    ]) ?>

</div>
