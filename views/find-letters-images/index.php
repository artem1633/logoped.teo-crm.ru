<?php
use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\Modal;
use kartik\grid\GridView;
use johnitvn\ajaxcrud\CrudAsset; 
use johnitvn\ajaxcrud\BulkButtonWidget;

/* @var $this yii\web\View */
/* @var $searchModel app\models\FindLettersImagesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Материалы пособия «Найди букву»';
$this->params['breadcrumbs'][] = $this->title;

CrudAsset::register($this);

?>
<div class="fix-letters-images-index">

    <div class="col-md-12">
        <div class="panel panel-success" style="margin-bottom: 25px;">
            <div class="panel-heading">
                <div class="panel-title">Поиск</div>
            </div>
            <div class="panel-body">
                <?php $form = \kartik\form\ActiveForm::begin(['method' => 'get']) ?>
                    <div class="col-md-10">
                        <?= $form->field($searchModel, 'symbol')->textInput() ?>
                    </div>
                    <div class="col-md-2">
                        <?= Html::submitButton('Поиск', ['class' => 'btn btn-success btn-block', 'style' => 'margin-top: 21px;']) ?>
                    </div>
                <?php \kartik\form\ActiveForm::end() ?>
            </div>
        </div>
    </div>

    <?php foreach ($dataProvider->models as $model): ?>
        <div class="col-md-2">
            <a href="<?=Url::toRoute(['update', 'id' => $model->id])?>" role="modal-remote">
                <div class="panel panel-success" style="margin-bottom: 20px;">
                    <div class="panel-heading"></div>
                    <div class="panel-body">
                        <h2><?=$model->symbol?></h2>
                        <?php
                        $smallWrote = $model->getWroteDownAttachmentsCount();
                        $bigWrote = $model->getWroteUpAttachmentsCount();
                        $smallPrinted = $model->getPrintedDownAttachmentsCount();
                        $bigPrinted = $model->getPrintedUpAttachmentsCount();
                        ?>
                        <p style="margin: 0; margin-top: 10px; font-weight: 600; color: #000;">Печатные</p>
                        <p class="<?=$bigPrinted == 0 ? 'text-danger' : 'text-success'?>" style="margin-top: 5px; margin-bottom: 2px;">Большие: <?=$bigPrinted?></p>
                        <p class="<?=$smallPrinted == 0 ? 'text-danger' : 'text-success'?>" style="margin-bottom: 2px;">Маленькие: <?=$smallPrinted?></p>
                        <p style="margin: 0; margin-top: 5px; font-weight: 600; color: #000;">Прописные</p>
                        <p class="<?=$bigWrote == 0 ? 'text-danger' : 'text-success'?>" style="margin-top: 5px; margin-bottom: 2px;">Большие: <?=$bigWrote?></p>
                        <p class="<?=$smallWrote == 0 ? 'text-danger' : 'text-success'?>" style="margin-bottom: 2px;">Маленькие: <?=$smallWrote?></p>
                    </div>
                </div>
            </a>
        </div>
    <?php endforeach; ?>
</div>
<?php Modal::begin([
    "id"=>"ajaxCrudModal",
    "footer"=>"",// always need it for jquery plugin
])?>
<?php Modal::end(); ?>
