<?php
use kartik\file\FileInput;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\forms\FindAttachmentsForm */
/* @var $form yii\widgets\ActiveForm */

$attachmentsWroteDown = \yii\helpers\ArrayHelper::getColumn(\app\models\FindLettersImagesAttachments::find()
    ->where(['find_letter_image_id' => $model->findLetterImageId, 'type' => \app\models\FindLettersImagesAttachments::TYPE_WROTE_DOWN])->all(), 'path');
for ($i = 0; $i < count($attachmentsWroteDown); $i++){
    $attachmentsWroteDown[$i] = '/'.$attachmentsWroteDown[$i];
}

$attachmentsWroteUp = \yii\helpers\ArrayHelper::getColumn(\app\models\FindLettersImagesAttachments::find()
    ->where(['find_letter_image_id' => $model->findLetterImageId, 'type' => \app\models\FindLettersImagesAttachments::TYPE_WROTE_UP])->all(), 'path');
for ($i = 0; $i < count($attachmentsWroteUp); $i++){
    $attachmentsWroteUp[$i] = '/'.$attachmentsWroteUp[$i];
}

$attachmentsPrintedUp = \yii\helpers\ArrayHelper::getColumn(\app\models\FindLettersImagesAttachments::find()
    ->where(['find_letter_image_id' => $model->findLetterImageId, 'type' => \app\models\FindLettersImagesAttachments::TYPE_PRINTED_UP])->all(), 'path');
for ($i = 0; $i < count($attachmentsPrintedUp); $i++){
    $attachmentsPrintedUp[$i] = '/'.$attachmentsPrintedUp[$i];
}

$attachmentsPrintedDown = \yii\helpers\ArrayHelper::getColumn(\app\models\FindLettersImagesAttachments::find()
    ->where(['find_letter_image_id' => $model->findLetterImageId, 'type' => \app\models\FindLettersImagesAttachments::TYPE_PRINTED_DOWN])->all(), 'path');
for ($i = 0; $i < count($attachmentsPrintedDown); $i++){
    $attachmentsPrintedDown[$i] = '/'.$attachmentsPrintedDown[$i];
}


?>

<div class="fix-letters-images-form">

    <?php $form = ActiveForm::begin(); ?>

    <ul class="nav nav-pills" style="margin: 5px 0;">
        <li class="active" style="width: 49%; text-align: center;"><a data-toggle="tab" href="#panel1">Прописные</a></li>
        <li style="width: 49%; text-align: center;"><a data-toggle="tab" href="#panel2">Печатные</a></li>
    </ul>

    <div class="tab-content">
        <div id="panel1" class="tab-pane fade in active">
            <?= $form->field($model, 'wroteUpAttachments[]')->widget(FileInput::class,[
                'options' => ['multiple' => true,],
                'pluginOptions' => [
                    'layoutTemplates' => [
                        'actionDownload' => '<a class="{downloadClass}" href="{downloadUrl}" target="_blank" data-pjax="0" title="Загрузить" download="{caption}"><i class="glyphicon glyphicon-download"></i></a> '
                    ],
                    'uploadUrl' => \yii\helpers\Url::toRoute(['upload-attachment', 'id' => $model->findLetterImageId]),
                    'initialPreview' => $attachmentsWroteUp,
                    'previewFileType' => 'any',
                    'overwriteInitial' => false,
                    'initialPreviewAsData' => true,
                    'preferIconicPreview' => true,
//            'initialPreviewConfig' => $previewConfig,
//            'initialPreviewDownloadUrl' => \yii\helpers\Url::to('uploads/' . $task->id . '/{filename}', true),
                ],
            ]) ?>

            <?= $form->field($model, 'wroteDownAttachments[]')->widget(FileInput::class,[
                'options' => ['multiple' => true,],
                'pluginOptions' => [
                    'layoutTemplates' => [
                        'actionDownload' => '<a class="{downloadClass}" href="{downloadUrl}" target="_blank" data-pjax="0" title="Загрузить" download="{caption}"><i class="glyphicon glyphicon-download"></i></a> '
                    ],
                    'uploadUrl' => \yii\helpers\Url::toRoute(['upload-attachment', 'id' => $model->findLetterImageId]),
                    'initialPreview' => $attachmentsWroteDown,
                    'previewFileType' => 'any',
                    'overwriteInitial' => false,
                    'initialPreviewAsData' => true,
                    'preferIconicPreview' => true,
//            'initialPreviewConfig' => $previewConfig,
//            'initialPreviewDownloadUrl' => \yii\helpers\Url::to('uploads/' . $task->id . '/{filename}', true),
                ],
            ]) ?>
        </div>
        <div id="panel2" class="tab-pane fade">
            <?= $form->field($model, 'printedUpAttachments[]')->widget(FileInput::class,[
                'options' => ['multiple' => true,],
                'pluginOptions' => [
                    'layoutTemplates' => [
                        'actionDownload' => '<a class="{downloadClass}" href="{downloadUrl}" target="_blank" data-pjax="0" title="Загрузить" download="{caption}"><i class="glyphicon glyphicon-download"></i></a> '
                    ],
                    'uploadUrl' => \yii\helpers\Url::toRoute(['upload-attachment', 'id' => $model->findLetterImageId]),
                    'initialPreview' => $attachmentsPrintedUp,
                    'previewFileType' => 'any',
                    'overwriteInitial' => false,
                    'initialPreviewAsData' => true,
                    'preferIconicPreview' => true,
//            'initialPreviewConfig' => $previewConfig,
//            'initialPreviewDownloadUrl' => \yii\helpers\Url::to('uploads/' . $task->id . '/{filename}', true),
                ],
            ]) ?>

            <?= $form->field($model, 'printedDownAttachments[]')->widget(FileInput::class,[
                'options' => ['multiple' => true,],
                'pluginOptions' => [
                    'layoutTemplates' => [
                        'actionDownload' => '<a class="{downloadClass}" href="{downloadUrl}" target="_blank" data-pjax="0" title="Загрузить" download="{caption}"><i class="glyphicon glyphicon-download"></i></a> '
                    ],
                    'uploadUrl' => \yii\helpers\Url::toRoute(['upload-attachment', 'id' => $model->findLetterImageId]),
                    'initialPreview' => $attachmentsPrintedDown,
                    'previewFileType' => 'any',
                    'overwriteInitial' => false,
                    'initialPreviewAsData' => true,
                    'preferIconicPreview' => true,
//            'initialPreviewConfig' => $previewConfig,
//            'initialPreviewDownloadUrl' => \yii\helpers\Url::to('uploads/' . $task->id . '/{filename}', true),
                ],
            ]) ?>
        </div>
    </div>


    <?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>
