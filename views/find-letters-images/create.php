<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\FixLettersImages */

?>
<div class="fix-letters-images-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
