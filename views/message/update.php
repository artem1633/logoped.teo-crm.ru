<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Messages */
?>
<div class="messages-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
