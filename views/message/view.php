<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Messages */
?>
<div class="messages-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'text_message',
            'start_date',
            'end_date',
            'button1',
            'button2',
        ],
    ]) ?>

</div>
