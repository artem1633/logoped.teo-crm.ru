<?php
use app\models\FixLettersImages;
use app\models\FixLettersImagesAttachments;

/** @var \yii\web\View $this */
/** @var \app\models\FixLetter $model */

$symbols = $model->fixLetterData;
$symbolsCount = count($symbols);

$letters = $model->bigLetterlist;

?>

<?php for($p = 0; $p < $model->page_count; $p++){ ?>
    <?php for($i = 0; $i < 30; $i++): ?>
        <div style="display: inline-block; float: left; text-align: center; width: 30mm; border: 1px solid #cecece; font-size: 45px; padding: 10px;">
            <span>&nbsp;</span>
        </div>
    <?php endfor; ?>

    <?php for($r = 1, $i = 1, $g = 0; $g < $symbolsCount; $i++, $g++){
        if($r == 1){
            $top = 20;
            $wTop = 20;
        } else {
            $top = $r * 21;
            $wTop = $r * 21;
        }
        if($i == 1){
            $left = 29;
            $wLeft = 29;
        } else if($i == 2) {
            $left = 66;
            $wLeft = 66;
        }
        else if($i == 3) {
            $left = 102;
            $wLeft = 102;
        }
        else if($i == 4) {
            $left = 137;
            $wLeft = 137;
        }
        else if($i == 5) {
            $left = 174;
            $wLeft = 174;
            $r++;
            $i = 0;
        }

        if (in_array($symbols[$g]->symbol, [7, 13, 25])){ // индекс Буквы Ж, М, Ш
            $left -= 2;
        }

        $wLeft += rand(-2, 4);
        $wTop += rand(4, 10);

        $format = $symbols[$g]->type;
        if(in_array($format, [0, 2])) {
            $lettersList = $model->bigLetterlist;
            $wHeight = 6;
            $symbol = mb_strtoupper($symbols[$g]->symbol);
        } else if(in_array($format, [1, 3])) {
            $lettersList = $model->smallLetterlist;
            $wHeight = 3;
            $symbol = mb_strtolower($symbols[$g]->symbol);
        }

        if(in_array($format, [0, 1])) {
            $drawing = FixLettersImagesAttachments::DRAWING_PRINTED;
        } else if(in_array($format, [2, 3])) {
            $drawing = FixLettersImagesAttachments::DRAWING_WROTE;
        }

        if($model->complexity == \app\models\Letter::LETTERS_EASY){
            $type = FixLettersImagesAttachments::TYPE_EASY;
        } else if ($model->complexity == \app\models\Letter::LETTERS_MEDIUM){
            $type = FixLettersImagesAttachments::TYPE_MIDDLE;
        } else if ($model->complexity == \app\models\Letter::LETTERS_COMPLEX){
            $type = FixLettersImagesAttachments::TYPE_HARD;
        }

        /** @var FixLettersImages $image */
        $image = FixLettersImages::find()->where("symbol REGEXP :symbol", [':symbol' => $symbol])->one();

        $imageAttachment = \yii\helpers\ArrayHelper::getColumn($image->getFixLettersImagesAttachments()->andWhere(['type' => $type, 'drawing' => $drawing])->all(), 'path');


        $imgCount = rand(0, count($imageAttachment)-1);
        $img = $imageAttachment[$imgCount];
        ?>
        <div style="font-size: 45px; position: absolute; top: <?=$top?>mm; left: <?=$left?>mm; font-family: RobotoRegular;">
            <img src="<?=$img?>" alt="">
        </div>
        <div style="position: absolute; top: <?=$wTop?>mm; left: <?=$wLeft?>mm; background: rgba(255, 255, 255, 1); width: 4mm; height: 4mm; display: none;"></div>
        <div style="position: absolute; top: <?=$wTop?>mm; left: <?=$wLeft?>mm; background: rgba(255, 255, 255, 1); width: 10mm; height: <?=$wHeight?>mm; display: none;"></div>
    <?php } ?>
<p style="position: absolute; bottom: 0; right: 10px; font-size: 13px;">ЛИСТ №<?=$model->id?>/<?=$p+1?></p>
<?php if($p < ($model->page_count - 1)): ?>
        <pagebreak>
    <?php endif; ?>
<?php } ?>

<pagebreak>

<?php for($p = 0; $p < $model->page_count; $p++){ ?>
    <?php for($i = 0; $i < 30; $i++): ?>
        <div style="display: inline-block; float: left; text-align: center; width: 30mm; border: 1px solid #cecece; font-size: 45px; padding: 10px;">
            <span>&nbsp;</span>
        </div>
    <?php endfor; ?>

    <?php for($r = 1, $i = 1, $g = 0; $g < $symbolsCount; $i++, $g++){
        if($r == 1){
            $top = 20;
            $wTop = 20;
        } else {
            $top = $r * 21;
            $wTop = $r * 21;
        }
        if($i == 1){
            $left = 29;
            $wLeft = 29;
        } else if($i == 2) {
            $left = 66;
            $wLeft = 66;
        }
        else if($i == 3) {
            $left = 102;
            $wLeft = 102;
        }
        else if($i == 4) {
            $left = 137;
            $wLeft = 137;
        }
        else if($i == 5) {
            $left = 174;
            $wLeft = 174;
            $r++;
            $i = 0;
        }

        if (in_array($symbols[$g]->symbol, [7, 13, 25])){ // индекс Буквы Ж, М, Ш
            $left -= 2;
        }

        $wLeft += rand(-2, 4);
        $wTop += rand(4, 10);

        $format = $symbols[$g]->type;
        if(in_array($format, [0, 2])) {
            $lettersList = $model->bigLetterlist;
            $wHeight = 6;
            $symbol = mb_strtoupper($symbols[$g]->symbol);
        } else if(in_array($format, [1, 3])) {
            $lettersList = $model->smallLetterlist;
            $wHeight = 3;
            $symbol = mb_strtolower($symbols[$g]->symbol);
        }

        if(in_array($format, [0, 1])) {
            $drawing = FixLettersImagesAttachments::DRAWING_PRINTED;
        } else if(in_array($format, [2, 3])) {
            $drawing = FixLettersImagesAttachments::DRAWING_WROTE;
        }

        if($model->complexity == \app\models\Letter::LETTERS_EASY){
            $type = FixLettersImagesAttachments::TYPE_EASY;
        } else if ($model->complexity == \app\models\Letter::LETTERS_MEDIUM){
            $type = FixLettersImagesAttachments::TYPE_MIDDLE;
        } else if ($model->complexity == \app\models\Letter::LETTERS_COMPLEX){
            $type = FixLettersImagesAttachments::TYPE_HARD;
        }

        $symbol = mb_strtolower($symbol);

        $img = "standartFixLetters/$symbol.png";
        ?>
        <div style="font-size: 45px; position: absolute; top: <?=$top?>mm; left: <?=$left?>mm; font-family: RobotoRegular;">
            <img src="<?=$img?>" alt="">
        </div>
        <div style="position: absolute; top: <?=$wTop?>mm; left: <?=$wLeft?>mm; background: rgba(255, 255, 255, 1); width: 4mm; height: 4mm; display: none;"></div>
        <div style="position: absolute; top: <?=$wTop?>mm; left: <?=$wLeft?>mm; background: rgba(255, 255, 255, 1); width: 10mm; height: <?=$wHeight?>mm; display: none;"></div>
    <?php } ?>
    <p style="position: absolute; bottom: 0; right: 10px; font-size: 13px;">ОТВЕТ НА ЛИСТ №<?=$model->id?>/<?=$p+1?></p>
    <?php if($p < ($model->page_count - 1)): ?>
        <pagebreak>
    <?php endif; ?>
<?php } ?>