<?php

use app\models\FindLetterData;
use app\widgets\pdf\Shape;

/** @var \app\models\FindLetter $model */


/**
 * @param int $min
 * @param int $max
 * @param int $compareDiff
 * @param array $array
 * @return int;
 */
function generateRecursive($min, $max, $array, $compareDiff)
{
    $mainValue = rand($min, $max);

    foreach ($array as $value) {
        $moduleDiff = abs($mainValue - $value);
        if ($moduleDiff <= $compareDiff) {
            generateRecursive($min, $max, $array, $compareDiff);
        }


        return $mainValue;
    }
}


    ?>

<?php for($p = 0; $p < $model->page_count; $p++){ ?>
    <div style="display: none;">

    </div>

    <div style="position: absolute;">
        <?php $index = rand(0, 2) ?>
        <img src="pdf_data/noises/bg/<?=$index?>.png" style="" alt="">
    </div>
    <div style="position: absolute;">
        <?php $index = rand(0, 2) ?>
        <img src="pdf_data/noises/1/<?=$index?>.png" style="" alt="">
    </div>
    <div style="position: absolute;">
        <?php $index = rand(0, 2) ?>
        <img src="pdf_data/noises/2/<?=$index?>.png" style="" alt="">
    </div>
    <div style="position: absolute;">
        <?php $index = rand(0, 2) ?>
        <img src="pdf_data/noises/3/<?=$index?>.png" style="" alt="">
    </div>
    <div style="position: absolute;">
        <?php $index = rand(0, 1) ?>
        <img src="pdf_data/noises/4/<?=$index?>.png" style="" alt="">
    </div>
    <div style="position: absolute;">
        <?php $index = rand(0, 1) ?>
        <img src="pdf_data/noises/6/<?=$index?>.png" style="" alt="">
    </div>

    <?php foreach ($imagesSymbols as $symbol): ?>
        <?php foreach ($symbol as $type): ?>
            <?php foreach ($type as $row): ?>
                <?php for ($c = 0; $c < $row['count']; $c++): ?>
                    <?php
                    $top = rand(20, 100);
                    $left = rand(40, 700);
                    $path = $row['paths'][rand(0, count($row['paths']) - 1)];

                    $imageHeight = key($type) == \app\models\FindLettersImagesAttachments::TYPE_PRINTED_DOWN ? '100px' : '150px';


                    ?>
                    <div style="position: absolute; top: <?= $top ?>px; left: <?= $left ?>px;">
                        <img style="display: inline-block; block; height: <?=$imageHeight?>;" src="<?= $path ?>" alt="">
                    </div>
                <?php endfor; ?>
            <?php endforeach; ?>
        <?php endforeach; ?>
    <?php endforeach; ?>
<p style="position: absolute; bottom: 0; right: 10px; font-size: 13px;">ЛИСТ №<?=$model->id?>/<?=$p+1?></p>
<?php if($p < ($model->page_count - 1)): ?>
<pagebreak>
    <?php endif; ?>
<?php } ?>




