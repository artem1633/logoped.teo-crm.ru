<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Регистрация';
?>

<div class="site-login">
	<center>
        <div class="row">
            <div class="col-md-12">
                <h1><?= Html::encode($this->title) ?> </h1>
            </div>
        </div>
    </center>
	<div class="row">
		<?php $form = ActiveForm::begin(['id' => 'login-form', 'enableClientValidation' => false], $options = ['class' => 'margin-bottom-0']); ?>
			<div class="row">
	            <div class="col-md-6" style="margin-left: 25%;">
					<?= $form->field($model, 'fio')->textInput(['autofocus' => true]) ?>
				</div>
	        </div>
	        <div class="row">
	            <div class="col-md-6" style="margin-left: 25%;">
					<?= $form->field($model, 'login')->textInput(['autofocus' => true]) ?>
				</div>            
	        </div>
	        <div class="row">
	            <div class="col-md-6" style="margin-left: 25%;">
					<?= $form->field($model, 'password')->textInput(['autofocus' => true]) ?>
				</div>
			</div>
	        <div class="row">
	            <div class="col-md-6" style="margin-left: 25%;">
					<?= $form->field($model, 'telephone')->textInput(['autofocus' => true]) ?>
				</div>
	        </div>
	        <div class="row">
	            <div class="col-md-6" style="margin-left: 25%;">
					<?= $form->field($model, 'promocode')->textInput(['autofocus' => true]) ?>
				</div>
	        </div>

			<div class="row">
				<div class="form-group">
		                <div class="col-xs-6" style="margin-left: 25%;">
							<?= Html::submitButton('Регистрация', ['class' => 'btn btn-primary', 'style' => 'width:100%', 'name' => 'login-button']) ?>
						</div>
					</div>
				</div>
			</div>
		<?php ActiveForm::end(); ?>
	</div>
</div>