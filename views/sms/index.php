<?php

/**
 * @var \yii\data\ActiveDataProvider $dataProvider
 * @var \app\models\SmsSearch $searchModel
 */

use kartik\grid\GridView;


$this->title = 'Sms-уведомления';

?>

<div class="row">
    <div class="col-md-5">
        <div class="panel panel-default">
            <div class="panel-heading">
                <img src="https://sms.ru/i/logo-blue.png?v3">
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-12">
                        <?php if(Yii::$app->smsRu->isBalanceNormal() == false): ?>
                            <div class="alert alert-danger" style="margin-bottom: 15px;">
                                Необходимо <a href="https://sms.ru/pay.php" target="_blank" style="color: #fff; text-decoration: underline;">пополнить ваш баланс</a>. Ваш баланс приближается к нулю.
                            </div>
                        <?php endif; ?>
                        <h2>Баланс: <?=number_format(Yii::$app->smsRu->balance, 2, '.', ' ')?>&#8381</h2>
                    </div>
                </div>
                <br>
                <div class="row">
                    <div class="col-md-6">
                        <h4>Ежедневный лимит: <?= Yii::$app->smsRu->limit->total_limit ?></h4>
                    </div>
                    <div class="col-md-6">
                        <h4>Отправлено сегодня: <?= Yii::$app->smsRu->limit->used_today ?></h4>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-7">
        <div class="sms-index">
            <div id="ajaxCrudDatatable">
                <?=GridView::widget([
                    'id'=>'crud-datatable',
                    'dataProvider' => $dataProvider,
//                    'filterModel' => $searchModel,
                    'pjax'=>true,
                    'columns' => require(__DIR__.'/_columns.php'),
                    'toolbar'=> '',
                    'striped' => true,
                    'condensed' => true,
                    'responsive' => true,
                    'panel' => [
                        'type' => 'primary',
                        'heading' => '<i class="glyphicon glyphicon-list"></i> Сообщения',
                    ]
                ])?>
            </div>
        </div>
    </div>
</div>

