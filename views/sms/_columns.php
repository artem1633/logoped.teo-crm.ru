<?php
use yii\helpers\Url;

return [
    [
        'class' => 'kartik\grid\CheckboxColumn',
        'width' => '20px',
    ],
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],
        // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'id',
    // ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'user_id',
        'content' => function($model){
            if($model->user_id != null){
                return \yii\helpers\Html::a($model->user->fio, ['account/index', 'id' => $model->user_id], ['data-pjax' => 0]);
            } else {
                return Yii::$app->formatter->asText(null);
            }
        }
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'phone_number',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'sms_text',
    ],
//    [
//        'class'=>'\kartik\grid\DataColumn',
//        'attribute'=>'status',
//    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'status_code',
        'label' => 'Статус',
        'content' => function($model){
            if($model->status == \app\models\Sms::STATUS_SUCCESS){
                return "<label class='label label-success'>{$model->statusCodeText}</label>";
            } else {
                return "<label class='label label-danger'>{$model->statusCodeText}</label>";
            }
        },
        'hAlign' => \kartik\grid\GridView::ALIGN_CENTER,
    ],
     [
         'class'=>'\kartik\grid\DataColumn',
         'attribute'=>'cost',
         'content' => function($model){
             return number_format($model->cost, 2, '.', ' ').'&#8381';
         },
     ],
     [
         'class'=>'\kartik\grid\DataColumn',
         'attribute'=>'created_at',
         'label' => 'Дата и время',
         'format' => ['date', 'php:d.m.Y H:i:s'],
     ],
//    [
//        'class' => 'kartik\grid\ActionColumn',
//        'dropdown' => false,
//        'vAlign'=>'middle',
//        'urlCreator' => function($action, $model, $key, $index) {
//                return Url::to([$action,'id'=>$key]);
//        },
//        'viewOptions'=>['role'=>'modal-remote','title'=>'View','data-toggle'=>'tooltip'],
//        'updateOptions'=>['role'=>'modal-remote','title'=>'Update', 'data-toggle'=>'tooltip'],
//        'deleteOptions'=>['role'=>'modal-remote','title'=>'Delete',
//                          'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
//                          'data-request-method'=>'post',
//                          'data-toggle'=>'tooltip',
//                          'data-confirm-title'=>'Are you sure?',
//                          'data-confirm-message'=>'Are you sure want to delete this item'],
//    ],

];   