<?php
use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\Modal;
use kartik\grid\GridView;
use johnitvn\ajaxcrud\CrudAsset; 
use johnitvn\ajaxcrud\BulkButtonWidget;

/* @var $this yii\web\View */
/* @var $searchModel app\models\BalancesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Баланс';
$this->params['breadcrumbs'][] = $this->title;

CrudAsset::register($this);

?>
    <div class="panel panel-default" style="margin-bottom: 5px; border-radius: 0;">
        <div class="panel-body" style="font-size: 15px;">
            В поле «Комментарий» укажите ваш промокод
        </div>
    </div>

    <iframe src="https://money.yandex.ru/quickpay/shop-widget?label=<?=Yii::$app->user->getId()?>&comment=on&writer=seller&targets=%D0%9F%D0%BE%D0%BF%D0%BE%D0%BB%D0%BD%D0%B5%D0%BD%D0%B8%D0%B5%20%D0%B1%D0%B0%D0%BB%D0%B0%D0%BD%D1%81%D0%B0&targets-hint=&default-sum=100&button-text=11&payment-type-choice=on&hint=&successURL=&quickpay=shop&account=<?= $configure->wallet ?>"
            width="100%" height="300" frameborder="0" allowtransparency="true" scrolling="no"></iframe>

<div class="balances-index">
    <div class="panel panel-default tabs">
        <ul class="nav nav-tabs nav-justified">
            <li class="active"><a href="#tab1" data-toggle="tab" style="margin-right: 5px; border-radius: 3px 3px 0px 0px;">Пополнение</a></li>
            <li><a href="#tab2" data-toggle="tab" style="margin-right: 5px; border-radius: 3px 3px 0px 0px;">Списание</a></li>
        </ul>
        <div class="panel-body tab-content">
            <div class="tab-pane active" id="tab1">
                    <?=GridView::widget([
                        'id'=>'crud-datatable-debit',
                        'dataProvider' => $dataProviderDebit,
                        'pjax'=>true,
                        'columns' => require(__DIR__ . '/_columns.php'),
                        'toolbar'=> [
                            ['content'=>
                                '<div style="margin-top:10px;">' .
                                Html::a('Добавить <i class="glyphicon glyphicon-plus"></i>', ['create'],
                                    ['role'=>'modal-remote','title'=> 'Добавить', 'class'=>'btn btn-info']).
                                '<ul class="panel-controls">
                    <li><a href="#" class="panel-fullscreen"><span class="fa fa-expand"></span></a></li>
                    <li><a href="#" class="panel-refresh"><span class="fa fa-refresh"></span></a></li>
                    <li><a href="#" class="panel-remove"><span class="fa fa-times"></span></a></li>
                </ul>  '.
                                '</div>'
                            ],
                        ],
                        'striped' => true,
                        'condensed' => true,
                        'responsive' => true,
                        'panel' => [
                            'type' => 'warning',
                            'heading' => '<i class="glyphicon glyphicon-list"></i> Список платежей',

                        ]
                    ])?>
            </div>
            <div class="tab-pane" id="tab2">
                <?=GridView::widget([
                    'id'=>'crud-datatable-credit',
                    'dataProvider' => $dataProviderCredit,
                    'filterModel' => $searchModelCredit,
                    'pjax'=>true,
                    'columns' => require(__DIR__ . '/_columns.php'),
                    'toolbar'=> [
                        ['content'=>
                            '<div style="margin-top:10px;">' .
                            Html::a('Добавить <i class="glyphicon glyphicon-plus"></i>', ['create'],
                                ['role'=>'modal-remote','title'=> 'Добавить', 'class'=>'btn btn-info']).
                            '<ul class="panel-controls">
                    <li><a href="#" class="panel-fullscreen"><span class="fa fa-expand"></span></a></li>
                    <li><a href="#" class="panel-refresh"><span class="fa fa-refresh"></span></a></li>
                    <li><a href="#" class="panel-remove"><span class="fa fa-times"></span></a></li>
                </ul>  '.
                            '</div>'
                        ],
                    ],
                    'striped' => true,
                    'condensed' => true,
                    'responsive' => true,
                    'panel' => [
                        'type' => 'warning',
                        'heading' => '<i class="glyphicon glyphicon-list"></i> Список платежей',

                    ]
                ])?>
            </div>
        </div>
    </div>
</div>
<?php Modal::begin([
    "id"=>"ajaxCrudModal",
    "footer"=>"",// always need it for jquery plugin
])?>
<?php Modal::end(); ?>


<?php

$script = <<< JS
    $('.widget-shop label.widget-shop__label').first().text('Промокод');
JS;


$this->registerJs($script, \yii\web\View::POS_READY);

?>