<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Balances */

?>
<div class="balances-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
