<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Balances */
?>
<div class="balances-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'user_id',
            'amount',
            'pay_date',
            'promo_code_id',
            'amount_bonus',
            'comment',
        ],
    ]) ?>

</div>
