<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Balances */
?>
<div class="balances-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
