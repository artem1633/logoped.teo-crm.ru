<?php
use yii\helpers\Url;

return [
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],
        // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'id',
    // ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'email_id',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'recipient_email',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'status',
        'hAlign' => \kartik\grid\GridView::ALIGN_CENTER,
        'content' => function($model){
            $output = '';
            switch ($model->status){
                case \app\models\FreeEmailRecipients::STATUS_WAIT:
                    $output = '<span class="label label-warning">В очереди</span>';
                    break;
                case \app\models\FreeEmailRecipients::STATUS_SUCCESS:
                    $output = '<span class="label label-success">Отправлено</span>';
                    break;
                case \app\models\FreeEmailRecipients::STATUS_ERROR:
                    $output = '<span class="label label-danger">Ошибка</span>';
                    break;
            }
            return $output;
        },
    ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'send_datetime',
    // ],

];   