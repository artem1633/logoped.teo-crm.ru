<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\FreeEmailRecipients */

?>
<div class="free-email-recipients-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
