<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\FreeEmailRecipients */
?>
<div class="free-email-recipients-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'email_id:email',
            'recipient_email:email',
            'content:ntext',
            'status',
            'exception_text:ntext',
            'send_datetime',
        ],
    ]) ?>

</div>
