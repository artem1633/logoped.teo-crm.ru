<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\FixLetter */
?>
<div class="fix-letter-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
