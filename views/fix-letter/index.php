<?php
use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\Modal;
use kartik\grid\GridView;
use johnitvn\ajaxcrud\CrudAsset; 
use johnitvn\ajaxcrud\BulkButtonWidget;
use app\widgets\UserSettingSwitcher;

/* @var $this yii\web\View */
/* @var $searchModel app\models\FixLetterSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Пособия «Почини букву»';
$this->params['breadcrumbs'][] = $this->title;

CrudAsset::register($this);

function getCheckboxLabel()
{
    $setting = \app\models\Settings::findByKey('cost_storage_fix_letter');
    $value = number_format($setting->value, 0, ',', ' ');

    return "Хранить созданные пособия бессрочно за {$value} руб. в день";
}

?>
<div class="fix-letter-index">
    <div id="ajaxCrudDatatable">
        <?=GridView::widget([
            'id'=>'crud-datatable',
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'pjax'=>true,
            'columns' => require(__DIR__.'/_columns.php'),
            'toolbar'=> [
                ['content'=>
                    '<div style="margin-top:10px;">' .
                    Html::a('Добавить <i class="glyphicon glyphicon-plus"></i>', ['create'],
                        ['role'=>'modal-remote','title'=> 'Добавить', 'class'=>'btn btn-info']).
                    '<ul class="panel-controls">
                    <li><a href="#" class="panel-fullscreen"><span class="fa fa-expand"></span></a></li>
                    <li><a href="#" class="panel-refresh"><span class="fa fa-refresh"></span></a></li>
                    <li><a href="#" class="panel-remove"><span class="fa fa-times"></span></a></li>
                </ul>  '.
                    '</div>'
                ],
            ],
            'striped' => true,
            'condensed' => true,
            'responsive' => true,          
            'panel' => [
                'before' => '<div style="margin-top: 17px; display: inline-block;">'.UserSettingSwitcher::widget(['attribute' => 'fix_letter_data_keeping', 'label' => getCheckboxLabel(),
                        'disabled' => Yii::$app->user->identity->subscription->fixMonth->typeInstance->getIsSubscribed()]).'</div>',
                'type' => 'warning',
                'heading' => '<i class="glyphicon glyphicon-list"></i> Список пособий «Почини букву»',
                'after'=>BulkButtonWidget::widget([
                        'buttons'=>Html::a('<i class="glyphicon glyphicon-trash"></i>Удалить все',
                            ["bulk-delete"] ,
                            [
                                "class"=>"btn btn-danger btn-xs",
                                'role'=>'modal-remote-bulk',
                                'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                                'data-request-method'=>'post',
                                'data-confirm-title'=>'Подтвердите действие',
                                'data-confirm-message'=>'Вы уверены что хотите удалить эти элемента?'
                            ]),
                    ]).
                    '<div class="clearfix"></div>',
            ]
        ])?>
    </div>
</div>
<?php Modal::begin([
    "id"=>"ajaxCrudModal",
    'size'=>'modal-lg',
    "footer"=>"",// always need it for jquery plugin
])?>
<?php Modal::end(); ?>
