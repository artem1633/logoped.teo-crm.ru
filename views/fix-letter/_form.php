<?php

use app\models\Letter;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\FindLetter */
/* @var $form yii\widgets\ActiveForm */
?>
<style>
    #tab-five {
        font-family: "Pacifico";
    }
    #tab-fourth {
        font-family: "Pacifico";
    }
</style>

<div class="find-letter-form" >

    <div class="panel panel-default tabs" style="padding-top: 20px">
        <ul class="nav nav-tabs" role="tablist">
            <li class="active"><a href="#tab-first" role="tab" data-toggle="tab" aria-expanded="true">Основное</a></li>
            <li class=""><a href="#tab-second" role="tab" data-toggle="tab" aria-expanded="true">Разметка</a></li>
        </ul>
        <?php $form = ActiveForm::begin(); ?>
        <div class="panel-body tab-content">
            <div class="tab-pane active" id="tab-first">
                <div class="row">
<!--                    <div class="col-md-4">-->
                        <?php //echo $form->field($model, 'page_format')->dropDownList(Letter::getPageFormat())?>
<!--                    </div>-->
                    <div class="col-md-4">
                        <?= $form->field($model, 'page_count')->textInput() ?>
                    </div>
                    <div class="col-md-4">
                        <?= $form->field($model, 'letters_count')->input('number', ['max' => 100, 'min' => 0]) ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <?= $form->field($model, 'complexity')->dropDownList(Letter::getComplexityies()) ?>
                    </div>
                    <?php if($model->isNewRecord == false): ?>
                        <div class="col-md-4">
                            <?= $form->field($model, 'amount')->textInput(['maxlength' => true]) ?>
                        </div>
                    <?php endif; ?>
                    <div class="col-md-4">
                        <br>
                        <?= $form->field($model, 'save')->checkbox() ?>
                    </div>
                </div>

                <?php if (!Yii::$app->request->isAjax){ ?>
                    <div class="form-group">
                        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                    </div>
                <?php } ?>
            </div>

            <div class="tab-pane" id="tab-second">
                <?= $this->render('_form_data', [
                    'model' => $model,
                    'lettersCount' => 0,
                    'form' => $form,
                ]) ?>
            </div>
        </div>
        <?php ActiveForm::end(); ?>
    </div>
</div>

<?php

$loadUrl = \yii\helpers\Url::toRoute(['fix-letter/letter-form'], true);

$script = <<< JS
    $('#fixletter-letters_count').change(function(){
        var val = $(this).val();
        
        $.ajax({
            'method': 'GET',
            'url': '{$loadUrl}?lettersCount='+val,
            'success': function(response){
                $('#tab-second').html(response);
            },
        });
    });
JS;

$this->registerJs($script, \yii\web\View::POS_READY);

?>