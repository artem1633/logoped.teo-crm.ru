<?php
use yii\helpers\Url;

return [
    [
        'class' => 'kartik\grid\CheckboxColumn',
        'width' => '20px',
    ],
    [
        'class' => 'kartik\grid\DataColumn',
        'attribute' => 'id',
        'width' => '150px',
        'label' => 'Номер',
        'content' => function($model){
            $html = '';
            if($model->isUsingExpired){
                $html = '<br><div class="label label-danger" style="font-size: 11px;">Хранение не оплачено. Скачивание недостпуно</div>';
            }

            $id = \yii\helpers\Html::a($model->id, ['test-generation', 'id' => $model->id], ['data-pjax' => 0]);

            return $id.$html;
        }
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'user.fio',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'page_format',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'page_count',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'letters_count',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'letters_composition',
    ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'complexity',
    // ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'amount',
    // ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'save',
    // ],
    [
        'class' => 'kartik\grid\ActionColumn',
        'dropdown' => false,
        'vAlign'=>'middle',
        'buttons' => [
            'email' => function($url, $model, $key){
                return \yii\helpers\Html::a('Отправить на почту', ['send-to-email', 'id' => $model->id], ['class' => 'btn btn-info btn-xs', 'style' => 'margin-bottom: 5px;', 'data_pjax' => 0]);
            },
            'download' => function($url, $model, $key){
                return \yii\helpers\Html::a('D', ['#'], ['class' => 'btn btn-success btn-xs', 'download' => $model->pdf_file]);
            },
            'print' => function($url, $model, $key){
                return \yii\helpers\Html::a('Распечатать', ['test-generation', 'id' => $model->id], ['class' => 'btn btn-warning', 'style' => 'margin-bottom: 5px;', 'target' => '_blank', 'data-pjax' => 0]);
            },
        ],
        'template' => '{print} {email} {download} {view} {update} {delete}',
        'urlCreator' => function($action, $model, $key, $index) {
                return Url::to([$action,'id'=>$key]);
        },
        'viewOptions'=>['label'=>'<button class="btn btn-info btn-xs"><span class="glyphicon glyphicon-eye-open"></span></button>','role'=>'modal-remote'],
        'updateOptions'=>['label'=>'<button class="btn btn-warning btn-xs"><span class="glyphicon glyphicon-pencil"></span></button>','role'=>'modal-remote','title'=>'Изменить', 'data-toggle'=>'tooltip'],
        'deleteOptions'=>['role'=>'modal-remote','title'=>'Удалить',
            'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
            'data-request-method'=>'post',
            'label'=>'<button class="btn btn-danger btn-xs"><span class="glyphicon glyphicon-trash"></span></button>',
            'data-toggle'=>'tooltip',
            'data-confirm-title'=>'Вы уверенны?',
            'data-confirm-message'=>'Вы действительно хотите удалить запись'],
    ],

];   