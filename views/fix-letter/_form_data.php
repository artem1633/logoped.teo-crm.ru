<?php

/** @var \app\models\FixLetter $model */
/** @var int $lettersCount */
/** @var \yii\widgets\ActiveForm $form */
/** @var \yii\web\View $this */

?>

<table id="letter-data-table" class="table">
    <tbody class="text-center">
    <?php for ($r = 1, $i = 0; $i < $lettersCount; $i++, $r++): ?>
        <?php if($r == 1): ?>
            <tr>
        <?php endif; ?>

            <td>
                <?= $form->field($model, "data[$i]['symbol']")->dropDownList([
                    'Печатные заглавные' => $model->bigLetterlist,
                    'Печатные строчные' => $model->bigLetterlist,
                    'Прописные заглавные' => $model->bigLetterlist,
                    'Прописные строчные' => $model->bigLetterlist
                ])->label(false) ?>
                <div class="hidden">
                    <?= $form->field($model, "data[$i]['type']")->hiddenInput(['value' => 0]) ?>
                </div>
            </td>

        <?php if($r == 5): ?>
            </tr>
        <?php $r = 0; endif; ?>
    <?php endfor; ?>
    </tbody>
</table>

<?php

$script = <<< JS
    $('#letter-data-table select').change( function (evt) {
          var label = $(this.options[this.selectedIndex]).closest('optgroup').prop('label');
          if(label == 'Печатные заглавные'){
            $(this).parent().parent().find('input[type="hidden"]').val(0);
          } else if(label == 'Печатные строчные'){
             $(this).parent().parent().find('input[type="hidden"]').val(1); 
          } else if(label == 'Прописные заглавные'){
              $(this).parent().parent().find('input[type="hidden"]').val(2);
          } else if(label == 'Прописные строчные'){
              $(this).parent().parent().find('input[type="hidden"]').val(3);
          }
    });
JS;

$this->registerJs($script, \yii\web\View::POS_READY);

?>