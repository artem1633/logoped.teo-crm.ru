<?php

/**
 * @var $this \yii\web\View
 * @var $configure \app\models\YandexMoney
 */

use kartik\form\ActiveForm;
use yii\helpers\Html;;

$this->title = "Настройки приема платежей Yandex Money";

?>

<div class="row js-settings">
    <div class="col-md-12">
        <div class="panel panel-inverse">
            <div class="panel-heading">
                <h4 class="panel-title"><?= $this->title; ?></h4>
            </div>
            <div class="panel-body panel-form">
                <?php $form = ActiveForm::begin(); ?>
                <div class="row">
                    <div class="col-md-4">
                        <?= $form->field($configure, 'secret')->textInput() ?>
                    </div>
                    <div class="col-md-4">
                        <?= $form->field($configure, 'wallet')->textInput()?>
                    </div>
                </div>

                <div class="form-group">
                    <?= Html::submitButton('Сохранить', ['class' => 'btn btn-primary']) ?>
                </div>

                <?php ActiveForm::end(); ?>

            </div>
        </div>
    </div>
</div>


