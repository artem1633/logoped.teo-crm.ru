<?php
use yii\helpers\Html;
use yii\bootstrap\Modal;
use johnitvn\ajaxcrud\CrudAsset; 
use yii\helpers\Url;
use app\models\Users;
use yii\widgets\Pjax;

//if (!file_exists('avatars/'.Yii::$app->user->identity->foto) || Yii::$app->user->identity->foto == '') {
    $path = 'http://' . $_SERVER['SERVER_NAME'].'/examples/images/users/avatar.jpg';
//} else {
 //   $path = 'http://' . $_SERVER['SERVER_NAME'].'/avatars/'.Yii::$app->user->identity->foto;
//}

?>
<?php 
        $session = Yii::$app->session;
        if($session['left'] == null | $session['left'] == 'small') $left="x-navigation-minimized";
        else $left="x-navigation-custom";
    ?>
<!-- START PAGE SIDEBAR -->
            <div class="page-sidebar page-sidebar-fixed scroll">
                <!-- START X-NAVIGATION -->
                <ul class="x-navigation <?=$left?>">
                    <li class="xn-logo">
                        <a href="<?=Url::toRoute([Yii::$app->homeUrl])?>"><?=Yii::$app->name?></a>
                        <a href="#" class="x-navigation-control"></a>
                    </li>
                    <li class="xn-profile">
                        <a href="#" class="profile-mini">
                            <img src="<?=$path?>" alt="John Doe"/>
                        </a>
                        <div class="profile">
                            <div class="profile-image">
                                <img src="<?=$path?>" alt="John Doe"/>
                            </div>
                            <div class="profile-data">
                                <div class="profile-data-name"><?=Yii::$app->user->identity->fio?></div>
                                <div class="profile-data-title"></div>
                            </div>
                            <div class="profile-controls">
                                <?= Html::a('<span class="fa fa-info"></span>', ['/users/profile'], ['title'=> 'Профиль','class'=>'profile-control-left']); ?>
                                <!-- <a href="pages-profile.html" class="profile-control-left"><span class="fa fa-info"></span></a> -->
                                <!-- <a href="pages-messages.html" class="profile-control-right"><span class="fa fa-envelope"></span></a> -->
                                <?= Html::a('<span class="fa fa-envelope"></span>', ['/users/profile'], ['title'=> 'Профиль','class'=>'profile-control-right']); ?>
                            </div>
                        </div>                                                                        
                    </li>
                    <li class="xn-title">Меню</li>
                    <li>
                        <?= Html::a('<span class="fa fa-desktop"></span> <span class="xn-text">Рабочий стол</span>', ['/users/dashboard'], []); ?>
                    </li>
                    <li>
                        <?= Html::a('<span class="fa fa-search"></span> <span class="xn-text">Найди букву</span>', ['/find-letter'], []); ?>
                    </li>
                    <li>
                        <?= Html::a('<span class="fa fa-pencil"></span> <span class="xn-text">Почини букву</span>', ['/fix-letter'], []); ?>
                    </li>
                    <li>
                        <?= Html::a('<span class="fa fa-money"></span> <span class="xn-text">Баланс</span>', ['/balance'], []); ?>
                    </li>
                    <li>
                        <?= Html::a('<span class="fa fa-cogs"></span> <span class="xn-text">Настройки</span>', ['/account'], []); ?>
                    </li>
                    <?php if(Yii::$app->user->identity->isSuperAdmin()): ?>
                        <li>
                            <?= Html::a('<span class="fa fa-rub"></span> <span class="xn-text">Яндекс Деньги</span>', ['/yandex-money'], []); ?>
                        </li>
                    <?php endif; ?>
                    <?php if(Yii::$app->user->identity->type == 0) { ?>
                    <li class="xn-openable">
                        <a href="#"><span class="fa fa-university"></span> <span class="xn-text">Настройки</span></a>
                        <ul>
                            <li><?= Html::a('<span class="fa fa-users"></span>Промокоды', ['/promo-code'], []); ?></li>
                            <li><?= Html::a('<span class="fa fa-users"></span>Почини букву', ['/fix-letters-images'], []); ?></li>
                            <li><?= Html::a('<span class="fa fa-users"></span>Найди букву', ['/find-letters-images'], []); ?></li>
                            <li><?= Html::a('<span class="fa fa-users"></span>Пользователи', ['/users'], []); ?></li>
                            <li><?= Html::a('<span class="fa fa-cogs"></span>Сообщения', ['/message'], []); ?></li>
                            <li><?= Html::a('<span class="fa fa-bookmark"></span>Параметры ', ['/settings'], []); ?></li>
                            <li><?= Html::a('<span class="fa fa-envelope"></span>e-mail уведомления', ['/email'],[]); ?></li>
                            <li><?= Html::a('<span class="fa fa-mobile"></span>SMS', ['/sms'],[]); ?></li>
                            <li><?= Html::a('<span class="fa fa-th-large"></span>Страница входа', ['/sliders/index'],[]); ?></li>
                        </ul>
                    </li>
                    <?php } ?>

                </ul>
                <!-- END X-NAVIGATION -->
            </div>
            <!-- END PAGE SIDEBAR -->