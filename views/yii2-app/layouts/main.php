<?php
use yii\helpers\Html;

$actionId = Yii::$app->controller->action->id;
if ($actionId  === 'login' || $actionId  === 'register') app\assets\RegisterAsset::register($this);
else app\assets\AppAsset::register($this);
//app\assets\AppAsset::register($this);

if (Yii::$app->controller->action->id === 'login' || Yii::$app->controller->action->id === 'register' || Yii::$app->controller->id === 'default' || Yii::$app->controller->id === 'setting') { 
    echo $this->render( 'main-login', ['content' => $content] );
} else {
    ?>
    <?php $this->beginPage() ?>
    <!DOCTYPE html>
    <html lang="<?= Yii::$app->language ?>">
    <head>
        <meta charset="<?= Yii::$app->charset ?>"/>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="https://fonts.googleapis.com/css?family=Pacifico" rel="stylesheet">
        <?= Html::csrfMetaTags() ?>
        <title><?= Html::encode($this->title) ?></title>
        <?php $this->head() ?>
    </head>
    <?php 
        $session = Yii::$app->session;
        if($session['body'] == null | $session['body'] == 'small') $body="page-navigation-toggled page-container-wide";
        else $body="";
    ?>
    <body class="">
    <?php $this->beginBody() ?>
        <div class="page-container <?=$body ?>">

            <?= $this->render( 'left.php', [] ) ?>
            <!-- PAGE CONTENT -->
            <div class="page-content">
                <?= Yii::$app->controller->id == 'default' ? $this->render( 'header-menu.php', [] ) : $this->render( 'header.php', [] ) ?>
                <?= $this->render( 'content.php', ['content' => $content, ] ) ?>
             </div>                       
            <!-- END PAGE CONTENT -->
        </div>
    <?php $this->endBody() ?>
    </body>
    </html>
    <?php $this->endPage() ?>
<?php } ?>
