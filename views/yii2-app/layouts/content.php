<?php
use yii\widgets\Breadcrumbs;
use app\widgets\Alert;

?>
<div class="content-wrapper">
    <!-- <section class="content-header">
        <?php /*if (isset($this->blocks['content-header'])) {*/ ?>
            <h1><?php /*$this->blocks['content-header']*/ ?></h1>
        <?php /*} else {*/ ?>
            <h1>
                <?php
                /*if ($this->title !== null) {
                    echo \yii\helpers\Html::encode($this->title);
                } else {
                    echo \yii\helpers\Inflector::camel2words(
                        \yii\helpers\Inflector::id2camel($this->context->module->id)
                    );
                    echo ($this->context->module->id !== \Yii::$app->id) ? '<small>Module</small>' : '';
                }*/ ?>
            </h1>
        <?php /*}*/ ?>

        <?php
        /*Breadcrumbs::widget(
            [
                'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
            ]
        )*/ ?>
    </section> -->

    <div class="alert alert-success" data-template="alert" style="display: none; margin-bottom: 8px;">
        <strong>Success!</strong> Indicates a successful or positive action.
    </div>

    <section class="content">
        <?= Alert::widget() ?>
        <?= $content ?>
    </section>
</div>
<footer class="main-footer">
    <div class="pull-right hidden-xs">
        <b>Version</b> 2.0
    </div>
    <strong>Copyright &copy; 2019 <a href="https://shop-crm.ru/portfolio?utm=teo_job">Разработчик TEO</a>.</strong> All rights
    reserved.
</footer>

<!-- Control Sidebar -->

<!-- Add the sidebar's background. This div must be placed
     immediately after the control sidebar -->
