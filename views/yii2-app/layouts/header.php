<?php
use yii\widgets\Breadcrumbs;
use app\widgets\Alert;
use yii\helpers\Html;
use app\models\Settings;
use app\models\Resume;
use yii\helpers\Url;
use app\models\Questionary;
use app\models\Chat;


//Yii::$app->db->createCommand()->update('users', ['last_login_date' => date('Y-m-d H:i:s')], [ 'id' => Yii::$app->user->identity->id ])->execute();
?>

                
                <!-- START X-NAVIGATION VERTICAL -->
                <ul class="x-navigation x-navigation-horizontal x-navigation-panel">
                    <!-- TOGGLE NAVIGATION -->
                    <li class="xn-icon-button">
                        <a href="#" onclick="$.post('/site/menu-position');" class="x-navigation-minimize"><span class="fa fa-dedent"></span></a>
                    </li>
                    <!-- END TOGGLE NAVIGATION -->
                    <!-- SEARCH -->
                    <!-- <li class="xn-search">
                        <form role="form">
                            <input type="text" name="search" placeholder="Search..."/>
                        </form>
                    </li>   --> 
                    <!-- END SEARCH -->
                    <!-- SIGN OUT -->
                    <li class="xn-icon-button pull-right">
                        <!-- <a href="#" class="mb-control" data-box="#mb-signout"><span class="fa fa-sign-out"></span></a> -->
                        <?= Html::a('<span class="fa fa-sign-out"></span>',['/site/logout'],['data-confirm'=> false, 'data-method'=>false,// for overide yii data api
                          'data-request-method'=>'post',
                          'data-toggle'=>'tooltip',]) ?>
                    </li> 
                    <!-- END SIGN OUT -->
                    <!-- MESSAGES -->
                    <!-- <li class="xn-icon-button pull-right">
                        <a href="#"><span class="fa fa-comments"></span></a>
                        <div class="informer informer-danger">4</div>
                        <div class="panel panel-primary animated zoomIn xn-drop-left xn-panel-dragging">
                            <div class="panel-heading">
                                <h3 class="panel-title"><span class="fa fa-comments"></span> Messages</h3>                                
                                <div class="pull-right">
                                    <span class="label label-danger">4 new</span>
                                </div>
                            </div>
                            <div class="panel-body list-group list-group-contacts scroll" style="height: 200px;">
                                <a href="#" class="list-group-item">
                                    <div class="list-group-status status-online"></div>
                                    <img src="/../examples/images/users/user2.jpg" class="pull-left" alt="John Doe"/>
                                    <span class="contacts-title">John Doe</span>
                                    <p>Praesent placerat tellus id augue condimentum</p>
                                </a>
                                <a href="#" class="list-group-item">
                                    <div class="list-group-status status-away"></div>
                                    <img src="/../examples/images/users/user.jpg" class="pull-left" alt="Dmitry Ivaniuk"/>
                                    <span class="contacts-title">Dmitry Ivaniuk</span>
                                    <p>Donec risus sapien, sagittis et magna quis</p>
                                </a>
                                <a href="#" class="list-group-item">
                                    <div class="list-group-status status-away"></div>
                                    <img src="/../examples/images/users/user3.jpg" class="pull-left" alt="Nadia Ali"/>
                                    <span class="contacts-title">Nadia Ali</span>
                                    <p>Mauris vel eros ut nunc rhoncus cursus sed</p>
                                </a>
                                <a href="#" class="list-group-item">
                                    <div class="list-group-status status-offline"></div>
                                    <img src="/../examples/images/users/user6.jpg" class="pull-left" alt="Darth Vader"/>
                                    <span class="contacts-title">Darth Vader</span>
                                    <p>I want my money back!</p>
                                </a>
                            </div>     
                            <div class="panel-footer text-center">
                                <a href="pages-messages.html">Show all messages</a>
                            </div>                            
                        </div>                        
                    </li> -->
                    <!-- END MESSAGES -->
                    <!-- SMS -->
                    <li class="xn-icon-button pull-right">
                        <a href="#"><span class="fa fa-envelope-o"></span></a>
                        <div class="panel panel-primary animated zoomIn xn-drop-left xn-panel-dragging">
                            <div class="panel-heading">
                                <h3 class="panel-title"><span class="fa fa-envelope"></span>Новые сообщения</h3>                          
                                <div class="pull-right">
                                    <span class="label label-warning"><?=$resumesSmsCount?></span>
                                </div>
                            </div>
                            <div class="panel-body list-group scroll" style="height: 200px;">

                            <?php
                                foreach ($resumesSms as $resume) {
                            ?>
                                <a class="list-group-item" href="<?=Url::toRoute(['/resume/view', 'id' => $resume->id ])?>">
                                    <strong><?=$resume->fio?></strong>
                                    <div class="progress progress-small progress-striped active">
                                        <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" style="width: 100%;">100%</div>
                                    </div>
                                    <small class="text-muted">Дата и время заполнения : <?=date('H:i:s d.m.Y', strtotime($resume->date_cr) )?></small>
                                </a>
                            <?php
                                }
                             ?>      
                            </div>                           
                        </div>                        
                    </li>
                    <!-- TASKS -->
                    <li class="xn-icon-button pull-right">
                        <a href="#"><span class="fa fa-tasks"></span></a>
                        <div class="informer informer-warning"><?=$resumesCount?></div>
                        <div class="panel panel-primary animated zoomIn xn-drop-left xn-panel-dragging">
                            <div class="panel-heading">
                                <h3 class="panel-title"><span class="fa fa-tasks"></span> Новые резюме</h3>                                
                                <div class="pull-right">
                                    <span class="label label-warning"><?=$resumesCount?></span>
                                </div>
                            </div>
                            <div class="panel-body list-group scroll" style="height: 200px;"> 
                            <?php
                                foreach ($resumes as $resume) {
                            ?>
                                <a class="list-group-item" href="<?=Url::toRoute(['/resume/view', 'id' => $resume->id ])?>">
                                    <strong><?=$resume->fio?></strong>
                                    <div class="progress progress-small progress-striped active">
                                        <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" style="width: 100%;">100%</div>
                                    </div>
                                    <small class="text-muted">Дата и время заполнения : <?=date('H:i:s d.m.Y', strtotime($resume->date_cr) )?></small>
                                </a>
                            <?php
                                }
                             ?>                            

                            </div>     
                            <div class="panel-footer text-center">
                                <a href="<?=Url::toRoute(['/resume/index'])?>">Все резюме</a>
                            </div>                            
                        </div>                        
                    </li>

                    <li class="xn-icon-button pull-right">
                        <?= Html::a(number_format(Yii::$app->user->identity->account->balance, 0, '.', ' ').'<i class="fa fa-rub"></i>', '#', ['style' => 'display: inline-block; width: 100px;']) ?>
                    </li>

                    <!-- END TASKS -->
                </ul>
                <!-- END X-NAVIGATION VERTICAL -->                    
                
                <!-- START BREADCRUMB -->
                <!-- <ul class="breadcrumb push-down-0">
                    <li><a href="#">Home</a></li>
                    <li><a href="#">Layouts</a></li>
                    <li class="active">Custom Navigation</li>                    
                </ul> -->

                <section class="breadcrumb push-down-0">
                <?php
               /* echo Breadcrumbs::widget(
                    [
                        'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                    ]
                )*/ ?>
                <?php if (isset($this->blocks['content-header'])) { ?>
                    <h1><?php $this->blocks['content-header'] ?></h1>
                <?php } else { ?>
                    <!-- <h1>
                        <?php
                        /*if ($this->title !== null) {
                            echo \yii\helpers\Html::encode($this->title);
                        } else {
                            echo \yii\helpers\Inflector::camel2words(
                                \yii\helpers\Inflector::id2camel($this->context->module->id)
                            );
                            echo ($this->context->module->id !== \Yii::$app->id) ? '<small>Module</small>' : '';
                        }*/ ?>
                    </h1> -->
                <?php } ?>

            </section>

                <!-- END BREADCRUMB -->                           
                
                <!-- PAGE CONTENT WRAPPER -->
                <div class="page-content-wrap">
                <br>                    
                </div>
                <!-- END PAGE CONTENT WRAPPER -->
