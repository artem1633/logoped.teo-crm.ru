<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\FindLetter */
?>
<div class="find-letter-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'user_id',
            'page_format',
            'page_count',
            'letters_count',
            'letters_composition',
            'complexity',
            'amount',
            'save',
        ],
    ]) ?>

</div>
