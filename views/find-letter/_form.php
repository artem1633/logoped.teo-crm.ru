<?php

use app\models\Letter;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\FindLetter */
/* @var $form yii\widgets\ActiveForm */
?>
<style>
    #tab-five {
        font-family: "Pacifico";
    }
    #tab-fourth {
        font-family: "Pacifico";
    }
</style>

<div class="find-letter-form" >

    <div class="panel panel-default tabs" style="padding-top: 20px">
        <ul class="nav nav-tabs" role="tablist">
            <li class="active"><a href="#tab-first" role="tab" data-toggle="tab" aria-expanded="true">Основное</a></li>
            <li class=""><a href="#tab-second" role="tab" data-toggle="tab" aria-expanded="true">Печатные заглавные</a></li>
            <li class=""><a href="#tab-third" role="tab" data-toggle="tab" aria-expanded="true">Печатные строчные</a></li>
            <li class=""><a href="#tab-fourth" role="tab" data-toggle="tab" aria-expanded="true">Прописные заглавные</a></li>
            <li class=""><a href="#tab-five" role="tab" data-toggle="tab" aria-expanded="true">Прописные строчные</a></li>
        </ul>
        <?php $form = ActiveForm::begin(); ?>
        <div class="panel-body tab-content">
            <div class="tab-pane active" id="tab-first">
                <div class="row">
                    <div class="col-md-6">
                        <?= $form->field($model, 'page_format')->dropDownList(Letter::getPageFormat())?>
                    </div>
                    <div class="col-md-6">
                        <?= $form->field($model, 'page_count')->textInput() ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <?= $form->field($model, 'complexity')->dropDownList(Letter::getComplexityies()) ?>
                    </div>
                    <?php if($model->isNewRecord == false): ?>
                        <div class="col-md-4">
                            <?= $form->field($model, 'amount')->textInput(['maxlength' => true]) ?>
                        </div>
                    <?php endif; ?>
                    <div class="col-md-4">
                        <br>
                        <?= $form->field($model, 'save')->checkbox() ?>
                    </div>
                </div>

                <?php if (!Yii::$app->request->isAjax){ ?>
                    <div class="form-group">
                        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                    </div>
                <?php } ?>
            </div>

            <div class="tab-pane" id="tab-second">
                <?php foreach ($model->bigLetterlist as $id=>$slt)
                    {
                        if($model->isNewRecord){
                            $value = '';
                        } else {
                            $symbol = mb_strtolower($slt);
                            $value = $model->dataAmount[\app\models\FindLetterData::TYPE_PRINT_UP][$symbol];
                        }

                        echo "<div class='col-md-2'>".$form->field($model,"printUp[$id]",
                                ['template' => "<div class=\"row\"><div class=\"col-xs-3\">{label}</div><div class=\"col-xs-9\">{input}</div></div>"])->
                            textInput(['value' => $value])->label($slt)."</div>";
                    }
                ?>
            </div>
            <div class="tab-pane " id="tab-third">
                <?php foreach ($model->smallLetterlist as $id=>$slt)
                {
                    if($model->isNewRecord){
                        $value = '';
                    } else {
                        $symbol = mb_strtolower($slt);
                        $value = $model->dataAmount[\app\models\FindLetterData::TYPE_PRINT_LOW][$symbol];
                    }

                    echo "<div class='col-md-2'>".$form->field($model,"printLow[$id]",
                            ['template' => "<div class=\"row\"><div class=\"col-xs-3\">{label}</div><div class=\"col-xs-9\">{input}</div></div>"])->
                        textInput(['value' => $value])->label($slt)."</div>";
                }
                ?>
            </div>
            <div class="tab-pane " id="tab-fourth">
                <?php foreach ($model->bigLetterlist as $id=>$slt)
                {
                    if($model->isNewRecord){
                        $value = '';
                    } else {
                        $symbol = mb_strtolower($slt);
                        $value = $model->dataAmount[\app\models\FindLetterData::TYPE_RECIPE_UP][$symbol];
                    }

                    echo "<div class='col-md-2'>".$form->field($model,"recipeUp[$id]",
                            ['template' => "<div class=\"row\"><div class=\"col-xs-3\">{label}</div><div class=\"col-xs-9\">{input}</div></div>"])->
                        textInput(['value' => $value])->label($slt)."</div>";
                }
                ?>
            </div>
            <div class="tab-pane " id="tab-five">
                <?php foreach ($model->smallLetterlist as $id=>$slt)
                {
                    if($model->isNewRecord){
                        $value = '';
                    } else {
                        $symbol = mb_strtolower($slt);
                        $value = $model->dataAmount[\app\models\FindLetterData::TYPE_RECIPE_LOW][$symbol];
                    }

                    echo "<div class='col-md-2'>".$form->field($model,"recipeLow[$id]",
                            ['template' => "<div class=\"row\"><div class=\"col-xs-3\">{label}</div><div class=\"col-xs-9\">{input}</div></div>"])->
                        textInput(['value' => $value])->label($slt)."</div>";
                }
                ?>
            </div>
        </div>
        <?php ActiveForm::end(); ?>
    </div>
</div>
