<?php
use kartik\file\FileInput;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\forms\FixAttachmentsForm */
/* @var $form yii\widgets\ActiveForm */

$attachmentsEasy = \yii\helpers\ArrayHelper::getColumn(\app\models\FixLettersImagesAttachments::find()
    ->where([
        'fix_letter_image_id' => $model->fixLetterImageId,
        'type' => \app\models\FixLettersImagesAttachments::TYPE_EASY,
        'drawing' => \app\models\FixLettersImagesAttachments::DRAWING_WROTE
    ])->all(), 'path');
for ($i = 0; $i < count($attachmentsEasy); $i++){
    $attachmentsEasy[$i] = '/'.$attachmentsEasy[$i];
}


$attachmentsMiddle = \yii\helpers\ArrayHelper::getColumn(\app\models\FixLettersImagesAttachments::find()
    ->where([
        'fix_letter_image_id' => $model->fixLetterImageId,
        'type' => \app\models\FixLettersImagesAttachments::TYPE_MIDDLE,
        'drawing' => \app\models\FixLettersImagesAttachments::DRAWING_WROTE
    ])->all(), 'path');
for ($i = 0; $i < count($attachmentsMiddle); $i++){
    $attachmentsMiddle[$i] = '/'.$attachmentsMiddle[$i];
}


$attachmentsHard = \yii\helpers\ArrayHelper::getColumn(\app\models\FixLettersImagesAttachments::find()
    ->where([
        'fix_letter_image_id' => $model->fixLetterImageId,
        'type' => \app\models\FixLettersImagesAttachments::TYPE_HARD,
        'drawing' => \app\models\FixLettersImagesAttachments::DRAWING_WROTE
    ])->all(), 'path');
for ($i = 0; $i < count($attachmentsHard); $i++){
    $attachmentsHard[$i] = '/'.$attachmentsHard[$i];
}


$attachmentsEasyPrinted = \yii\helpers\ArrayHelper::getColumn(\app\models\FixLettersImagesAttachments::find()
    ->where([
        'fix_letter_image_id' => $model->fixLetterImageId,
        'type' => \app\models\FixLettersImagesAttachments::TYPE_EASY,
        'drawing' => \app\models\FixLettersImagesAttachments::DRAWING_PRINTED
    ])->all(), 'path');
for ($i = 0; $i < count($attachmentsEasyPrinted); $i++){
    $attachmentsEasyPrinted[$i] = '/'.$attachmentsEasyPrinted[$i];
}


$attachmentsMiddlePrinted = \yii\helpers\ArrayHelper::getColumn(\app\models\FixLettersImagesAttachments::find()
    ->where([
        'fix_letter_image_id' => $model->fixLetterImageId,
        'type' => \app\models\FixLettersImagesAttachments::TYPE_MIDDLE,
        'drawing' => \app\models\FixLettersImagesAttachments::DRAWING_PRINTED
    ])->all(), 'path');
for ($i = 0; $i < count($attachmentsMiddlePrinted); $i++){
    $attachmentsMiddlePrinted[$i] = '/'.$attachmentsMiddlePrinted[$i];
}


$attachmentsHardPrinted = \yii\helpers\ArrayHelper::getColumn(\app\models\FixLettersImagesAttachments::find()
    ->where([
        'fix_letter_image_id' => $model->fixLetterImageId,
        'type' => \app\models\FixLettersImagesAttachments::TYPE_HARD,
        'drawing' => \app\models\FixLettersImagesAttachments::DRAWING_PRINTED
    ])->all(), 'path');
for ($i = 0; $i < count($attachmentsHardPrinted); $i++){
    $attachmentsHardPrinted[$i] = '/'.$attachmentsHardPrinted[$i];
}


?>

<div class="fix-letters-images-form">

    <?php $form = ActiveForm::begin(); ?>

    <ul class="nav nav-pills" style="margin: 5px 0;">
        <li class="active" style="width: 49%; text-align: center;"><a data-toggle="tab" href="#panel1">Прописные</a></li>
        <li style="width: 49%; text-align: center;"><a data-toggle="tab" href="#panel2">Печатные</a></li>
    </ul>

    <div class="tab-content">
        <div id="panel1" class="tab-pane fade in active">
            <?= $form->field($model, 'easyAttachments[]')->widget(FileInput::class,[
                'options' => ['multiple' => true,],
                'pluginOptions' => [
                    'layoutTemplates' => [
                        'actionDownload' => '<a class="{downloadClass}" href="{downloadUrl}" target="_blank" data-pjax="0" title="Загрузить" download="{caption}"><i class="glyphicon glyphicon-download"></i></a> '
                    ],
                    'uploadUrl' => \yii\helpers\Url::toRoute(['upload-attachment', 'id' => $model->fixLetterImageId]),
                    'initialPreview' => $attachmentsEasy,
                    'previewFileType' => 'any',
                    'overwriteInitial' => false,
                    'initialPreviewAsData' => true,
                    'preferIconicPreview' => true,
                ],
            ]) ?>


            <?= $form->field($model, 'middleAttachments[]')->widget(FileInput::class,[
                'options' => ['multiple' => true,],
                'pluginOptions' => [
                    'layoutTemplates' => [
                        'actionDownload' => '<a class="{downloadClass}" href="{downloadUrl}" target="_blank" data-pjax="0" title="Загрузить" download="{caption}"><i class="glyphicon glyphicon-download"></i></a> '
                    ],
                    'uploadUrl' => \yii\helpers\Url::toRoute(['upload-attachment', 'id' => $model->fixLetterImageId]),
                    'initialPreview' => $attachmentsMiddle,
                    'previewFileType' => 'any',
                    'overwriteInitial' => false,
                    'initialPreviewAsData' => true,
                    'preferIconicPreview' => true,
                ],
            ]) ?>


            <?= $form->field($model, 'hardAttachments[]')->widget(FileInput::class,[
                'options' => ['multiple' => true,],
                'pluginOptions' => [
                    'layoutTemplates' => [
                        'actionDownload' => '<a class="{downloadClass}" href="{downloadUrl}" target="_blank" data-pjax="0" title="Загрузить" download="{caption}"><i class="glyphicon glyphicon-download"></i></a> '
                    ],
                    'uploadUrl' => \yii\helpers\Url::toRoute(['upload-attachment', 'id' => $model->fixLetterImageId]),
                    'initialPreview' => $attachmentsHard,
                    'previewFileType' => 'any',
                    'overwriteInitial' => false,
                    'initialPreviewAsData' => true,
                    'preferIconicPreview' => true,
                ],
            ]) ?>
        </div>
        <div id="panel2" class="tab-pane fade">
            <?= $form->field($model, 'easyAttachmentsPrinted[]')->widget(FileInput::class,[
                'options' => ['multiple' => true,],
                'pluginOptions' => [
                    'layoutTemplates' => [
                        'actionDownload' => '<a class="{downloadClass}" href="{downloadUrl}" target="_blank" data-pjax="0" title="Загрузить" download="{caption}"><i class="glyphicon glyphicon-download"></i></a> '
                    ],
                    'uploadUrl' => \yii\helpers\Url::toRoute(['upload-attachment', 'id' => $model->fixLetterImageId]),
                    'initialPreview' => $attachmentsEasyPrinted,
                    'previewFileType' => 'any',
                    'overwriteInitial' => false,
                    'initialPreviewAsData' => true,
                    'preferIconicPreview' => true,
                ],
            ]) ?>


            <?= $form->field($model, 'middleAttachmentsPrinted[]')->widget(FileInput::class,[
                'options' => ['multiple' => true,],
                'pluginOptions' => [
                    'layoutTemplates' => [
                        'actionDownload' => '<a class="{downloadClass}" href="{downloadUrl}" target="_blank" data-pjax="0" title="Загрузить" download="{caption}"><i class="glyphicon glyphicon-download"></i></a> '
                    ],
                    'uploadUrl' => \yii\helpers\Url::toRoute(['upload-attachment', 'id' => $model->fixLetterImageId]),
                    'initialPreview' => $attachmentsMiddlePrinted,
                    'previewFileType' => 'any',
                    'overwriteInitial' => false,
                    'initialPreviewAsData' => true,
                    'preferIconicPreview' => true,
                ],
            ]) ?>


            <?= $form->field($model, 'hardAttachmentsPrinted[]')->widget(FileInput::class,[
                'options' => ['multiple' => true,],
                'pluginOptions' => [
                    'layoutTemplates' => [
                        'actionDownload' => '<a class="{downloadClass}" href="{downloadUrl}" target="_blank" data-pjax="0" title="Загрузить" download="{caption}"><i class="glyphicon glyphicon-download"></i></a> '
                    ],
                    'uploadUrl' => \yii\helpers\Url::toRoute(['upload-attachment', 'id' => $model->fixLetterImageId]),
                    'initialPreview' => $attachmentsHardPrinted,
                    'previewFileType' => 'any',
                    'overwriteInitial' => false,
                    'initialPreviewAsData' => true,
                    'preferIconicPreview' => true,
                ],
            ]) ?>
        </div>
    </div>





    <?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>
