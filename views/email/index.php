<?php

use yii\helpers\Html;

/** @var \app\models\StrictEmails[] $emails */
/** @var \yii\web\View $this */

$this->title = 'Почтовые сообщения';

?>

<style>
    .panel-email .panel-body p {
        color: #000 !important;
    }
</style>

<h2 style="margin-bottom: 15px;"><?=$this->title?></h2>

<div class="panel panel-success" style="margin-bottom: 30px;">
    <div class="panel-heading">
        <h4 class="panel-title">
            Свободные сообщения
        </h4>
    </div>
    <div class="panel-body">
        <?= Html::a('<i class="fa fa-pencil"></i> Написать сообщение', ['write-new'], ['class' => 'btn btn-success']) ?>
        <?= Html::a('Посмотрть черновики', ['free'], ['class' => 'btn btn-default']) ?>
    </div>
</div>

<h2 style="margin-bottom: 10px;">Регулярные сообщения</h2>

<?php foreach ($emails as $email): ?>
    <a href="<?=\yii\helpers\Url::toRoute(['update', 'id' => $email->id])?>">
        <div class="panel panel-default panel-email" style="margin-bottom: 15px;">
            <div class="panel-body">
                <h2 style="font-size: 20px; margin-bottom: 10px;"><?=$email->keyLabel?></h2>
                <p style="font-size: 14px; color: #000 !important;"><?=$email->content?></p>
            </div>
        </div>
    </a>
<?php endforeach; ?>
