<?php

use yii\widgets\ActiveForm;

/** @var \app\models\StrictEmails $model */
/** @var \yii\web\View $this */

?>

<div class="panel panel-default">
    <div class="panel-body">
        <?php $form = ActiveForm::begin() ?>

        <?= $form->field($model, 'subject')->textInput() ?>
        <?= $form->field($model, 'content')->widget(\dosamigos\ckeditor\CKEditor::class, []) ?>

        <?= \yii\helpers\Html::submitButton('Сохранить', ['class' => 'btn btn-success pull-right']) ?>

        <?php ActiveForm::end() ?>
    </div>
</div>