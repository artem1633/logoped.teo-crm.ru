<?php
use yii\helpers\Url;
use yii\helpers\Html;


return [
    [
        'class' => 'kartik\grid\CheckboxColumn',
        'width' => '20px',
    ],
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],
        // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'id',
    // ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => "fone",
        'width' =>'60px',
        'content' => function ($data) {
            if($data->fone == null) $path = 'http://' . $_SERVER['SERVER_NAME'] . '/images/noimage.png';
            else $path = 'http://' . $_SERVER['SERVER_NAME'] . '/uploads/sliders/' . $data->fone;
            return '<img style="width: 45px; height:45px; border-radius: 1em;border: solid 1px #cecece;" src="' . $path . '" >';
        }
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'title',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'format'=>'html', 
        'attribute'=>'text',
    ],
   
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'order',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'view',
        'content' => function($data){
            if($data->view == 1) return 'Да';
            else return 'Нет';
        }
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'view_time',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
       'attribute'=>'buttons',
        'width'=>'100px',
        'label' => 'Действия',

        'content' => function($data){
            $url = Url::to(['/sliders/update', 'id' => $data->id]);
            $update = Html::a('<button class="btn btn-info btn-xs"><span class="glyphicon glyphicon-pencil"></span></button>', $url, ['role'=>'modal-remote','title'=>'Изменить', 'data-toggle'=>'tooltip',]);
            $url = Url::to(['/sliders/delete', 'id' => $data->id]);
            $delete = Html::a('<button class="btn btn-danger btn-xs"><span class="glyphicon glyphicon-trash"></span></button>', $url, 
                    [

                        'role'=>'modal-remote','title'=>'Удалить', 
                        'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                        'data-request-method'=>'post',
                        'data-toggle'=>'tooltip',
                        'data-confirm-title'=>'Подтвердите действие',
                        'data-confirm-message'=>'Вы уверены что хотите удалить этого элемента?'
                    ]);

            $url = Url::to(['/sliders/sorting']);
            $sorting = Html::a('<button class="btn btn-warning btn-xs"><span class="glyphicon glyphicon-sort"></span></button>', $url, ['role'=>'modal-remote','title'=>'Поменять место слидеров', 'data-toggle'=>'tooltip']);

            return '<center>' . $update . '&nbsp; ' . $sorting . '&nbsp; ' . $delete . '</center>';

        }
    
, 
    ],

];   