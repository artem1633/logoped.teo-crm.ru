<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Sliders */
?>
<div class="sliders-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'title',
            'text:ntext',
            'fone',
            'order',
            'view',
            'view_time',
        ],
    ]) ?>

</div>
