<?php

use app\widgets\UserSettingSwitcher;
use yii\helpers\Html;
use kartik\grid\GridView;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\users\Users */
/* @var $account app\models\users\Account */
/* @var $changePasswordForm \app\models\forms\ChangePasswordForm */
/* @var $searchModel app\models\AccountSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Настройки';
$this->params['breadcrumbs'][] = $this->title;

function getFindMonthSubscriptionLabel()
{
    $setting = \app\models\Settings::findByKey('cost_subscription_find_letter');
    $value = number_format($setting->value, 0, ',', ' ');

    /** @var \app\models\users\Users $user */
    $user = Yii::$app->user->identity;

    if($user->subscription->findMonth->payed_datetime == null)
    {
        $endDateTime = '';
    } else {
        $endDateTime = \app\helpers\DatesCalculation::getNextRealMonth($user->subscription->findMonth->payed_datetime);
        $endDateTime = ' <span style="color: gray;">Оканчивается '.date('d.m.Y', strtotime($endDateTime)).'</span>';
    }

    return "Месячная подписка на пособия «Найди букву» {$value} руб. в месяц.".$endDateTime;
}

function getFixMonthSubscriptionLabel()
{
    $setting = \app\models\Settings::findByKey('cost_subscription_fix_letter');
    $value = number_format($setting->value, 0, ',', ' ');

    /** @var \app\models\users\Users $user */
    $user = Yii::$app->user->identity;

    if($user->subscription->fixMonth->payed_datetime == null)
    {
        $endDateTime = '';
    } else {
        $endDateTime = \app\helpers\DatesCalculation::getNextRealMonth($user->subscription->fixMonth->payed_datetime);
        $endDateTime = ' <span style="color: gray;">Оканчивается '.date('d.m.Y', strtotime($endDateTime)).'</span>';
    }

    return "Месячная подписка на пособия «Почини букву» {$value} руб. в месяц.".$endDateTime;
}


$confirmMessageAttributes = [];

if($model->isEmailConfirmed == false){
    $confirmMessageAttributes[] = 'Подтвердите E-mail';
}

if($model->isTelephoneConfirmed == false){
    $upBonus = \app\models\Settings::findByKey('up_balance_after_phone_confirm_amount')->value;
    $confirmMessageAttributes[] = "Подтвердите Телефон и получите на баланс {$upBonus} руб.";
}

?>
<div class="account-index">

    <?php if(count($confirmMessageAttributes) > 0): ?>
        <div class="row">
            <div class="col-md-12">
                <div class="alert alert-warning" role="alert" style="background: #fcb14b; margin-bottom: 10px;">
                    <h4 class="alert-heading">Полностью заполните свой профиль:</h4>
                    <ul>
                        <?php foreach ($confirmMessageAttributes as $message): ?>
                            <li><?=$message?></li>
                        <?php endforeach; ?>
                    </ul>
                </div>
            </div>
        </div>
    <?php endif; ?>

    <div class="row">
        <div class="col-md-6">
            <div class="panel panel-default">
                <div class="panel-body">
                    <h2 style="margin-bottom: 10px;">Личная информация</h2>
                    <?php $form = ActiveForm::begin()?>
                        <div class="col-md-12">
                            <?= $form->field($model,'fio')->textInput() ?>
                        </div>

                        <div class="col-md-12">
                            <?= $form->field($model,'login')->textInput(['disabled' => $model->isEmailConfirmed]) ?>
                        </div>
                        <?php if(Yii::$app->user->identity->isSuperAdmin()): ?>
                                <div class="col-md-12">
                                    <?= $form->field($account,'balance')->textInput() ?>
                                </div>
                        <?php endif; ?>

                        <?php if(Yii::$app->user->identity->isSuperAdmin() == false): ?>
                            <?php if($model->isTelephoneConfirmed): ?>
                                <div class="col-md-12">
                                    <?= $form->field($model,'telephone')->textInput(['disabled' => $model->isTelephoneConfirmed]) ?>
                                </div>
                            <?php else: ?>
                                <div class="col-md-8">
                                    <?= $form->field($model,'telephone')->textInput(['disabled' => $model->isTelephoneConfirmed]) ?>
                                </div>
                                <div class="col-md-4">
                                    <?= Html::a('Подтвердить', ['users/confirm-phone'], ['class' => 'btn btn-warning btn-block', 'style' => 'margin-top: 22px;']); ?>
                                </div>
                            <?php endif; ?>
                        <?php else: ?>
                            <div class="col-md-12">
                                <?= $form->field($model,'telephone')->textInput(['disabled' => $model->isTelephoneConfirmed]) ?>
                            </div>
                        <?php endif; ?>

                        <div class="col-md-12">
                            <?= $form->field($account,'town')->textInput() ?>
                        </div>

                        <div class="col-md-12">
                            <?= $form->field($account,'position')->textInput() ?>
                        </div>

                        <div class="col-md-12">
                            <?= Html::submitButton('Сохранить изменения', ['class' => 'btn btn-success']) ?>
                        </div>

                    <?php ActiveForm::end()?>

                </div>
            </div>
        </div>
        <?php if(Yii::$app->user->identity->isSuperAdmin()): ?>
            <div class="col-md-6">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <h2 style="margin-bottom: 20px;">Подписки</h2>

                        <div style="margin-bottom: 17px; display: inline-block;"><?=UserSettingSwitcher::widget(['attribute' => 'find_letter_data_subscribe', 'label' => getFindMonthSubscriptionLabel(), 'userId' => $model->id])?></div>
                        <div style="margin-bottom: 17px; display: inline-block;"><?=UserSettingSwitcher::widget(['attribute' => 'fix_letter_data_subscribe', 'label' => getFixMonthSubscriptionLabel(), 'userId' => $model->id])?></div>

                    </div>
                </div>
            </div>
        <?php else: ?>
            <div class="col-md-6">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <h2 style="margin-bottom: 20px;">Подписки</h2>

                        <div style="margin-bottom: 17px; display: inline-block;"><?=UserSettingSwitcher::widget(['attribute' => 'find_letter_data_subscribe', 'label' => getFindMonthSubscriptionLabel()])?></div>
                        <div style="margin-bottom: 17px; display: inline-block;"><?=UserSettingSwitcher::widget(['attribute' => 'fix_letter_data_subscribe', 'label' => getFixMonthSubscriptionLabel()])?></div>

                    </div>
                </div>
            </div>
        <?php endif; ?>
    </div>

    <div class="panel panel-default" style="margin-top: 15px;">
        <div class="panel-body">
            <h2 style="margin-bottom: 20px;">Почтовые уведомления</h2>

            <?php if(Yii::$app->user->identity->isSuperAdmin()): ?>
                <div style="margin-bottom: 17px; display: block;"><?=UserSettingSwitcher::widget(['attribute' => 'after_balance_up', 'label' => 'При пополнении баланса', 'userId' => $model->id])?></div>
                <div style="margin-bottom: 17px; display: block"><?=UserSettingSwitcher::widget(['attribute' => 'after_letter_create', 'label' => 'При пополнении баланса', 'userId' => $model->id])?></div>
                <div style="margin-bottom: 17px; display: block;"><?=UserSettingSwitcher::widget(['attribute' => 'balance_up_reminder', 'label' => 'Напоминание о пополнении баланса', 'userId' => $model->id])?></div>
                <div style="margin-bottom: 17px; display: block;"><?=UserSettingSwitcher::widget(['attribute' => 'news', 'label' => 'Новости и акции сервиса', 'userId' => $model->id])?></div>
            <?php else: ?>
                <div style="margin-bottom: 17px; display: block;"><?=UserSettingSwitcher::widget(['attribute' => 'after_balance_up', 'label' => 'При пополнении баланса'])?></div>
                <div style="margin-bottom: 17px; display: block"><?=UserSettingSwitcher::widget(['attribute' => 'after_letter_create', 'label' => 'При пополнении баланса'])?></div>
                <div style="margin-bottom: 17px; display: block;"><?=UserSettingSwitcher::widget(['attribute' => 'balance_up_reminder', 'label' => 'Напоминание о пополнении баланса'])?></div>
                <div style="margin-bottom: 17px; display: block;"><?=UserSettingSwitcher::widget(['attribute' => 'news', 'label' => 'Новости и акции сервиса'])?></div>
            <?php endif; ?>

        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <div class="panel panel-default" style="margin-top: 15px;">
                <div class="panel-body">
                    <?php if(Yii::$app->session->hasFlash('success_password_changed')): ?>
                        <div class="alert alert-success" role="alert" style="margin-bottom: 10px;">
                            <p><?=Yii::$app->session->getFlash('success_password_changed')?></p>
                        </div>
                    <?php endif; ?>
                    <h2 style="margin-bottom: 10px;">Изменить пароль</h2>
                    <?php $form = ActiveForm::begin()?>
                        <?= $form->field($changePasswordForm,'oldPassword')->passwordInput() ?>
                        <?= $form->field($changePasswordForm,'newPassword')->passwordInput() ?>

                    <?= Html::submitButton('Сохранить пароль', ['class' => 'btn btn-success']) ?>
                    <?php ActiveForm::end()?>

                </div>
            </div>
        </div>
    </div>

    <?php if(Yii::$app->user->identity->isSuperAdmin()): ?>
        <div class="row">
            <div class="col-md-12">
                <div class="panel-wrapper" style="margin-top: 15px;">
                    <?=GridView::widget([
                        'id'=>'crud-datatable',
                        'dataProvider' => $findDataProvider,
//                    'filterModel' => $findSearchModel,
                        'pjax'=>true,
                        'columns' => [
                            [
                                'class'=>'\kartik\grid\DataColumn',
                                'attribute'=>'id',
                            ],
                            [
                                'class'=>'\kartik\grid\DataColumn',
                                'attribute'=>'page_format',
                            ],
                            [
                                'class'=>'\kartik\grid\DataColumn',
                                'attribute'=>'page_count',
                            ],
                            [
                                'class'=>'\kartik\grid\DataColumn',
                                'attribute'=>'letters_count',
                            ],
                            [
                                'class'=>'\kartik\grid\DataColumn',
                                'attribute'=>'letters_composition',
                            ],
                        ],
                        'toolbar'=> null,
                        'striped' => true,
                        'condensed' => true,
                        'responsive' => true,
                        'panel' => [
                            'type' => 'warning',
                            'heading' => 'История пособий «Найди букву»',
                        ]
                    ])?>
                </div>
            </div>
            </div>
            <div class="col-md-12">
                <div class="panel-wrapper" style="margin-top: 15px;">
                    <?=GridView::widget([
                        'id'=>'crud-datatable',
                        'dataProvider' => $fixDataProvider,
//                    'filterModel' => $findSearchModel,
                        'pjax'=>true,
                        'columns' => [
                            [
                                'class'=>'\kartik\grid\DataColumn',
                                'attribute'=>'id',
                            ],
                            [
                                'class'=>'\kartik\grid\DataColumn',
                                'attribute'=>'page_format',
                            ],
                            [
                                'class'=>'\kartik\grid\DataColumn',
                                'attribute'=>'page_count',
                            ],
                            [
                                'class'=>'\kartik\grid\DataColumn',
                                'attribute'=>'letters_count',
                            ],
                            [
                                'class'=>'\kartik\grid\DataColumn',
                                'attribute'=>'letters_composition',
                            ],
                        ],
                        'toolbar'=> null,
                        'striped' => true,
                        'condensed' => true,
                        'responsive' => true,
                        'panel' => [
                            'type' => 'warning',
                            'heading' => 'История пособий «Найди букву»',
                        ]
                    ])?>
                </div>
            </div>
        </div>
    <?php endif; ?>

</div>

<?php

$script = <<< JS
  $("#account-town").suggestions({
    token: "b9be7b7c829ded21856aa2d40e573275ae06f432",
    type: "ADDRESS",
    hint: false,
    bounds: "city",
    constraints: {
      label: "",
      locations: { city_type_full: "город" }
    },
    onSelect: function(suggestion) {
      console.log(suggestion);
    }
  });
JS;

$this->registerJs($script, \yii\web\View::POS_READY);


?>