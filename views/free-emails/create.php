<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\FreeEmails */

?>
<div class="free-emails-create">
    <div class="panel panel-success">
        <div class="panel-heading">
            <h4 class="panel-title">Новое письмо</h4>
        </div>
        <div class="panel-body">
            <?= $this->render('_form', [
                'model' => $model,
            ]) ?>
        </div>
    </div>
</div>
