<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\FreeEmails */
?>
<div class="free-emails-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
