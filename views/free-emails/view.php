<?php

use kartik\grid\GridView;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\FreeEmails */

$this->title = "Рассылка #{$model->id}";

?>
<div class="free-emails-view">

    <div class="row">
        <div class="col-md-4">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Информация</h3>
                </div>
                <div class="panel-body">
                    <?= DetailView::widget([
                        'model' => $model,
                        'attributes' => [
                            'id',
                            'subject',
                            'content:ntext',
            //            'attachment:ntext',
            //            'send_datetime',
            //            'created_at',
                        ],
                    ]) ?>
                </div>
            </div>
        </div>
        <div class="col-md-8">
                    <?= $this->render('@app/views/free-email-recipients/index.php', [
                        'dataProvider' => $dataProvider,
                        'searchModel' => $searchModel
                    ]) ?>
        </div>
    </div>

</div>
