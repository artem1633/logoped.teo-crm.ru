<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\file\FileInput;

/* @var $this yii\web\View */
/* @var $model app\models\FreeEmails */
/* @var $form yii\widgets\ActiveForm */

$attachments = \yii\helpers\ArrayHelper::getColumn($model->freeEmailsAttachments, 'path');
for ($i = 0; $i < count($attachments); $i++){
    $attachments[$i] = '/'.$attachments[$i];
}

?>

<div class="free-emails-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'recipient')->widget(\kartik\select2\Select2::class, [
            'data' => \yii\helpers\ArrayHelper::map(\app\models\users\Users::find()->where(['type' => 1])->all(), 'id', 'fio'),
            'options' => [
                'multiple' => true,
            ],
            'pluginOptions' => [
                'tags' => true,
            ],
    ]) ?>

    <?= $form->field($model, 'subject')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'content')->widget(\dosamigos\ckeditor\CKEditor::class, []) ?>

    <?= $form->field($model, 'attachment[]')->widget(FileInput::class,[
        'options' => ['multiple' => true,],
        'pluginOptions' => [
            'layoutTemplates' => [
                'actionDownload' => '<a class="{downloadClass}" href="{downloadUrl}" target="_blank" data-pjax="0" title="Загрузить" download="{caption}"><i class="glyphicon glyphicon-download"></i></a> '
            ],
            'uploadUrl' => \yii\helpers\Url::toRoute(['upload-attachment', 'id' => $model->id]),
            'initialPreview' => $attachments,
            'previewFileType' => 'any',
            'overwriteInitial' => false,
            'initialPreviewAsData' => true,
            'preferIconicPreview' => true,
//            'initialPreviewConfig' => $previewConfig,
//            'initialPreviewDownloadUrl' => \yii\helpers\Url::to('uploads/' . $task->id . '/{filename}', true),
        ],
    ]) ?>

  
	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton('<i class="fa fa-envelope"></i> Запустить рассылку', ['class' => 'btn btn-success btn-block']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>
