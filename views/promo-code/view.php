<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\PromoCode */
?>
<div class="promo-code-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            'description',
            'start_date',
            'end_date',
            'endless',
            'action',
        ],
    ]) ?>

</div>
