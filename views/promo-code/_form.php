<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;

/* @var $this yii\web\View */
/* @var $model app\models\PromoCode */
/* @var $form yii\widgets\ActiveForm */

?>

<div class="promo-code-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
        <div class="col-md-3">
            <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-9">
            <?= $form->field($model, 'description')->textInput(['maxlength' => true]) ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-10"><?= $form->field($model, 'dates')->widget(\kartik\daterange\DateRangePicker::class, []) ?></div>
        <div class="col-md-2"></div></br><?= $form->field($model, 'endless')->checkbox() ?></div>
    <div class="row">
        <div class="col-md-8">
            <?= $form->field($model, 'action')->dropDownList($model::getActions()) ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'action_value')->textInput() ?>
        </div>
    </div>



  
	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Изменить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>
