<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\PromoCode */
?>
<div class="promo-code-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
