<?php

use yii\helpers\Html;
use kartik\form\ActiveForm;

/**
 * @var \app\models\forms\PhoneConfirmForm $model
 * @var \app\models\PhonesConfirmations $code
 * @var boolean $error
 * @var string $errorCode
 */

?>


<div class="row">
    <div class="col-md-12">
            <div class="panel panel-default" style="width: 30%; margin: 5% 35%;">
                <div class="panel-heading">
                    <h3 class="panel-title" style="width: 100%; text-align: center;">Подтверждение телефона</h3>
                </div>
                <div class="panel-body">
                    <?php $form = ActiveForm::begin() ?>
                    <div class="row">
                        <div class="col-md-12" style="text-align: center;">
                            <div class="top-text" style="margin-bottom: 10px;">
                                <p>Код подтверждения отправлен на телефон:</p>
                                <h3><?=$code->phone?></h3>
                                <?= Html::a('Изменить телефон', ['account/index']) ?>
                                <?php if($error): ?>
                                    <div class="alert alert-danger"><b>Во время отправки sms произошла ошибка:</b> <?=$errorCode?> <b>Обратитесь к администрации</b></div>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <?= $form->field($model, 'code')->textInput(['disabled' => !$code->isActive]) ?>
                        </div>
                    </div>
                    <?php if($code->isActive): ?>
                        <div class="row">
                            <div class="col-md-12">
                                <?= Html::submitButton('Готово', ['class' => 'btn btn-success btn-block']) ?>
                            </div>
                        </div>
                    <?php else: ?>
                        <div class="row">
                            <div class="col-md-12">
                                <p class="text-danger" style="text-align: center;">Срок действия кода истек</p>
                                <?= Html::a('Отправить ещё раз', ['users/confirm-phone', 'again' => 1], ['class' => 'btn btn-info btn-block']) ?>
                            </div>
                        </div>
                    <?php endif; ?>
                    <?php ActiveForm::end() ?>
                </div>
            </div>
    </div>
</div>

