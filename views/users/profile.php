<?php

use yii\helpers\Html;
use johnitvn\ajaxcrud\CrudAsset; 
use yii\widgets\Pjax;
use yii\bootstrap\Modal;
use app\models\Users;

CrudAsset::register($this);
$model = Users::findOne(Yii::$app->user->identity->id);

if (!file_exists('avatars/'.$model->foto) || $model->foto == '') {
    $path = 'http://' . $_SERVER['SERVER_NAME'].'/examples/images/users/avatar.jpg';
} else {
    $path = 'http://' . $_SERVER['SERVER_NAME'].'/avatars/'.$model->foto;
}

?>

<?php Pjax::begin(['enablePushState' => false, 'id' => 'profile-pjax']) ?>
<div class="row">
    <div class="">
        <!-- CONTACT ITEM -->
        <div class="profile-container">
            <div class="panel panel-default">
                <div class="panel-body profile">
                    <div class="profile-image">
                        <img src="<?= $path?>" alt="Nadia Ali">
                    </div>
                    <div class="profile-data">
                        <div class="profile-data-name"><?=$model->fio?></div>
                        <div class="profile-data-title"><?=$model->getTypeDescription()?></div>
                    </div>
                    <div class="profile-controls">
                        <?= Html::a('<span class="fa fa-pencil"></span>', ['/users/change', 'id' => $model->id], [ 'role' => 'modal-remote', 'title'=> 'Профиль','class'=>'profile-control-left']); ?>
                        <?= Html::a('<span class="fa fa-download"></span>', ['/users/avatar'], [ 'role' => 'modal-remote', 'title'=> 'Загрузить аватар','class'=>'profile-control-right']); ?>
                    </div>
                </div>                                
                <div class="panel-body">                                    
                    <div class="contact-info">
                        <p><small>Телефон</small><br><?='&nbsp;'.$model->telephone?></p>
                        <p><small>Логин</small><br><?=$model->login?></p>
                        <p><small>Id чат телеграма</small><br><?=' '.$model->telegram_id?></p>
                        <p><small>Уникальный код для подключения к боту</small><br><?=' '.$model->unique_code_for_telegram?></p>
                        <p><small>Страница компании</small><br> <?= Html::a( 'https://' . $_SERVER['SERVER_NAME'] . '/' . $model->utm , [ '/'.$model->utm ], ['data-pjax'=>'0', 'title'=> 'Создать', 'target'=>'_blank', ]); ?></p>
                        <p><small>Описание компании</small><br><?=' '.$model->description?></p>                              
                    </div>
                </div>                                
            </div>
    </div>
        <!-- END CONTACT ITEM -->
    </div>                       
</div>
<?php Pjax::end() ?>

<?php Modal::begin([
    "id"=>"ajaxCrudModal",
    "options" => [
        "tabindex" => false,
    ],
    "footer"=>"",// always need it for jquery plugin
])?>
<?php Modal::end(); ?>