<?php
use yii\helpers\Url;
use yii\helpers\Html;

return [
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'created_at',
        'label' => 'Дата и время регистрации',
        'format' => ['date', 'php:d.m.Y H:i:s']
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'fio',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'login',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'telephone',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'account.town',
        'label' => 'Город'
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'account.position',
        'label' => 'Должность'
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'account.balance',
        'label' => 'Баланс'
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'account.balance',
        'label' => 'Всего пополнений на сумму',
        'content' => function($model){
            return \app\models\Balances::find()->where(['user_id' => $model->id, 'dk' => \app\models\Balances::DEBIT])->sum('amount');
        },
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'account.balance',
        'label' => 'Кол-во заказов «Найди букву»',
        'content' => function($model){
            return \app\models\FindLetter::find()->where(['user_id' => $model->id])->count();
        },
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'account.balance',
        'label' => 'Сумма потраченная на заказы «Найди букву»',
        'content' => function($model){
            return \app\models\Balances::find()->where(['user_id' => $model->id, 'category' => [
                \app\models\Balances::CATEGORY_SUBSCRIPTION_FIND,
                \app\models\Balances::CATEGORY_STORAGE_FIND,
            ], 'dk' => \app\models\Balances::CREDIT])->sum('amount');
        },
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'account.balance',
        'label' => 'Кол-во заказов «Почини букву»',
        'content' => function($model){
            return \app\models\FixLetter::find()->where(['user_id' => $model->id])->count();
        },
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'account.balance',
        'label' => 'Сумма потраченная на заказы «Почини букву»',
        'content' => function($model){
            return \app\models\Balances::find()->where(['user_id' => $model->id, 'category' => [
                \app\models\Balances::CATEGORY_SUBSCRIPTION_FIX,
                \app\models\Balances::CATEGORY_STORAGE_FIX,
            ], 'dk' => \app\models\Balances::CREDIT])->sum('amount');
        },
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'account.balance',
        'label' => 'Потраченных средств всего',
        'content' => function($model){
            return \app\models\Balances::find()->where(['user_id' => $model->id, 'dk' => \app\models\Balances::CREDIT])->sum('amount');
        },
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'account.balance',
        'label' => 'Последний использованный промокод',
        'content' => function($model){
            $promoCodeUsage = \app\models\PromoCodesUsages::find()->where(['user_id' => $model->id])->orderBy('last_usage_datetime desc')->one();
            if($promoCodeUsage != null){
                return $promoCodeUsage->promo_code_name;
            }

            return '';
        },
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'account.balance',
        'label' => 'Получено бонусных рублей',
        'content' => function($model){
            return \app\models\Balances::find()->where(['user_id' => $model->id, 'dk' => \app\models\Balances::DEBIT])->sum('amount_bonus');
        },
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'last_activity_datetime',
        'label' => 'Дата и время последней активности',
        'format' => ['date', 'php:d.m.Y H:i:s']
    ],
];