<?php

/**
 * @var $model \app\models\users\Users
 * @var $account \app\models\users\Account
 */

use yii\helpers\Html;
use yii\widgets\ActiveForm;

$type = Yii::$app->user->identity->type;
$user_id = Yii::$app->user->identity->id;

?>

<div class="users-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
        <div class="col-md-6 col-xs-6">
            <?= $form->field($model, 'fio')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-6 col-xs-6">
            <?= $form->field($model, 'login')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-12 col-xs-12">
            <?= $form->field($account, 'balance')->textInput(['maxlength' => true]) ?>
        </div>
    </div>

  
	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>
