<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $account \app\models\users\Account */
/* @var $model app\models\Users */
?>
<div class="users-update">

    <?= $this->render('_form', [
        'model' => $model,
        'account' => $account
    ]) ?>

</div>
