<?php

use yii\helpers\Html;
use dosamigos\chartjs\ChartJs;
use app\models\Resume;
use app\models\ResumeStatus; 

$this->title = 'Рабочий стол';

/** @var \app\models\Messages[] $messages */

///** @var \app\models\users\Users $user */
//$user = Yii::$app->user->identity;

//var_dump($user->emailMarkers->unMark('long_time_no_enter'));

?>
<div class="site-index">
    <div class="row">


    </div>
    <br>

    <div class="row">
        <?php foreach($messages as $message): ?>
            <div class="col-md-12">
                <div class="panel panel-success panel-hidden-controls ">
                    <div class="panel-heading ui-draggable-handle">
                        <h3 class="panel-title">Сообщение</h3>
                        <ul class="panel-controls">
                            <li><a href="#" class="panel-fullscreen"><span class="fa fa-expand"></span></a></li>
                            <li><a href="#" class="panel-collapse"><span class="fa fa-angle-down"></span></a></li>
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="fa fa-cog"></span></a>
                                <ul class="dropdown-menu">
                                    <li><a href="#" class="panel-collapse"><span class="fa fa-angle-down"></span> Collapse</a></li>
                                    <li><a href="#" class="panel-refresh"><span class="fa fa-refresh"></span> Refresh</a></li>
                                </ul>
                            </li>
                            <li><a href="#" class="panel-remove"><span class="fa fa-times"></span></a></li>
                        </ul>
                    </div>
                    <div class="panel-body">
                        <?=$message->text_message?>
                    </div>
                    <div class="panel-footer">
                    </div>
                </div>
            </div>
        <?php endforeach; ?>
    </div>

    <br>
    <br>



        

</div>


<!-- <div class="col-md-12">
    <div>
        <canvas id="myChart" width="300" height="80" ></canvas>
    </div>
</div>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.1.4/Chart.min.js"></script> -->
<?php 
/*$this->registerJs(<<<JS

var ctx = document.getElementById('myChart').getContext('2d');
var myChart = new Chart(ctx, {
  type: 'line',
  data: {
    labels: ['M', 'T', 'W', 'T', 'F', 'S', 'S'],
    datasets: [{
      label: 'apples',
      data: [12, 19, 3, 17, 6, 3, 7],
      backgroundColor: "rgba(153,255,51,0.4)"
    }, {
      label: 'oranges',
      data: [2, 29, 5, 5, 2, 3, 10],
      backgroundColor: "rgba(255,153,0,0.4)"
    }]
  }
});

JS
);*/
?>