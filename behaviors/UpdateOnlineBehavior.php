<?php

namespace app\behaviors;

use Yii;
use yii\base\Behavior;
use yii\web\Controller;
use yii\base\Event;

/**
 * Class UpdateOnlineBehavior
 * @package app\models\behaviors
 */
class UpdateOnlineBehavior extends Behavior
{
    /**
     * @inheritdoc
     */
    public function events()
    {
        return [
            Controller::EVENT_AFTER_ACTION => 'setUserOnline',
        ];
    }

    /**
     * @param Event $event
     * @return null|bool
     */
    public function setUserOnline($event)
    {
         if(Yii::$app->user->isGuest){
            return false;
         }

        /** @var \app\models\users\Users $user */
         $user = Yii::$app->user->identity;

         if($user->last_activity_datetime != null){
             $now = time();
             $lastSeenTimestamp = strtotime($user->last_activity_datetime);

             if(($now - $lastSeenTimestamp) > 18000) { // 18000 — 5 часов
                 $hours = round(($now - $lastSeenTimestamp) / 3600);
             }
         }

         $user->last_activity_datetime = date('Y-m-d H:i:s');

         $user->save(false);
    }
}