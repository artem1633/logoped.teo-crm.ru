<?php

namespace app\behaviors;

use app\models\users\Users;
use Closure;
use Yii;
use yii\base\Behavior;
use yii\base\Event;

/**
 * Class PaymentBehavior
 * @package app\behaviors
 */
class PaymentBehavior extends Behavior
{
    /**
     * @var array
     */
    public $writeOffEvents;

    /**
     * {@inheritdoc}
     */
    public function events()
    {
        return array_fill_keys(
            array_keys($this->writeOffEvents),
            'handling'
        );
    }

    /**
     * @param Event $event
     */
    public function handling($event)
    {
        if(!empty($this->writeOffEvents[$event->name])){
            $options = $this->writeOffEvents[$event->name];
            $amount = floatval($this->getAmount($options['amount'], $event));
            $category = $this->getCategory($options['category'], $event);
            $comment = $this->getComment($options['comment'], $event);
            $condition = $this->getCondition($options['condition'], $event);

            if($condition === null){
                $condition = true;
            }

            if($condition)
            {
                $user = Users::findOne(Yii::$app->user->getId());
                if($user->wallet->hasAmount($amount)) {
                    $user->wallet->writeOff($amount, $category, $comment);
                }
            }
        }
    }

    /**
     * @param float|Closure $amount
     * @param Event $event
     * @return mixed
     */
    protected function getAmount($amount, $event)
    {
        if ($amount instanceof Closure || (is_array($amount) && is_callable($amount))) {
            return call_user_func($amount, $event);
        }

        return $amount;
    }

    /**
     * @param string|Closure $category
     * @param Event $event
     * @return mixed
     */
    protected function getCategory($category, $event)
    {
        if ($category instanceof Closure || (is_array($category) && is_callable($category))) {
            return call_user_func($category, $event);
        }

        return $category;
    }

    /**
     * @param string|Closure $comment
     * @param Event $event
     * @return mixed
     */
    protected function getComment($comment, $event)
    {
        if ($comment instanceof Closure || (is_array($comment) && is_callable($comment))) {
            return call_user_func($comment, $event);
        }

        return $comment;
    }

    /**
     * @param string|Closure $condition
     * @param Event $event
     * @return mixed
     */
    protected function getCondition($condition, $event)
    {
        if ($condition instanceof Closure || (is_array($condition) && is_callable($condition))) {
            return call_user_func($condition, $event);
        }

        return $condition;
    }
}