<?php

namespace app\behaviors;

use app\components\helpers\TagHelper;
use app\models\users\Users;
use Closure;
use Yii;
use yii\base\Behavior;
use yii\base\Event;
use yii\helpers\ArrayHelper;
use yii\validators\EmailValidator;

/**
 * Class MailsBehavior
 * @package app\behaviors
 */
class MailsBehavior extends Behavior
{
    /**
     * @var array
     */
    public $mails;

    /**
     * @var \app\models\users\Users[]
     */
    private $_users;

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        $users = Users::find()->all();
        $logins = ArrayHelper::getColumn($users, 'login');
        $this->_users = array_combine($logins, $users);
    }

    /**
     * {@inheritdoc}
     */
    public function events()
    {
        return array_fill_keys(
            array_keys($this->mails),
            'handling'
        );
    }

    /**
     * @param Event $event
     */
    public function handling($event)
    {
        if(!empty($this->mails[$event->name])){
            $options = $this->mails[$event->name];
            $subject = $this->getSubject($options['subject'], $event);
            $htmlBody = $this->getHtmlBody($options['htmlBody'], $event);
            $attach = $this->getAttach($options['attach'], $event);
            $to = $this->getTo($options['to'], $event);
            $hasTags = $this->getHasTags($options['hasTags'], $event);
            $customTags = $this->getCustomTags($options['customTags'], $event);

            if(is_array($to) == false)
            {
                $user = $this->_users[$to];
                if($user == null){
                    $user = Users::find()->where(['login' => $to])->one();
                    if($user == null)
                        return;
                }

                if(!isset($options['condition'])){
                    $condition = true;
                } else {
                    $condition = $this->getCondition($options['condition'], $event, $user);
                }

                if($condition)
                {
                    $emailValidator = new EmailValidator();
                    if($emailValidator->validate($to) == false){
                        return;
                    }

                    if($hasTags){
                        $htmlBody = TagHelper::handle($htmlBody, $user, $customTags);
                    }

                    $message = Yii::$app->mailer->compose()
                        ->setFrom('logoped.service@mail.ru')
                        ->setTo($to)
                        ->setSubject($subject)
                        ->setHtmlBody($htmlBody);

                    if($attach != null){
                        $message->attach($attach);
                    }

                    $message->send();
                }
            } else {
                $messages = [];
                foreach ($to as $toEmail)
                {
                    $user = $this->_users[$toEmail];
                    if($user == null){
                        $user = Users::find()->where(['login' => $toEmail])->one();
                        if($user == null)
                            continue;
                    }

                    if(!isset($options['condition'])){
                        $condition = true;
                    } else {
                        $condition = $this->getCondition($options['condition'], $event, $user);
                    }

                    if($condition)
                    {
                        $emailValidator = new EmailValidator();
                        if($emailValidator->validate($toEmail) == false)
                            continue;

                        if($hasTags){
                            $htmlBody = TagHelper::handle($htmlBody, $user, $customTags);
                        }

                        $message = Yii::$app->mailer->compose()
                            ->setFrom('logoped.service@mail.ru')
                            ->setTo($toEmail)
                            ->setSubject($subject)
                            ->setHtmlBody($htmlBody);

                        if($attach != null){
                            $message->attach($attach);
                        }

                        $messages[] = $message;
                    }
                }

                    Yii::$app->mailer->sendMultiple($messages);
            }
        }
    }

    /**
     * @param string|Closure $subject
     * @param Event $event
     * @return mixed
     */
    protected function getSubject($subject, $event)
    {
        if ($subject instanceof Closure || (is_array($subject) && is_callable($subject))) {
            return call_user_func($subject, $event);
        }

        return $subject;
    }

    /**
     * @param string|Closure $htmlBody
     * @param Event $event
     * @return mixed
     */
    protected function getHtmlBody($htmlBody, $event)
    {
        if ($htmlBody instanceof Closure || (is_array($htmlBody) && is_callable($htmlBody))) {
            return call_user_func($htmlBody, $event);
        }

        return $htmlBody;
    }

    /**
     * @param array|Closure $to
     * @param Event $event
     * @return mixed
     */
    protected function getTo($to, $event)
    {
        if ($to instanceof Closure || (is_array($to) && is_callable($to))) {
            return call_user_func($to, $event);
        }

        return $to;
    }

    /**
     * @param string|Closure $path
     * @param Event $event
     * @return mixed
     */
    protected function getAttach($path, $event)
    {
        if ($path instanceof Closure || (is_array($path) && is_callable($path))) {
            return call_user_func($path, $event);
        }

        return $path;
    }

    /**
     * @param string|Closure $customTags
     * @param Event $event
     * @return mixed
     */
    protected function getCustomTags($customTags, $event)
    {
        if ($customTags instanceof Closure || (is_array($customTags) && is_callable($customTags))) {
            return call_user_func($customTags, $event);
        }

        return $customTags != null ? $customTags : [];
    }

    /**
     * @param string|Closure $hasTags
     * @param Event $event
     * @return mixed
     */
    protected function getHasTags($hasTags, $event)
    {
        if ($hasTags instanceof Closure || (is_array($hasTags) && is_callable($hasTags))) {
            return call_user_func($hasTags, $event);
        }

        return $hasTags;
    }

    /**
     * @param bool|Closure $condition
     * @param Event $event
     * @param Users $user
     * @return mixed
     */
    protected function getCondition($condition, $event, $user)
    {
        if ($condition instanceof Closure || (is_array($condition) && is_callable($condition))) {
            return call_user_func($condition, $event, $user);
        }

        return $condition;
    }
}