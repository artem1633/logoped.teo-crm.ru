<?php

namespace app\components;

use yii\base\Component;
use \app\models\Fonts as FontsAR;
use yii\base\InvalidConfigException;

/**
 * Class Fonts
 * @package app\components
 */
class Fonts extends Component
{
    /**
     * @var string Путь до css-файла со щрифтами
     */
    public $fontsPath;

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        if($this->fontsPath == null){
            throw new InvalidConfigException('Путь генерации css-файла шрифтов должен быть указан');
        }

        parent::init();
    }

    /**
     * Генерирует css-файл со шрифтами
     */
    public function generateCssFontsFile()
    {
        /** @var \app\models\Fonts $fonts */
        $fonts = FontsAR::find()->all();

        $styles = '';

        foreach ($fonts as $font){
            $styles .= $this->generateFontFace($font) . "\n\n";
        }

        file_put_contents($this->fontsPath, $styles);
    }

    /**
     * Генерирует css для регистрации шрифта
     * @param \app\models\Fonts $font
     * @return string
     */
    public function generateFontFace($font)
    {
        $output = "@font-face {\n";

        $output .= "font-family: '{$font->font_family}';\n";

        if($font->eot_file_path != null){
            $output .= "\tsrc: url('{$font->eot_file_path}');\n";
        }
        if($font->eot_ie_file_path != null){
            $output .= "\tsrc: url('{$font->eot_ie_file_path}?#iefix') format('embedded-opentype');\n";
        }
        if($font->woff2_file_path != null){
            $output .= "\tsrc: url('{$font->woff2_file_path}') format('woff2');\n";
        }
        if($font->woff_file_path != null){
            $output .= "\tsrc: url('{$font->woff_file_path}') format('woff');\n";
        }
        if($font->ttf_file_path != null){
            $output .= "\tsrc: url('{$font->ttf_file_path}') format('truetype');\n";
        }
        if($font->svg_file_path != null){
            $output .= "\tsrc: url('{$font->svg_file_path}') format('svg');\n";
        }
        if($font->font_weight != null){
            $output .= "\tfont-weight: {$font->font_weight};\n";
        }
        if($font->font_style != null){
            $output .= "\tfont-style: {$font->font_style};\n";
        }

        $output .= "}";

        return $output;
    }
}