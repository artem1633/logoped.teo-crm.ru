<?php

namespace app\components\helpers;
use yii\helpers\ArrayHelper;


/**
 * Занимается обработкой текста с тэгами
 * Class TagHelper
 * @package app\components\helpers
 */
class TagHelper
{

    const LEFT_PRETAG = '{';
    const RIGHT_PRETAG = '}';

    /**
     * Обрабатывают строку
     * @param string $text
     * @param \yii\base\Model|\yii\base\Model[] $model
     * @param array $additionalTags
     * @return string
     */
    public static function handle($text, $model, $additionalTags = [])
    {
        $output = $text;
        if(!is_array($model) && $model instanceof \yii\base\Model)
        {
            $className = self::getClassName($model);
            $tags = [];

            foreach ($model->attributes as $name => $value)
            {
                $tags[self::LEFT_PRETAG."{$className}.{$name}".self::RIGHT_PRETAG] = $value;
            }

            $tags = ArrayHelper::merge($tags, $additionalTags);

            $output = str_ireplace(array_keys($tags), array_values($tags), $output);

            return $output;
        } else if (is_array($model))
        {
            foreach ($model as $modelItem)
            {
                $className = self::getClassName($modelItem);
                $tags = [];

                foreach ($modelItem->attributes as $name => $value)
                {
                    $tags[self::LEFT_PRETAG."{$className}.{$name}".self::RIGHT_PRETAG] = $value;
                }

                $tags = ArrayHelper::merge($tags, $additionalTags);

                $output = str_ireplace(array_keys($tags), array_values($tags), $output);
            }

            return $output;
        }

        return '';
    }

    /**
     * @param \yii\base\Model $model
     * @return string
     */
    private static function getClassName($model)
    {
        return lcfirst(\yii\helpers\StringHelper::basename(get_class($model)));
    }
}