<?php

namespace app\components;

use app\models\PromoCode;
use yii\base\Component;
use yii\base\InvalidConfigException;
use yii\web\NotFoundHttpException;

/**
 * Class PromoCodesManager
 * @package app\components
 */
class PromoCodesManager extends Component
{
    /**
     * @var PromoCode
     */
    public $model;

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        if($this->model == null){
            throw new InvalidConfigException('Model cant be null');
        }
    }

    /**
     * @param $code
     * @return PromoCodesManager|array|null|\yii\db\ActiveRecord
     * @throws NotFoundHttpException
     */
    public static function createFromCode($code)
    {
        $model = PromoCode::find()->where(['name' => $code])->one();

        if($model == null){
            throw new NotFoundHttpException();
        }

        $model = new self(['model' => $model]);

        return $model;
    }


    /**
     * @param int $operationId
     */
    public function getPayInfo($operationId)
    {
        $ch = curl_init('money.yandex.ru/api/operation-details');

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, ['operation_id' => $operationId]);

        $response = curl_exec($ch);

        $new = self::createFromCode($response['comment']);

        curl_close($ch);

        return $new;
    }

}