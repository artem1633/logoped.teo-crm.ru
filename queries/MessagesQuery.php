<?php

namespace app\queries;

use yii\db\ActiveQuery;

/**
 * Class MessagesQuery
 * @package app\queries
 */
class MessagesQuery extends ActiveQuery
{
    /**
     * Ищет сообщения только которые активны
     * @return $this
     */
    public function active()
    {
        $now = date('Y-m-d');

        $this->andWhere(['and', ['<=', 'start_date', $now], ['>=', 'end_date', $now]]);

        return $this;
    }
}