<?php

namespace app\queries;

use yii\db\ActiveQuery;

/**
 * Class LetterQuery
 * @package app\queries
 */
class LetterQuery extends ActiveQuery
{
    const STORAGE_TIME_LIMIT = 2.592e+6; // 30 дней

    /**
     * Ищет записи, превысившие лимит хранения
     * @return $this
     */
    public function storingExpired()
    {
        $now = time();
        $month = self::STORAGE_TIME_LIMIT;
        $result = $now - $month;
        $result = date('Y-m-d H:i:s', $result);

        $this->andWhere(['<', 'using_end_datetime', $result]);

        return $this;
    }
}