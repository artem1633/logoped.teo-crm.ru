<?php

namespace app\queries;

use yii\db\ActiveQuery;

/**
 * Class PromoCodeQuery
 * @package app\queries
 */
class PromoCodeQuery extends ActiveQuery
{
    /**
     * Ищет коды только которые активны
     * @return $this
     */
    public function active()
    {
        $now = date('Y-m-d');

        $this->andWhere(['or', ['and', ['<=', 'start_date', $now], ['>=', 'end_date', $now]], ['endless' => 1]]);

        return $this;
    }
}