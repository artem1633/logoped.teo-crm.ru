<?php

namespace app\queries;

use yii\db\ActiveQuery;

/**
 * Class SubscriptionQuery
 * @package app\queries
 */
class SubscriptionQuery extends ActiveQuery
{
    /**
     * Ищет записи, которые требуют оплаты
     * @return $this
     */
    public function notPayed()
    {
//        $now = time();
//        $month = 86400;
//        $result = $now - $month;
//        $result = date('Y-m-d H:i:s', $result);
//
//        $this->andWhere(['<', 'payed_datetime', $result]);
//
        return $this;
    }
}