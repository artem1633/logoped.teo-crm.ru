<?php

namespace app\source\pdfGenerators;

use app\behaviors\MailsBehavior;
use app\models\Letter;
use app\models\StrictEmails;
use Mpdf\Config\ConfigVariables;
use Mpdf\Config\FontVariables;
use Mpdf\Mpdf;
use Yii;
use kartik\mpdf\Pdf;

/**
 * Class FixPdfGenerator
 * @package app\source\pdfGenerators
 */
class FixPdfGenerator extends PdfGenerator
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            [
                'class' => MailsBehavior::class,
                'mails' => [
                    self::EVENT_PDF_GENERATED => [
                        'to' => function(){
                            return $this->model->user->login;
                        },
                        'subject' => 'Пособие «Почини букву»',
                        'htmlBody' => StrictEmails::findByKey('letter')->content,
                        'hasTags' => true,
                        'attach' => function(){
                            return $this->pdfPath;
                        },
                    ],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function generate()
    {
        $content = Yii::$app->controller->renderPartial('@app/views/_pdf/fix_letter', [
            'model' => $this->model
        ]);

        $defaultConfig = (new ConfigVariables())->getDefaults();
        $fontDirs = $defaultConfig['fontDir'];

        $defaultFontConfig = (new FontVariables())->getDefaults();
        $fontData = $defaultFontConfig['fontdata'];

        if($this->model->page_format == Letter::FORMAT_PAGE_A3){
            $format = Pdf::FORMAT_A3;
        } else if($this->model->page_format == Letter::FORMAT_PAGE_A4){
            $format = Pdf::FORMAT_A4;
        }

        $pdf = new Mpdf([
            'fontDir' => array_merge($fontDirs, [
                __DIR__.'/fonts'
            ]),
            'fontData' => [
                'ProximaNova' => [
                    'R' => 'ProximaNova-Regular.ttf',
                    'I' => 'ProximaNova-Regular.ttf',
                ],
                'RobotoRegular' => [
                    'R' => 'RobotoRegular.ttf',
                    'I' => 'RobotoRegular.ttf',
                ],
            ],
            'default_font' => 'RobotoRegular',
            'mode' => Pdf::MODE_UTF8,
            'format' => $format,
            'orientation' => Pdf::ORIENT_PORTRAIT,
            'destination' => Pdf::DEST_BROWSER,
            'content' => $content,
            'cssFile' => '@vendor/kartik-v/yii2-mpdf/assets/kv-mpdf-bootstrap.min.css',
            'cssInline' => file_get_contents('css/pdf/fix.css'),
        ]);

        if(is_dir('pdf') == false){
            mkdir('pdf');
        }

        $fileName = Yii::$app->security->generateRandomString();
        $this->pdfPath = "pdf/{$fileName}.pdf";

        $this->model->pdf_file = $this->pdfPath;
        $this->model->save(false);

        $pdf->WriteHTML($content);
        $pdf->Output($this->pdfPath, 'F');

        parent::generate();

        return $this->pdfPath;
    }
}