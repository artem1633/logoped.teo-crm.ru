<?php

namespace app\source\pdfGenerators;

use app\behaviors\MailsBehavior;
use app\models\FindLetterData;
use app\models\FindLettersImagesAttachments;
use app\models\Letter;
use app\models\StrictEmails;
use Mpdf\Mpdf;
use Yii;
use kartik\mpdf\Pdf;

/**
 * Class FindPdfGenerator
 * @package app\source\pdfGenerators
 */
class FindPdfGenerator extends PdfGenerator
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => MailsBehavior::class,
                'mails' => [
                    self::EVENT_PDF_GENERATED => [
                        'to' => function(){
                            return $this->model->user->login;
                        },
                        'subject' => 'Пособие «Найди букву»',
                        'htmlBody' => StrictEmails::findByKey('letter')->content,
                        'hasTags' => true,
                        'attach' => function(){
                            return $this->pdfPath;
                        },
                    ],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function generate()
    {
        $model = $this->model;
        $wroteDownLetterData = FindLetterData::find()->where(['find_letter_id' => $model->id, 'type' => FindLetterData::TYPE_RECIPE_LOW])->andFilterWhere(['>','amount', 0])->all();
        $wroteUpLetterData = FindLetterData::find()->where(['find_letter_id' => $model->id, 'type' => FindLetterData::TYPE_RECIPE_UP])->andFilterWhere(['>','amount', 0])->all();
        $printedUpLetterData = FindLetterData::find()->where(['find_letter_id' => $model->id, 'type' => FindLetterData::TYPE_PRINT_UP])->andFilterWhere(['>','amount', 0])->all();
        $printedDownLetterData = FindLetterData::find()->where(['find_letter_id' => $model->id, 'type' => FindLetterData::TYPE_PRINT_LOW])->andFilterWhere(['>','amount', 0])->all();
        $lettersImages = \app\models\FindLettersImages::find()->all();
        $imagesSymbols = [];

        foreach ($lettersImages as $image){
            $attachmentsWroteDown = \yii\helpers\ArrayHelper::getColumn(\app\models\FindLettersImagesAttachments::find()->where(['find_letter_image_id' => $image->id, 'type' => \app\models\FindLettersImagesAttachments::TYPE_WROTE_DOWN])->all(), 'path');
            $attachmentsWroteUp = \yii\helpers\ArrayHelper::getColumn(\app\models\FindLettersImagesAttachments::find()->where(['find_letter_image_id' => $image->id, 'type' => \app\models\FindLettersImagesAttachments::TYPE_WROTE_UP])->all(), 'path');
            $attachmentsPrintedUp = \yii\helpers\ArrayHelper::getColumn(\app\models\FindLettersImagesAttachments::find()->where(['find_letter_image_id' => $image->id, 'type' => \app\models\FindLettersImagesAttachments::TYPE_PRINTED_UP])->all(), 'path');
            $attachmentsPrintedDown = \yii\helpers\ArrayHelper::getColumn(\app\models\FindLettersImagesAttachments::find()->where(['find_letter_image_id' => $image->id, 'type' => \app\models\FindLettersImagesAttachments::TYPE_PRINTED_DOWN])->all(), 'path');

            $wroteDownData = null;
            foreach ($wroteDownLetterData as $data){
                if($data->symbol == $image->symbol) {
                    $wroteDownData = $data;
                }
            }
            $wroteUpData = null;
            foreach ($wroteUpLetterData as $data){
                if($data->symbol == $image->symbol) {
                    $wroteUpData = $data;
                }
            }
            $printedUpData = null;
            foreach ($printedUpLetterData as $data){
                if($data->symbol == $image->symbol) {
                    $printedUpData = $data;
                }
            }
            $printedDownData = null;
            foreach ($printedDownLetterData as $data){
                if($data->symbol == $image->symbol) {
                    $printedDownData = $data;
                }
            }

            $types = [];

            if($wroteDownData != null){
                $types[FindLettersImagesAttachments::TYPE_WROTE_DOWN] = [
                    'count' => $wroteDownData->amount,
                    'paths' => $attachmentsWroteDown,
                ];
            }
            if($wroteUpData != null){
                $types[FindLettersImagesAttachments::TYPE_WROTE_UP] = [
                    'count' => $wroteUpData->amount,
                    'paths' => $attachmentsWroteUp,
                ];
            }
            if($printedUpData != null){
                $types[FindLettersImagesAttachments::TYPE_PRINTED_UP] = [
                    'count' => $printedUpData->amount,
                    'paths' => $attachmentsPrintedUp,
                ];
            }
            if($printedDownData != null){
                $types[FindLettersImagesAttachments::TYPE_PRINTED_DOWN] = [
                    'count' => $printedDownData->amount,
                    'paths' => $attachmentsPrintedDown,
                ];
            }

//            $imagesSymbols[$image->symbol] = [
//                'types' => [
//                    \app\models\FindLettersImagesAttachments::TYPE_WROTE_DOWN => [
//                        'count' => $wroteDownData->amount,
//                        'paths' => $attachmentsWroteDown,
//                    ],
//                ],
//            ];
            $imagesSymbols[$image->symbol] = [
                'types' => $types,
            ];
        }
        $content = Yii::$app->controller->renderPartial('@app/views/_pdf/find_letter', [
            'model' => $model,
//            'thinLetterData' => $wroteDownLetterData,
            'imagesSymbols' => $imagesSymbols,
        ]);

        $defaultConfig = (new \Mpdf\Config\ConfigVariables())->getDefaults();
        $fontDirs = $defaultConfig['fontDir'];

        $defaultFontConfig = (new \Mpdf\Config\FontVariables())->getDefaults();
        $fontData = $defaultFontConfig['fontdata'];

        if($model->page_format == Letter::FORMAT_PAGE_A3){
            $format = Pdf::FORMAT_A3;
        } else if($model->page_format == Letter::FORMAT_PAGE_A4){
            $format = Pdf::FORMAT_A4;
        }

        $pdf = new Mpdf([
            'mode' => Pdf::MODE_UTF8,
            'format' => $format,
            'orientation' => Pdf::ORIENT_LANDSCAPE,
            'destination' => Pdf::DEST_BROWSER,
            'content' => $content,
            'cssFile' => '@vendor/kartik-v/yii2-mpdf/assets/kv-mpdf-bootstrap.min.css',
            'cssInline' => file_get_contents('css/pdf/find.css'),
//            'fontDir' => array_merge($fontDirs, [
//                __DIR__ . '/../web/fonts',
//            ]),
//            'fontdata' => $fontData + [
//                    'method' => [
//                        'R' => 'Method.ttf',
//                        'I' => 'Method.ttf',
//                    ],
//                    'propisi' => [
//                        'R' => 'Propisi.ttf',
//                        'I' => 'Propisi.ttf',
//                    ]
//                ],
//            'default_font' => 'method'
        ]);

        if(is_dir('pdf') == false){
            mkdir('pdf');
        }

        $fileName = Yii::$app->security->generateRandomString();
        $this->pdfPath = "pdf/{$fileName}.pdf";

        $model->pdf_file = $this->pdfPath;
        $model->save(false);

        $pdf->WriteHTML($content);
        $pdf->Output($this->pdfPath);

        parent::generate();

        return $this->pdfPath;
    }
}