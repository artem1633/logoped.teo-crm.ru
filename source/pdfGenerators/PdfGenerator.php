<?php

namespace app\source\pdfGenerators;

use app\models\Letter;
use yii\base\Component;

/**
 * Class PdfGenerator
 * @package app\source\pdfGenerators
 */
abstract class PdfGenerator extends Component
{
    const EVENT_PDF_GENERATED = 'pdf_generated';

    /**
     * @var string
     */
    protected $pdfPath;

    /**
     * @var Letter
     */
    public $model;

    /**
     * pdfGenerator constructor.
     * @param Letter $model
     * @param array $config
     */
    public function __construct($model, array $config = [])
    {
        $this->model = $model;
        parent::__construct($config);
    }

    /**
     * Должен реализовывать генерацию PDF-документа
     * и возвращать путь до PDF-документа
     */
    public function generate()
    {
        $this->trigger(self::EVENT_PDF_GENERATED);
    }
}