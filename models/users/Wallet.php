<?php
namespace app\models\users;

use app\models\Balances;
use Yii;
use yii\base\Component;
use yii\db\Transaction;

/**
 * Class Wallet
 * @package app\models\users
 */
class Wallet extends Component
{
    const EVENT_WROTE_OFF = 'wrote_off';

    /**
     * @var Users
     */
    private $_user;

    /**
     * @var Account
     */
    private $_userAccount;

    /**
     * Wallet constructor.
     * @param Users $user
     * @param array $config
     */
    public function __construct($user, array $config = [])
    {
        $this->_user = $user;
        $this->_userAccount = $user->account;
        parent::__construct($config);
    }

    /**
     * @param float $amount
     * @param string|null $category
     * @param string|null $comment
     * @return bool
     */
    public function writeOff($amount, $category = null, $comment = null)
    {
        /** @var Transaction $transaction */
        $transaction = Yii::$app->db->beginTransaction();
        try{

            $this->_userAccount->balance -= $amount;
            $this->_userAccount->save(false);

            $balance = new Balances([
                'amount' => $amount,
                'user_id' => $this->_user->id,
                'dk' => Balances::CREDIT,
                'category' => $category,
                'comment' => $comment
            ]);

            $balance->save(false);

            $transaction->commit();

            $this->trigger(self::EVENT_WROTE_OFF);

            return true;
        } catch(\Exception $e)
        {
            $transaction->rollBack();
            return false;
        }
    }

    /**
     * @param $amount
     * @param string|null $category
     * @param string|null $comment
     * @return bool
     */
    public function writeUpBonus($amount, $category = null, $comment = null)
    {
        /** @var Transaction $transaction */
        $transaction = Yii::$app->db->beginTransaction();
        try{

            $this->_userAccount->balance += $amount;
            $this->_userAccount->save(false);

            $balance = new Balances([
                'amount' => $amount,
                'amount_bonus' => $amount,
                'user_id' => $this->_user->id,
                'dk' => Balances::DEBIT,
                'category' => $category,
                'comment' => $comment
            ]);

            $balance->save(false);

            $transaction->commit();

            $this->trigger(self::EVENT_WROTE_OFF);

            return true;
        } catch(\Exception $e)
        {
            $transaction->rollBack();
            return false;
        }
    }

    /**
     * Проверяет есть ли указанная сумма на счету
     * @param float $amount
     * @return bool
     */
    public function hasAmount($amount)
    {
        return $this->_userAccount->balance >= $amount;
    }
}