<?php

namespace app\models\users;

use app\models\UsersEmailNotifications;
use yii\base\Component;
use yii\base\InvalidConfigException;

/**
 * Class EmailMarkers
 * @package app\models\users
 *
 * Отвечает за отметку уведомлен ли пользователь о чем либо или нет
 */
class EmailMarkers extends Component
{
    /**
     * @var Users
     */
    public $user;

    /**
     * @var \app\models\UsersEmailNotifications
     */
    private $notifications;

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();

        if($this->user == null){
            throw new InvalidConfigException('$user property must be required');
        }

        $notifications = UsersEmailNotifications::findOne(['user_id' => $this->user->id]);

        if($notifications == null){
            $this->notifications = new UsersEmailNotifications(['user_id' => $this->user->id]);
        } else {
            $this->notifications = $notifications;
        }
    }

    /**
     * @param string $markerName
     * @return bool
     */
    public function mark($markerName)
    {
        if($this->notifications->hasAttribute($markerName)){
            $this->notifications->$markerName = 1;
            return $this->notifications->save(false);
        }

        return false;
    }

    /**
     * @param string $markerName
     * @return bool
     */
    public function unMark($markerName)
    {
        if($this->notifications->hasAttribute($markerName)){
            $this->notifications->$markerName = 0;
            return $this->notifications->save(false);
        }

        return false;
    }

    /**
     * @param string $markerName
     * @return bool
     */
    public function isMarked($markerName)
    {
        if($this->notifications->hasAttribute($markerName)){
            return $this->notifications->$markerName == 1;
        }

        return false;
    }
}