<?php

namespace app\models\users;

use app\models\PromoCode;
use app\models\PromoCodesUsages;
use yii\base\Component;

/**
 * Class PromoCodesUsager
 * @package app\models\users
 * Записывает, когда пользователь использует
 * промокоды в БД (таблица promo_codes_usages)
 */
class PromoCodesUsager extends Component
{
    /**
     * @var Users
     */
    private $_user;

    /**
     * PromoCodesUsager constructor.
     * @param Users $user
     * @param array $config
     */
    public function __construct($user, array $config = [])
    {
        $this->_user = $user;
        parent::__construct($config);
    }

    /**
     * @param PromoCode $promoCode
     */
    public function usage($promoCode)
    {
        /** @var PromoCodesUsages $usage */
        $usage = PromoCodesUsages::find()->where(['user_id' => $this->_user->id, 'promo_code_id' => $promoCode->id])->one();
        if($usage != null){
            $usage->count = $usage->count + 1;
            $usage->last_usage_datetime = date('Y-m-d H:i:s');
            $usage->save(false);
        } else {
            $usage = new PromoCodesUsages([
                'user_id' => $this->_user->id,
                'promo_code_id' => $promoCode->id,
                'promo_code_name' => $promoCode->name,
                'count' => 1,
            ]);
            $usage->save(false);
        }
    }
}