<?php

namespace app\models\users;

use app\models\FindLetter;
use app\models\FixLetter;
use app\models\PhonesConfirmations;
use app\models\PromoCodesUsages;
use app\models\UsersEmailNotifications;
use app\models\UsersSettings;
use PharIo\Manifest\Email;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\helpers\Html;
use yii\helpers\Url;


/**
 * This is the model class for table "users".
 *
 * @property int $id
 * @property string $fio
 * @property string $login
 * @property string $password
 * @property string $telephone
 * @property int $type
 * @property string $telegram_id
 * @property int $email_confirmed
 * @property int $telephone_confirmed
 * @property string $last_activity_datetime Дата и время последней активности
 * @property string $created_at
 *
 * @property string $emailConfirmLinkHTML
 * @property bool $isEmailConfirmed
 * @property bool $isTelephoneConfirmed
 * @property Subscription $subscription
 *
 * @property Account[] $accounts
 * @property FindLetter[] $findLetters
 * @property FixLetter[] $fixLetters
 * @property Account $account
 * @property UsersSettings $settings
 * @property UsersSettings $usersSettings
 * @property Wallet $wallet
 * @property EmailMarkers $emailMarkers
 * @property PromoCodesUsager $promoCodesUsager
 */
class Users extends \yii\db\ActiveRecord
{
    const EVENT_EMAIL_CHANGED = 'email_changed';
    const EVENT_TELEPHONE_CHANGED = 'telephone_changed';

    const SUPER_COMPANY = '0';
    const COMPANY = '1';

    public $new_password;
    public $file;
    public $page;

    private $_subscription;
    private $_wallet;
    private $_emailMarkers;
    private $_promoCodesUsager;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'users';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['type'], 'integer'],
            [['fio', 'login', 'password', 'telephone', 'telegram_id'], 'string', 'max' => 255],
            [['create_at', 'email_confirmed', 'telephone_confirmed', 'last_activity_datetime'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::class,
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => null,
                'value' => date('Y-m-d H:i:s'),
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'fio' => 'ФИО',
            'login' => 'E-mail',
            'password' => 'Password',
            'telephone' => 'Телефон',
            'type' => 'Type',
            'telegram_id' => 'Telegram ID',
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function beforeSave($insert)
    {
        if ($this->isNewRecord) {
            $this->password = Yii::$app->security->generatePasswordHash($this->password);
            
        }

        return parent::beforeSave($insert);
    }

    /**
     * {@inheritdoc}
     */
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

        $changedAttributes = array_keys($changedAttributes);
        if(in_array('login', $changedAttributes)){
            $this->trigger(self::EVENT_EMAIL_CHANGED);
        }
        if(in_array('telephone', $changedAttributes)){
            $confirm  = PhonesConfirmations::find()->where(['user_id' => $this->id])->one();
            if($confirm != null){
                $confirm->delete();
            }

            $this->trigger(self::EVENT_TELEPHONE_CHANGED);
        }
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAccounts()
    {
        return $this->hasMany(Account::class, ['user_id' => 'id']);
    }

    /**
     * @return Account
     */
    public function getAccount()
    {
        $account = Account::findOne(['user_id' => $this->id]);
        if($account == null) {
            $account = new Account(['user_id' => $this->id]);
            $account->save(false);
        }

        return $account;
    }

    /**
     * @return UsersSettings|array|null|\yii\db\ActiveRecord
     */
    public function getSettings()
    {
        $settings = UsersSettings::find()->where(['user_id' => $this->id])->one();

        if($settings == null)
        {
            $settings = new UsersSettings(['user_id' => $this->id]);
            $settings->save(false);
        }

        return $settings;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsersSettings()
    {
        return $this->hasOne(UsersSettings::class, ['user_id' => 'id']);
    }

    /**
     * @return Subscription
     */
    public function getSubscription()
    {
        if($this->_subscription == null){
            $this->_subscription = new Subscription($this);
        }

        return $this->_subscription;
    }

    /**
     * @return Wallet
     */
    public function getWallet()
    {
        if($this->_wallet == null){
            $this->_wallet = new Wallet($this);
        }

        return $this->_wallet;
    }

    /**
     * @return PromoCodesUsager
     */
    public function getPromoCodesUsager()
    {
       if($this->_promoCodesUsager == null){
           $this->_promoCodesUsager = new PromoCodesUsager($this);
       }

       return $this->_promoCodesUsager;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFindLetters()
    {
        return $this->hasMany(FindLetter::class, ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFixLetters()
    {
        return $this->hasMany(FixLetter::class, ['user_id' => 'id']);
    }

    /**
     * @return EmailMarkers
     */
    public function getEmailMarkers()
    {
        if($this->_emailMarkers == null){
            $this->_emailMarkers = new EmailMarkers(['user' => $this]);
        }

        return $this->_emailMarkers;
    }

    /**
     * @return bool
     */
    public function getIsEmailConfirmed()
    {
        return $this->email_confirmed == 1;
    }

    /**
     * @return string
     */
    public function getEmailConfirmLinkHTML()
    {
        $emailHash = Yii::$app->security->generatePasswordHash($this->login);
        $token = "{$this->id}\$ep{$emailHash}";
        $url = Url::toRoute(['users/confirm-email', 'confirmToken' => $token], true);
        return Html::a($url, $url);
    }

    /**
     * @return bool
     */
    public function getIsTelephoneConfirmed()
    {
        return $this->telephone_confirmed == 1;
    }

    /**
     * @return bool
     */
    public function isSuperAdmin()
    {
        return $this->type == 0;
    }
}
