<?php

namespace app\models\users;

use app\models\subscriptions\Subscriptions;
use app\models\subscriptions\SubscriptionType;
use yii\base\Component;

/**
 * Class Subscription
 * @package app\models\users
 *
 * @property Subscriptions $findDaily
 * @property Subscriptions $fixDaily
 * @property Subscriptions $findMonth
 * @property Subscriptions $fixMonth
 */
class Subscription extends Component
{
    /**
     * @var Users
     */
    private $_user;

    /**
     * @var Subscriptions
     */
    private $_findDaily;

    /**
     * @var Subscriptions
     */
    private $_fixDaily;

    /**
     * @var Subscriptions
     */
    private $_findMonth;

    /**
     * @var Subscriptions
     */
    private $_fixMonth;


    /**
     * Subscription constructor.
     * @param Users $user
     * @param array $config
     */
    public function __construct($user, array $config = [])
    {
        $this->_user = $user;
        parent::__construct($config);
    }

    /**
     * @return Subscriptions
     */
    public function getFindDaily()
    {
        if($this->_findDaily != null){
            return $this->_findDaily;
        } else {
            $this->_findDaily = Subscriptions::find()->where(['user_id' => $this->_user->id, 'type' => Subscriptions::TYPE_FIND_DAILY])->one();
        }

        if($this->_findDaily == null)
        {
            $this->_findDaily = new Subscriptions(['user_id' => $this->_user->id, 'type' => Subscriptions::TYPE_FIND_DAILY]);
            $this->_findDaily->save(false);
        }

        return $this->_findDaily;
    }

    /**
     * @return Subscriptions
     */
    public function getFixDaily()
    {
        if($this->_fixDaily != null){
            return $this->_fixDaily;
        } else {
            $this->_fixDaily = Subscriptions::find()->where(['user_id' => $this->_user->id, 'type' => Subscriptions::TYPE_FIX_DAILY])->one();
        }

        if($this->_fixDaily == null)
        {
            $this->_fixDaily = new Subscriptions(['user_id' => $this->_user->id, 'type' => Subscriptions::TYPE_FIX_DAILY]);
            $this->_fixDaily->save(false);
        }

        return $this->_fixDaily;
    }

    /**
     * @return Subscriptions
     */
    public function getFindMonth()
    {
        if($this->_findMonth != null){
            return $this->_findMonth;
        } else {
            $this->_findMonth = Subscriptions::find()->where(['user_id' => $this->_user->id, 'type' => Subscriptions::TYPE_FIND_MONTHLY])->one();
        }

        if($this->_findMonth == null)
        {
            $this->_findMonth = new Subscriptions(['user_id' => $this->_user->id, 'type' => Subscriptions::TYPE_FIND_MONTHLY]);
            $this->_findMonth->save(false);
        }

        return $this->_findMonth;
    }

    /**
     * @return Subscriptions
     */
    public function getFixMonth()
    {
        if($this->_fixMonth != null){
            return $this->_fixMonth;
        } else {
            $this->_fixMonth = Subscriptions::find()->where(['user_id' => $this->_user->id, 'type' => Subscriptions::TYPE_FIX_MONTHLY])->one();
        }

        if($this->_fixMonth == null)
        {
            $this->_fixMonth = new Subscriptions(['user_id' => $this->_user->id, 'type' => Subscriptions::TYPE_FIX_MONTHLY]);
            $this->_fixMonth->save(false);
        }

        return $this->_fixMonth;
    }

    /**
     * Возвращает все подписки
     * @return array
     */
    public function getSubscriptionsList()
    {
        return [
            Subscriptions::TYPE_FIND_DAILY => $this->findDaily,
            Subscriptions::TYPE_FIND_MONTHLY => $this->findMonth,
            Subscriptions::TYPE_FIX_DAILY => $this->fixDaily,
            Subscriptions::TYPE_FIX_MONTHLY => $this->fixMonth,
        ];
    }
}