<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "fix_letters_images_attachments".
 *
 * @property int $id
 * @property int $find_letter_image_id Буква
 * @property string $path Путь
 * @property string $type Тип
 *
 * @property FindLettersImages $findLetterImage
 */
class FindLettersImagesAttachments extends \yii\db\ActiveRecord
{
    const TYPE_WROTE_DOWN = 0;
    const TYPE_WROTE_UP = 1;
    const TYPE_PRINTED_DOWN = 2;
    const TYPE_PRINTED_UP = 3;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'find_letters_images_attachments';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['find_letter_image_id'], 'integer'],
            [['path', 'type'], 'string', 'max' => 255],
            [['find_letter_image_id'], 'exist', 'skipOnError' => true, 'targetClass' => FixLettersImages::className(), 'targetAttribute' => ['find_letter_image_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'find_letter_image_id' => 'Fix Letter Image ID',
            'path' => 'Path',
            'type' => 'Type',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFixLetterImage()
    {
        return $this->hasOne(FindLettersImage::className(), ['id' => 'find_letter_image_id']);
    }
}
