<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\FreeEmailRecipients;

/**
 * FreeEmailRecipientsSearch represents the model behind the search form about `app\models\FreeEmailRecipients`.
 */
class FreeEmailRecipientsSearch extends FreeEmailRecipients
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'email_id', 'status'], 'integer'],
            [['recipient_email', 'content', 'exception_text', 'send_datetime'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = FreeEmailRecipients::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'email_id' => $this->email_id,
            'status' => $this->status,
            'send_datetime' => $this->send_datetime,
        ]);

        $query->andFilterWhere(['like', 'recipient_email', $this->recipient_email])
            ->andFilterWhere(['like', 'content', $this->content])
            ->andFilterWhere(['like', 'exception_text', $this->exception_text]);

        return $dataProvider;
    }
}
