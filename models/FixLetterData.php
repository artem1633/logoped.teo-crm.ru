<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "fix_letter_data".
 *
 * @property int $id
 * @property int $fix_letter_id Пособие
 * @property string $symbol Буква
 * @property string $type Стиль написания
 *
 * @property FixLetter $fixLetter
 */
class FixLetterData extends \yii\db\ActiveRecord
{
    const TYPE_PRINT_UP = 0;
    const TYPE_PRINT_LOW = 1;
    const TYPE_RECIPE_UP = 2;
    const TYPE_RECIPE_LOW = 3;
    
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'fix_letter_data';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['fix_letter_id'], 'integer'],
            [['symbol'], 'string', 'max' => 10],
            [['type'], 'string', 'max' => 255],
            [['fix_letter_id'], 'exist', 'skipOnError' => true, 'targetClass' => FixLetter::class, 'targetAttribute' => ['fix_letter_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'fix_letter_id' => 'Fix Letter ID',
            'symbol' => 'Symbol',
            'type' => 'Type',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFixLetter()
    {
        return $this->hasOne(FixLetter::class, ['id' => 'fix_letter_id']);
    }
}
