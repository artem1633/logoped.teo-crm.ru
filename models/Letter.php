<?php

namespace app\models;

use app\helpers\DatesCalculation;
use app\models\users\Users;
use app\queries\LetterQuery;
use app\source\pdfGenerators\FindPdfGenerator;
use app\source\pdfGenerators\FixPdfGenerator;
use app\source\pdfGenerators\PdfGenerator;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * Class Letter
 * @package app\models
 *
 * @property string $using_end_datetime Дата и время окончания возможности использования пособия
 * @property string $pdf_file
 * @property integer $page_format
 *
 * @property boolean $isUsingExpired
 */
class Letter extends ActiveRecord
{
    const LETTERS_EASY = 0;
    const LETTERS_MEDIUM = 1;
    const LETTERS_COMPLEX = 2;

    const FORMAT_PAGE_A4 = 0;
    const FORMAT_PAGE_A3 = 1;

    /**
     * @var PdfGenerator
     */
    public $pdfGenerator;

    /**
     * @inheritdoc
     */
    public function init()
    {
        if($this instanceof FindLetter)
        {
            $this->pdfGenerator = new FindPdfGenerator($this);
        } else if ($this instanceof FixLetter){
            $this->pdfGenerator = new FixPdfGenerator($this);
        }

        parent::init();
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::class,
                'attributes' => [
                    self::EVENT_BEFORE_INSERT => ['using_end_datetime'],
                ],
                'value' => function(){
                    /** @var Users $user */
                    $user = Yii::$app->user->identity;

                    if($this instanceof FindLetter){
                        if($user->subscription->findMonth->typeInstance->getIsSubscribed() && $user->subscription->findMonth->typeInstance->getIsPayed()){
                            return DatesCalculation::getNextRealMonth($user->subscription->findMonth->payed_datetime);
                        }
                    } else if ($this instanceof FixLetter) {
                        if ($user->subscription->fixMonth->typeInstance->getIsSubscribed() && $user->subscription->fixMonth->typeInstance->getIsPayed()) {
                            return DatesCalculation::getNextRealMonth($user->subscription->fixMonth->payed_datetime);
                        }
                    }

                    return date('Y-m-d H:i:s', time() + 86400);
                },
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public static function find()
    {
        return new LetterQuery(get_called_class());
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['page_count'], 'required'],
            [['page_format'], 'integer'],
            [['pdf_file'], 'string'],
            [['using_end_datetime'], 'safe'],
        ];
    }

    /**
     * @return array
     */
    public static function getPageFormat()
    {
        return [
            self::FORMAT_PAGE_A4 => 'A4',
            self::FORMAT_PAGE_A3 => 'A3',
        ];
    }

    /**
     * @return array
     */
    public static function getComplexityies()
    {
        return [
            self::LETTERS_EASY => 'Легкий',
            self::LETTERS_MEDIUM => 'Средний',
            self::LETTERS_COMPLEX => 'Сложный',
        ];
    }

    /**
     * @return bool
     */
    public function getIsUsingExpired()
    {
        $usingEndDatetime = strtotime($this->using_end_datetime);
        $now = time();

        return $now > $usingEndDatetime;
    }

    /**
     *
     */
    public function saveAsFile()
    {
        $text = "test";

        $fp = fopen("pdf/file.pdf", "w");

        fwrite($fp, $text);

        fclose($fp);
    }
}