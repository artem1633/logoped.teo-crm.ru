<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "payment_orders".
 *
 * @property int $id
 * @property int $company_id ID компании
 * @property string $date_order Дата и время заказа выплаты
 * @property string $date_payment Дата и время выплаты
 * @property string $amount Сумма выплаты
 * @property int $status Статус выплаты
 * @property int $type Тип выплаты
 * @property string $requisites Реквизиты
 * @property string $description Коментарии
 */
class PaymentOrders extends \yii\db\ActiveRecord
{
    const SCENARIO_SEND_ORDER = 'send';

    const STATUS_SUCCESS = 1;

    const TYPE_CARD = 1;
    const TYPE_YANDEX = 2;
    const TYPE_BALLANCE = 3;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'payment_orders';
    }


    public function sufficientAmount ($attribute) {
        $company = Companies::findOne($this->company_id);
        if($this->amount > $company->affiliate_amount){
            $this->addError ($attribute, 'Вы можете заказать ' . $company->affiliate_amount . ' рублей');
        }
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['date_order', 'date_payment'], 'safe'],
            [['amount'], 'sufficientAmount', 'on' => self::SCENARIO_SEND_ORDER],
            [['amount'], 'number'],
            [['status', 'type', 'company_id'], 'integer'],
            [['type'], 'required'],
            [['requisites', 'description'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'company_id' => 'Компания',
            'date_order' => 'Дата заказа',
            'date_payment' => 'Дата выплаты',
            'amount' => 'Сумма',
            'status' => 'Статус',
            'type' => 'Тип',
            'requisites' => 'Реквизиты',
            'description' => 'Коментарии',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompanyModel()
    {
        return $this->hasOne(Companies::className(), ['id' => 'company_id']);
    }

}
