<?php

namespace app\models;

use app\services\events\SmsError;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "sms".
 *
 * @property int $id
 * @property int $user_id Пользователь, которому отправлена SMS
 * @property string $phone_number Номер телефона
 * @property string $sms_text Sms-текст
 * @property int $status Статус
 * @property int $status_code Код статуса
 * @property double $cost Стоимость
 * @property string $created_at
 *
 * @property string $statusCodeText
 *
 * @property \app\models\users\Users $user
 */
class Sms extends \yii\db\ActiveRecord
{
    const STATUS_ERROR = 0;
    const STATUS_SUCCESS = 1;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'sms';
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::class,
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => null,
                'value' => date('Y-m-d H:i:s'),
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'status', 'status_code'], 'integer'],
            [['sms_text'], 'string'],
            [['cost'], 'number'],
            [['created_at'], 'safe'],
            [['phone_number'], 'string', 'max' => 255],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => \app\models\users\Users::class, 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'Пользователь, которому отправлена SMS',
            'phone_number' => 'Номер телефона',
            'sms_text' => 'Sms текст',
            'status' => 'Статус',
            'status_code' => 'Код статуса',
            'cost' => 'Стоимость',
            'created_at' => 'Created At',
        ];
    }

    /**
     * @return string
     */
    public function getStatusCodeText()
    {
        if($this->status == self::STATUS_SUCCESS)
        {
            return 'Успех';
        } else {
            $errors = SmsError::getErrorsLabels();
            if(in_array($this->status_code, array_keys($errors)) == false){
                return "Неивестная ошибка: {$this->status_code}";
            }

            return $errors[$this->status_code];
        }
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(\app\models\users\Users::class, ['id' => 'user_id']);
    }
}
