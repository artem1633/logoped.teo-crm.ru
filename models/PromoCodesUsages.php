<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "promo_codes_usages".
 *
 * @property int $id
 * @property int $user_id Пользователь
 * @property int $promo_code_id Промокод
 * @property string $promo_code_name Наименование промокода
 * @property int $count Кол-во использований
 * @property string $last_usage_datetime Дата и время последнего использования
 * @property string $created_at
 *
 * @property PromoCode $promoCode
 * @property \app\models\users\Users $user
 */
class PromoCodesUsages extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'promo_codes_usages';
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::class,
                'attributes' => [
                    self::EVENT_BEFORE_INSERT => ['created_at', 'last_usage_datetime'],
                ],
                'value' => date('Y-m-d H:i:s'),
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'promo_code_id', 'count'], 'integer'],
            [['last_usage_datetime', 'created_at'], 'safe'],
            [['promo_code_name'], 'string', 'max' => 255],
            [['promo_code_id'], 'exist', 'skipOnError' => true, 'targetClass' => PromoCode::class, 'targetAttribute' => ['promo_code_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => \app\models\users\Users::class, 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'promo_code_id' => 'Promo Code ID',
            'promo_code_name' => 'Promo Code Name',
            'count' => 'Count',
            'last_usage_datetime' => 'Last Usage Datetime',
            'created_at' => 'Created At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPromoCode()
    {
        return $this->hasOne(PromoCode::class, ['id' => 'promo_code_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(\app\models\users\Users::class, ['id' => 'user_id']);
    }
}
