<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "fix_letters_images_attachments".
 *
 * @property int $id
 * @property int $fix_letter_image_id Буква
 * @property string $path Путь
 * @property string $type Тип
 * @property int $drawing Начертание
 *
 * @property FixLettersImages $fixLetterImage
 */
class FixLettersImagesAttachments extends \yii\db\ActiveRecord
{
    const TYPE_EASY = 0;
    const TYPE_MIDDLE = 1;
    const TYPE_HARD = 2;

    const DRAWING_WROTE = 0;
    const DRAWING_PRINTED = 1;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'fix_letters_images_attachments';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['fix_letter_image_id', 'drawing'], 'integer'],
            [['path', 'type'], 'string', 'max' => 255],
            [['fix_letter_image_id'], 'exist', 'skipOnError' => true, 'targetClass' => FixLettersImages::className(), 'targetAttribute' => ['fix_letter_image_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'fix_letter_image_id' => 'Fix Letter Image ID',
            'path' => 'Путь',
            'type' => 'Тип',
            'drawing' => 'Начертание',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFixLetterImage()
    {
        return $this->hasOne(FixLettersImages::className(), ['id' => 'fix_letter_image_id']);
    }
}
