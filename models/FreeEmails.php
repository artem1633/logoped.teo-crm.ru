<?php

namespace app\models;

use app\models\users\Users;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\helpers\ArrayHelper;
use yii\validators\EmailValidator;

/**
 * This is the model class for table "free_emails".
 *
 * @property int $id
 * @property string $recipient Получатели
 * @property string $subject Тема письма
 * @property integer $state Состояние рассылки
 * @property string $content Содержание
 * @property string $attachment Вложения
 * @property string $send_datetime Дата и время отправки
 * @property string $created_at Дата и время создания
 *
 * @property FreeEmailsAttachments[] $freeEmailsAttachments
 */
class FreeEmails extends \yii\db\ActiveRecord
{
    const EVENT_SEND = 'send';

    const STATE_WAIT = 0;
    const STATE_STARTED = 1;
    const STATE_STOPPED = 2;
    const STATE_FINISHED = 3;

    public $attachment;

    /**
     * @var array
     */
    public $recipient;

    /**
     * @var array
     */
    private $_recipientEmails;

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::class,
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => null,
                'value' => date('Y-m-d H:i:s'),
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'free_emails';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['subject', 'content'], 'required'],
            [['content'], 'string'],
            [['subject'], 'string', 'max' => 255],
            [['state'], 'integer'],
            [['attachment', 'recipient', 'send_datetime', 'created_at'], 'safe'],
            ['recipient', function($attribute){
                $recipients = array_values($this->recipient);
                $this->_recipientEmails = ArrayHelper::getColumn(Users::findAll($recipients), 'login');
                foreach ($this->_recipientEmails as $recip){
                    $validator = new EmailValidator();
                    if($validator->validate($recip) == false){
                        $this->addError('recipient', 'Присутствует некорректный email');
                    }
                }
            }],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function beforeSave($insert)
    {
//        if($this->recipient != null){
//            $this->recipient = implode(',', $this->recipient);
//        }

        return parent::beforeSave($insert);
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'recipient' => 'Получатели',
            'state' => 'Состояние',
            'subject' => 'Тема письма',
            'content' => 'Содержание',
            'attachment' => 'Приложение',
        ];
    }

    /**
     * Запускает рассылку
     */
    public function start()
    {
//        echo '123';
        if($this->validate())
        {
//            echo '123';
            $this->state = self::STATE_STARTED;
//            var_dump($this->_recipientEmails);
//            exit;
            $this->save(false);
            return true;
/*            try {
                $this->recipient = explode(',', $this->recipient);

                $fileNames = ArrayHelper::getColumn($this->freeEmailsAttachments, 'path');
                for ($i = 0; $i < count($fileNames); $i++){
                    $fileNames[$i] = '/'.$fileNames[$i];
                }

                $mail = Yii::$app->mailer->compose()
                    ->setFrom('logoped.service@yandex.ru')
                    ->setTo($this->recipient)
                    ->setSubject($this->subject)
                    ->setHtmlBody($this->content);

                if(count($fileNames) > 0){
                    $mail->attach($fileNames);
                }

                $mail->send();

                return true;

            } catch (\Exception $e){
                var_dump($e);
                exit;
            }*/
        }
    }

    /**
     * {@inheritdoc}
     */
    public function afterSave($insert, $changedAttributes)
    {
        foreach ($this->_recipientEmails as $recipient)
        {
            if($insert){
                $recipientAR = new FreeEmailRecipients([
                    'email_id' => $this->id,
                    'recipient_email' => $recipient,
                    'content' => $this->content,
                    'status' => FreeEmailRecipients::STATUS_WAIT,
                ]);
                $recipientAR->save(false);
            }
        }

        parent::afterSave($insert, $changedAttributes);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFreeEmailsAttachments()
    {
        return $this->hasMany(FreeEmailsAttachments::class, ['email_id' => 'id']);
    }
}
