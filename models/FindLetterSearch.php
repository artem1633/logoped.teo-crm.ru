<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\FindLetter;

/**
 * FindLetterSearch represents the model behind the search form about `app\models\FindLetter`.
 */
class FindLetterSearch extends FindLetter
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'user_id', 'page_format', 'page_count', 'letters_count', 'complexity', 'save'], 'integer'],
            [['letters_composition'], 'safe'],
            [['amount'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = FindLetter::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_DESC,
                ],
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'user_id' => $this->user_id,
            'page_format' => $this->page_format,
            'page_count' => $this->page_count,
            'letters_count' => $this->letters_count,
            'complexity' => $this->complexity,
            'amount' => $this->amount,
            'save' => $this->save,
        ]);

        $query->andFilterWhere(['like', 'letters_composition', $this->letters_composition]);

        if(Yii::$app->user->identity->isSuperAdmin() == false){
            $query->andWhere(['user_id' => Yii::$app->user->getId()]);
        }

        return $dataProvider;
    }
}
