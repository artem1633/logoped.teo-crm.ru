<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "users_email_notifications".
 *
 * @property int $id
 * @property int $user_id Пользователь
 * @property int $long_time_no_enter Долгое время не заходил в систему
 *
 * @property Users $user
 */
class UsersEmailNotifications extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'users_email_notifications';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'long_time_no_enter'], 'integer'],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => Users::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'long_time_no_enter' => 'Long Time No Enter',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(Users::className(), ['id' => 'user_id']);
    }
}
