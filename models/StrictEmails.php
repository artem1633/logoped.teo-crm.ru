<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "strict_emails".
 *
 * @property int $id
 * @property string $key Ключ
 * @property string $subject Тема письма
 * @property string $content Содержание
 *
 * @property string $keyLabel
 */
class StrictEmails extends \yii\db\ActiveRecord
{
    const KEYS_LABELS = [
        'register' => 'Успешная регистрация',
        'email_confirm' => 'Подтверждение E-mail',
        'forget_password' => 'Востановление пароля',
        'letter_created_not_payed' => 'Заказ создан, но не оплачен',
        'long_time_not_online' => 'Не входил в сервис определенное кол-во дней',
        'letter' => 'Получение заказа',
        'up_balance_for_storage' => 'Необходимо пополнить баланс, чтобы хватило денег на хранение ',
    ];

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'strict_emails';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['content'], 'string'],
            [['key', 'subject'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'key' => 'Key',
            'subject' => 'Тема',
            'content' => 'Контент',
        ];
    }

    /**
     * @param string $key
     * @return null|static
     */
    public static function findByKey($key)
    {
        return self::findOne(['key' => $key]);
    }

    /**
     * @return string
     */
    public function getKeyLabel()
    {
       if(isset(self::KEYS_LABELS[$this->key])){
           return self::KEYS_LABELS[$this->key];
       } else {
           return null;
       }
    }
}
