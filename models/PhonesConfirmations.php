<?php

namespace app\models;

use app\models\users\Users;
use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "phones_confirmations".
 *
 * @property int $id
 * @property int $user_id Пользователь
 * @property string $phone Телефон на который отправлен код подтверждения
 * @property int $code
 * @property int $status Статус (Неподтвержден/Подтвержден)
 * @property string $created_at
 *
 * @property boolean $isActive
 *
 * @property \app\models\users\Users $user
 */
class PhonesConfirmations extends \yii\db\ActiveRecord
{
    const EVENT_CODE_CONFIRMED = 'code_confirmed';
    const ACTIVE_TIME = 86400; // Сутки
    const STATUS_CONFIRMED = 1;
    const STATUS_UNCONFIRMED = 0;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'phones_confirmations';
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::class,
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => null,
                'value' => date('Y-m-d H:i:s'),
            ],
        ];
    }

    /**
     * Генерирует код подтверждения для пользователя
     * @param int $user_id
     * @return self
     */
    public static function generate($user_id)
    {
        $user = Users::findOne($user_id);
        if($user == null)
            return null;

        $code = strval(rand(0, 99999));

        if(strlen($code) < 5){
            $count = 5 - strlen($code);
            for($i = 0; $i < $count; $i++)
            {
                $code = '0'.$code;
            }
        }

        $model = new self([
            'user_id' => $user_id,
            'phone' => $user->telephone,
            'code' => $code,
            'status' => 0,
        ]);
        $model->save(false);

        return $model;
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['phone'], 'string', 'max' => 255],
            [['user_id', 'code', 'status'], 'integer'],
            [['created_at'], 'safe'],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => \app\models\users\Users::class, 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'Пользователь',
            'phone' => 'Телефон',
            'code' => 'Code',
            'status' => 'Статус (Неподтвержден/Подтвержден)',
            'created_at' => 'Created At',
        ];
    }

    /**
     * Возвращает активно ли подтверждение или нет
     * @return bool
     */
    public function getIsActive()
    {
        $now = time();
        $date = strtotime($this->created_at);

        return $now <= ($date + self::ACTIVE_TIME);
    }

    /**
     * Подтвердить код
     */
    public function confirm()
    {
        $this->status = 1;
        $this->save(false);

        $user = $this->user;
        $user->telephone_confirmed = 1;
        $user->save(false);

        $upBonus = floatval(\app\models\Settings::findByKey('up_balance_after_phone_confirm_amount')->value);

        if(is_int($upBonus) || is_float($upBonus) || is_double($upBonus)){
            $user->wallet->writeUpBonus($upBonus, Balances::CATEGORY_BONUS, 'Бонус за подтверждение телефона');
        }

        $this->trigger(self::EVENT_CODE_CONFIRMED);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(\app\models\users\Users::class, ['id' => 'user_id']);
    }
}
