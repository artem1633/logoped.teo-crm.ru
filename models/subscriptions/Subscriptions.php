<?php

namespace app\models\subscriptions;

use app\models\subscriptions\types\FindDailyType;
use app\models\subscriptions\types\FindMonthType;
use app\models\subscriptions\types\FixDailyType;
use app\models\subscriptions\types\FixMonthType;
use app\queries\SubscriptionQuery;
use app\models\users\Users;

/**
 * This is the model class for table "subscriptions".
 *
 * @property int $id
 * @property int $user_id Пользователь
 * @property int $type Тип подписки
 * @property string $payed_datetime Дата и время последней оплаты
 *
 * @property SubscriptionType $typeInstance Стратегия типа подписки
 *
 * @property Users $user
 */
class Subscriptions extends \yii\db\ActiveRecord
{
    /**
     * @const int Тип подписки «Найди букву | Суточное хранение»
     */
    const TYPE_FIND_DAILY = 100;

    /**
     * @const int Тип подписки «Найди букву | Месячное хранение»
     */
    const TYPE_FIND_MONTHLY = 150;

    /**
     * @const int Тип подписки «Почини букву | Суточное хранение»
     */
    const TYPE_FIX_DAILY = 200;

    /**
     * @const int Тип подписки «Почини букву | Месячное хранение»
     */
    const TYPE_FIX_MONTHLY = 250;

    /**
     * @var SubscriptionType Стратегия типа подписки
     */
    private $_typeInstance;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'subscriptions';
    }

    /**
     * {@inheritdoc}
     */
    public static function find()
    {
        return new SubscriptionQuery(get_called_class());
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'type'], 'integer'],
            [['payed_datetime'], 'safe'],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => Users::class, 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'Пользователь',
            'type' => 'Тип',
            'payed_datetime' => 'Дата и время последней оплаты',
        ];
    }

    /**
     * @return array
     */
    public function typeLabels()
    {
        return [
            self::TYPE_FIND_DAILY => 'Найди букву | Суточное хранение',
            self::TYPE_FIND_MONTHLY => 'Найди букву | Месячное хранение',
            self::TYPE_FIX_DAILY => 'Почини букву | Месячное хранение',
            self::TYPE_FIX_MONTHLY => 'Почини букву | Месячное хранение',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(Users::class, ['id' => 'user_id']);
    }

    /**
     * @return SubscriptionType|null
     */
    public function getTypeInstance()
    {
        if($this->_typeInstance != null)
        {
            return $this->_typeInstance;
        }

        switch ($this->type)
        {
            case self::TYPE_FIND_DAILY:
                $this->_typeInstance = new FindDailyType($this);
                break;
            case self::TYPE_FIND_MONTHLY:
                $this->_typeInstance = new FindMonthType($this);
                break;
            case self::TYPE_FIX_DAILY:
                $this->_typeInstance = new FixDailyType($this);
                break;
            case self::TYPE_FIX_MONTHLY:
                $this->_typeInstance = new FixMonthType($this);
                break;
            default:
                $this->_typeInstance = null;
                break;
        }

        return $this->_typeInstance;
    }
}
