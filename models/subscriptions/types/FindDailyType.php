<?php

namespace app\models\subscriptions\types;

use app\models\Balances;
use app\models\FindLetter;
use app\models\Settings;
use app\models\subscriptions\events\SubscriptionTypeEvent;
use app\models\subscriptions\SubscriptionType;

/**
 * Class FindDailyType
 * @package app\models\subscriptions\types
 *
 * @property bool $isSubscribed
 */
class FindDailyType extends SubscriptionType
{
    /**
     * Оплатить подписку
     */
    public function pay()
    {
        // Проверка на месячную подписку аккаунта
        if($this->_activeRecord->user->subscription->findMonth->typeInstance->getIsSubscribed()){
            return;
        }

        $key = 'cost_storage_find_letter';
        $category = Balances::CATEGORY_STORAGE_FIND;
        $this->price = Settings::findByKey($key)->value;

        if($this->_activeRecord->user->wallet->hasAmount($this->price))
        {
            if($this->_activeRecord->user->wallet->writeOff($this->price, $category)){
                $this->_activeRecord->payed_datetime = date('Y-m-d H:i:s');
                $this->_activeRecord->save(false);
                FindLetter::updateAll(['using_end_datetime' => date('Y-m-d H:i:s', time() + 86400)], ['user_id' => $this->_activeRecord->user_id]);
                $this->trigger(self::EVENT_PAYED, new SubscriptionTypeEvent(['price' => $this->price]));
            }
        } else {
            $this->noMoney();
        }
    }

    /**
     * Подписан ли пользователь
     * @return mixed
     */
    public function getIsSubscribed()
    {
        return $this->_activeRecord->user->settings->find_letter_data_keeping == 1 ? true : false;
    }

    /**
     * Оплачена ли подписка
     * @return mixed
     */
    public function getIsPayed()
    {
        if($this->_activeRecord->payed_datetime == null)
            return false;

        $lastPay = strtotime($this->_activeRecord->payed_datetime);
        $now = time();

        return ($now - $lastPay) < 86400;
    }

    /**
     * Подписывает на подписку данного типа
     * @return mixed
     */
    public function subscribe()
    {
        $settings = $this->_activeRecord->user->settings;
        return $settings->switchOn('find_letter_data_keeping');
    }

    /**
     * Отписывает с подписку данного типа
     * @return mixed
     */
    public function unSubscribe()
    {
        $settings = $this->_activeRecord->user->settings;
        return $settings->switchOff('find_letter_data_keeping');
    }
}