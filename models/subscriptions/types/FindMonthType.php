<?php
/**
 * Created by PhpStorm.
 * User: Ilusha
 * Date: 22.02.2019
 * Time: 1:44
 */

namespace app\models\subscriptions\types;

use app\helpers\DatesCalculation;
use app\models\Balances;
use app\models\FindLetter;
use app\models\Settings;
use app\models\subscriptions\events\SubscriptionTypeEvent;
use app\models\subscriptions\Subscriptions;
use app\models\subscriptions\SubscriptionType;

/**
 * Class FindMonthType
 * @package app\models\subscriptions\types
 *
 * @property bool $isSubscribed
 */
class FindMonthType extends SubscriptionType
{
    /**
     * Оплатить подписку
     */
    public function pay()
    {
        $key = 'cost_subscription_find_letter';
        $category = Balances::CATEGORY_SUBSCRIPTION_FIND;
        $this->price = Settings::findByKey($key)->value;

        if($this->_activeRecord->user->wallet->hasAmount($this->price))
        {
            if($this->_activeRecord->user->wallet->writeOff($this->price, $category)){
                $this->_activeRecord->payed_datetime = date('Y-m-d H:i:s');
                $this->_activeRecord->save(false);
                FindLetter::updateAll(['using_end_datetime' => DatesCalculation::getNextRealMonth(date('Y-m-d H:i:s'))], ['user_id' => $this->_activeRecord->user_id]);
                $this->trigger(self::EVENT_PAYED, new SubscriptionTypeEvent(['price' => $this->price]));
            }
        } else {
            $this->noMoney();
        }
    }

    /**
     * Подписан ли пользователь
     * @return mixed
     */
    public function getIsSubscribed()
    {
        return $this->_activeRecord->user->settings->find_letter_data_subscribe == 1 ? true : false;
    }

    /**
     * Оплачена ли подписка
     * @return mixed
     */
    public function getIsPayed()
    {
        if($this->_activeRecord->payed_datetime == null)
            return false;

        $currentPaymentExpiredTimestamp = strtotime(DatesCalculation::getNextRealMonth($this->_activeRecord->payed_datetime));
        $now = time();

        return $now < $currentPaymentExpiredTimestamp;
    }

    /**
     * Подписывает на подписку данного типа
     * @return mixed
     */
    public function subscribe()
    {
        $settings = $this->_activeRecord->user->settings;
        return $settings->switchOn('find_letter_data_subscribe');
    }

    /**
     * Отписывает с подписку данного типа
     * @return mixed
     */
    public function unSubscribe()
    {
        $settings = $this->_activeRecord->user->settings;
        return $settings->switchOff('find_letter_data_subscribe');
    }
}