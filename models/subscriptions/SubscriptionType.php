<?php

namespace app\models\subscriptions;

use app\behaviors\MailsBehavior;
use app\models\StrictEmails;
use app\models\subscriptions\events\SubscriptionTypeEvent;
use app\models\subscriptions\exceptions\NoMoneyException;
use app\models\subscriptions\types\FindDailyType;
use app\models\subscriptions\types\FindMonthType;
use app\models\subscriptions\types\FixDailyType;
use app\models\subscriptions\types\FixMonthType;
use yii\base\Component;

/**
 * Class SubscriptionType
 * @package app\models\subscriptions
 *
 * @property bool $isSubscribed
 */
abstract class SubscriptionType extends Component
{
    const EVENT_PAYED = 'payed';
    const EVENT_NO_MONEY = 'no_money';

    /**
     * @var double
     */
    protected $price;

    /**
     * @var Subscriptions
     */
    protected $_activeRecord;

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => MailsBehavior::class,
                'mails' => [
                    self::EVENT_NO_MONEY => [
                        'to' => function(){
                            return $this->_activeRecord->user->login;
                        },
                        'subject' => 'Недостаточно средств',
                        'htmlBody' => StrictEmails::findByKey('up_balance_for_storage')->content,
                        'hasTags' => true,
                        'customTags' => function($event){
                            /** @var SubscriptionTypeEvent $event */
                            $sender = $event->sender;
                            $subscriptionName = '';
                            $price = $event->price;

                            switch (true){
                                case $sender instanceof FindDailyType:
                                    $subscriptionName = 'Пособие «Найди букву» | Ежедневная подписка';
                                    break;
                                case $sender instanceof FindMonthType:
                                    $subscriptionName = 'Пособие «Найди букву» | Месячная подписка';
                                    break;
                                case $sender instanceof FixDailyType:
                                    $subscriptionName = 'Пособие «Почини букву» | Ежедневная подписка';
                                    break;
                                case $sender instanceof FixMonthType:
                                    $subscriptionName = 'Пособие «Почини букву» | Месячная подписка';
                                    break;
                            }

                            return [
                                '{subscription}' => $subscriptionName,
                                '{price}' => $price,
                            ];
                        },
                        'condition' => function($event){
                            /** @var SubscriptionType $sender */
                            $sender = $event->sender;
                            return $sender->_activeRecord->user->settings->isEnabled('balance_up_reminder');
                        }
                    ],
                ],
            ],
        ];
    }

    /**
     * SubscriptionType constructor.
     * @param Subscriptions $activeRecord
     * @param array $config
     */
    public function __construct($activeRecord, array $config = [])
    {
        $this->_activeRecord = $activeRecord;
        parent::__construct($config);
    }

    /**
     * Оплатить подписку
     * @return mixed
     */
    abstract public function pay();

    /**
     * Подписан ли пользователь
     * @return mixed
     */
    abstract public function getIsSubscribed();

    /**
     * Оплачена ли подписка
     * @return mixed
     */
    abstract public function getIsPayed();

    /**
     * Подписывает на подписку данного типа
     * @return mixed
     */
    abstract public function subscribe();

    /**
     * Отписывает с подписку данного типа
     * @return mixed
     */
    abstract public function unSubscribe();

    /**
     * Вызывается в том случае если недостаточно денег
     * для продления подписки
     */
    protected function noMoney()
    {
        $this->unSubscribe();
        $this->trigger(self::EVENT_NO_MONEY, new SubscriptionTypeEvent(['price' => $this->price]));
        throw new NoMoneyException();
    }
}