<?php

namespace app\models\subscriptions\events;

use yii\base\Event;

/**
 * Class SubscriptionTypeEvent
 * @package app\models\subscriptions\events
 */
class SubscriptionTypeEvent extends Event
{
    public $price;
}