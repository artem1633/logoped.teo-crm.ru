<?php

namespace app\models\subscriptions\exceptions;

use yii\web\HttpException;

/**
 * Class NoMoneyException
 * @package app\models\subscriptions\exceptions
 */
class NoMoneyException extends HttpException
{
    public function __construct($status = 402, $message = 'Недостаточно средств', $code = 0, \Exception $previous = null)
    {
        parent::__construct($status, $message, $code, $previous);
    }
}