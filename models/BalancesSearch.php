<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Balances;
use yii\db\ActiveQuery;

/**
 * BalancesSearch represents the model behind the search form about `app\models\Balances`.
 */
class BalancesSearch extends Balances
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'user_id', 'promo_code_id', 'dk'], 'integer'],
            [['amount', 'amount_bonus'], 'number'],
            [['pay_date', 'comment'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Balances::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'user_id' => $this->user_id,
            'amount' => $this->amount,
            'pay_date' => $this->pay_date,
            'promo_code_id' => $this->promo_code_id,
            'amount_bonus' => $this->amount_bonus,
        ]);

        $query->andFilterWhere(['like', 'comment', $this->comment]);

        $this->addPermissionFilter($query);

        return $dataProvider;
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function searchDebit($params)
    {
        $query = Balances::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'user_id' => $this->user_id,
            'amount' => $this->amount,
            'pay_date' => $this->pay_date,
            'promo_code_id' => $this->promo_code_id,
            'amount_bonus' => $this->amount_bonus,
            'dk' => 1,
        ]);

        $query->andFilterWhere(['like', 'comment', $this->comment]);

        $this->addPermissionFilter($query);

        return $dataProvider;
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function searchCredit($params)
    {
        $query = Balances::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'user_id' => $this->user_id,
            'amount' => $this->amount,
            'pay_date' => $this->pay_date,
            'promo_code_id' => $this->promo_code_id,
            'amount_bonus' => $this->amount_bonus,
            'dk' => 0,
        ]);

        $query->andFilterWhere(['like', 'comment', $this->comment]);

        $this->addPermissionFilter($query);

        return $dataProvider;
    }

    /**
     * @param ActiveQuery $query
     */
    private function addPermissionFilter(&$query)
    {
        if(Yii::$app->user->identity->isSuperAdmin() == false) {
            $query->andWhere(['user_id' => Yii::$app->user->getId()]);
        }
    }
}
