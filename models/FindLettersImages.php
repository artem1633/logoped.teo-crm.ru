<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "find_letters_images".
 *
 * @property int $id
 * @property string $symbol Символ
 *
 * @property int $commonAttachmentsCount
 * @property int $easyAttachmentsCount
 * @property int $middleAttachmentsCount
 * @property int $hardAttachmentsCount
 * @property string $type
 *
 * @property FindLettersImagesAttachments[] $findLettersImagesAttachments
 */
class FindLettersImages extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'find_letters_images';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['symbol'], 'string', 'max' => 10],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'symbol' => 'Символ',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFindLettersImagesAttachments()
    {
        return $this->hasMany(FindLettersImagesAttachments::className(), ['fix_letter_image_id' => 'id']);
    }

    public function getCommonAttachmentsCount()
    {
        return FindLettersImagesAttachments::find()->where(['find_letter_image_id' => $this->id])->count();
    }

    public function getWroteDownAttachmentsCount()
    {
        return FindLettersImagesAttachments::find()->where(['find_letter_image_id' => $this->id, 'type' => FindLettersImagesAttachments::TYPE_WROTE_DOWN])->count();
    }

    public function getWroteUpAttachmentsCount()
    {
        return FindLettersImagesAttachments::find()->where(['find_letter_image_id' => $this->id, 'type' => FindLettersImagesAttachments::TYPE_WROTE_UP])->count();
    }

    public function getPrintedDownAttachmentsCount()
    {
        return FindLettersImagesAttachments::find()->where(['find_letter_image_id' => $this->id, 'type' => FindLettersImagesAttachments::TYPE_PRINTED_DOWN])->count();
    }

    public function getPrintedUpAttachmentsCount()
    {
        return FindLettersImagesAttachments::find()->where(['find_letter_image_id' => $this->id, 'type' => FindLettersImagesAttachments::TYPE_PRINTED_UP])->count();
    }




//    public function getMiddleAttachmentsCount()
//    {
//        return FindLettersImagesAttachments::find()->where(['fix_letter_image_id' => $this->id, 'type' => FindLettersImagesAttachments::TYPE_MIDDLE])->count();
//    }
//
//    public function getHardAttachmentsCount()
//    {
//        return FindLettersImagesAttachments::find()->where(['fix_letter_image_id' => $this->id, 'type' => FindLettersImagesAttachments::TYPE_HARD])->count();
//    }
}
