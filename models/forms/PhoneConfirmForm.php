<?php

namespace app\models\forms;

use app\models\PhonesConfirmations;
use app\models\users\Users;
use yii\base\InvalidConfigException;
use yii\base\Model;

/**
 * Class PhoneConfirmForm
 * @package app\models\forms
 */
class PhoneConfirmForm extends Model
{
    /**
     * @var string
     */
    public $code;

    /**
     * @var Users
     */
    public $user;

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        if($this->user == null)
            throw new InvalidConfigException('$user must be required');

        parent::init();
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['code'], 'required'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'code' => 'Код подтверждения'
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function confirm()
    {
        if($this->validate())
        {
            /** @var null|PhonesConfirmations $confirm */
            $confirm = PhonesConfirmations::find()->where(['user_id' => $this->user->id, 'code' => $this->code])->one();

            if($confirm == null){
                $this->addError('code', 'Некорректный код');
                return false;
            }

            if($confirm->isActive == false){
                $this->addError('code', 'Истек срок действия кода');
                return false;
            }

            $confirm->confirm();
            return true;
        }

        return false;
    }
}