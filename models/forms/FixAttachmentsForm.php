<?php
namespace app\models\forms;

use app\models\FixLettersImagesAttachments;
use yii\base\Model;

/**
 * Class FixAttachmentsForm
 * @package app\models\forms
 */
class FixAttachmentsForm extends Model
{
    /**
     * @var int
     */
    public $fixLetterImageId;

    /**
     * @var mixed
     */
    public $easyAttachments;

    /**
     * @var mixed
     */
    public $middleAttachments;

    /**
     * @var mixed
     */
    public $hardAttachments;

    /**
     * @var mixed
     */
    public $easyAttachmentsPrinted;

    /**
     * @var mixed
     */
    public $middleAttachmentsPrinted;

    /**
     * @var mixed
     */
    public $hardAttachmentsPrinted;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['fixLetterImageId'], 'required'],
            [['easyAttachments', 'middleAttachments', 'hardAttachments',
                'easyAttachmentsPrinted', 'middleAttachmentsPrinted', 'hardAttachmentsPrinted'
            ], 'file', 'skipOnEmpty' => false, 'maxFiles' => 20],
        ];
    }

    public function attributeLabels()
    {
        return [
            'easyAttachments' => 'Легкий уровень',
            'middleAttachments' => 'Средний уровень',
            'hardAttachments' => 'Тяжелый уровень',
            'easyAttachmentsPrinted' => 'Легкий уровень',
            'middleAttachmentsPrinted' => 'Средний уровень',
            'hardAttachmentsPrinted' => 'Тяжелый уровень'
        ];
    }

    public function save($id)
    {
        foreach ($this->easyAttachments as $file) {

            if(is_dir('attachmentFixLettersImage') == false){
                mkdir('attachmentFixLettersImage');
            }

            $name = \Yii::$app->security->generateRandomString();
            $path = "attachmentFixLettersImage/{$name}.{$file->extension}";

            $file->saveAs($path);

            $attach = new FixLettersImagesAttachments(['fix_letter_image_id' => $id, 'type' => FixLettersImagesAttachments::TYPE_EASY, 'path' => $path]);
            $attach->save(false);
        }

        foreach ($this->middleAttachments as $file) {

            if(is_dir('attachmentFixLettersImage') == false){
                mkdir('attachmentFixLettersImage');
            }

            $name = \Yii::$app->security->generateRandomString();
            $path = "attachmentFixLettersImage/{$name}.{$file->extension}";

            $file->saveAs($path);

            $attach = new FixLettersImagesAttachments(['fix_letter_image_id' => $id, 'type' => FixLettersImagesAttachments::TYPE_MIDDLE, 'path' => $path]);
            $attach->save(false);
        }

        foreach ($this->hardAttachments as $file) {

            if(is_dir('attachmentFixLettersImage') == false){
                mkdir('attachmentFixLettersImage');
            }

            $name = \Yii::$app->security->generateRandomString();
            $path = "attachmentFixLettersImage/{$name}.{$file->extension}";

            $file->saveAs($path);

            $attach = new FixLettersImagesAttachments(['fix_letter_image_id' => $id, 'type' => FixLettersImagesAttachments::TYPE_HARD, 'path' => $path]);
            $attach->save(false);
        }

        foreach ($this->easyAttachmentsPrinted as $file) {

            if(is_dir('attachmentFixLettersImage') == false){
                mkdir('attachmentFixLettersImage');
            }

            $name = \Yii::$app->security->generateRandomString();
            $path = "attachmentFixLettersImage/{$name}.{$file->extension}";

            $file->saveAs($path);

            $attach = new FixLettersImagesAttachments(['fix_letter_image_id' => $id, 'type' => FixLettersImagesAttachments::TYPE_EASY, 'drawing' => FixLettersImagesAttachments::DRAWING_PRINTED, 'path' => $path]);
            $attach->save(false);
        }

        foreach ($this->middleAttachmentsPrinted as $file) {

            if(is_dir('attachmentFixLettersImage') == false){
                mkdir('attachmentFixLettersImage');
            }

            $name = \Yii::$app->security->generateRandomString();
            $path = "attachmentFixLettersImage/{$name}.{$file->extension}";

            $file->saveAs($path);

            $attach = new FixLettersImagesAttachments(['fix_letter_image_id' => $id, 'type' => FixLettersImagesAttachments::TYPE_MIDDLE, 'drawing' => FixLettersImagesAttachments::DRAWING_PRINTED, 'path' => $path]);
            $attach->save(false);
        }

        foreach ($this->hardAttachmentsPrinted as $file) {

            if(is_dir('attachmentFixLettersImage') == false){
                mkdir('attachmentFixLettersImage');
            }

            $name = \Yii::$app->security->generateRandomString();
            $path = "attachmentFixLettersImage/{$name}.{$file->extension}";

            $file->saveAs($path);

            $attach = new FixLettersImagesAttachments(['fix_letter_image_id' => $id, 'type' => FixLettersImagesAttachments::TYPE_HARD, 'drawing' => FixLettersImagesAttachments::DRAWING_PRINTED, 'path' => $path]);
            $attach->save(false);
        }
    }
}