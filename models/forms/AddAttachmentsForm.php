<?php

namespace app\models\forms;

use app\models\FreeEmailsAttachments;
use yii\base\Model;
use yii\web\UploadedFile;

/**
 * Class AddAttachmentsForm
 * @package app\models\attachment
 *
 */
class AddAttachmentsForm extends Model
{
    /**
     * @var int
     */
    public $freeEmailId;

    /**
     * @var UploadedFile[]
     */
    public $files;

    /**
     * @var array
     */
    private $attached;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            // name, email, subject and body are required
            [['taskId',], 'required'],

            [['files',], 'file', 'skipOnEmpty' => false, 'maxFiles' => 5],
        ];
    }

    /**
     * @return array customized attribute labels
     */
    public function attributeLabels()
    {
        return [
            'files' => 'Файлы',
        ];
    }

    /**
     * @return array
     */
    public function getAttached()
    {
        return $this->attached;
    }

    /**
     *
     */
    public function save()
    {
        foreach ($this->files as $file) {

            if(is_dir('attachment') == false){
                mkdir('attachment');
            }

            $name = \Yii::$app->security->generateRandomString();
            $path = "attachment/{$name}.{$file->extension}";

            $this->attached[] = $path;
            $file->saveAs($path);

            $attach = new FreeEmailsAttachments(['name' => $file->name, 'email_id' => $this->freeEmailId, 'path' => $path]);
            $attach->save(false);
        }
    }
}
