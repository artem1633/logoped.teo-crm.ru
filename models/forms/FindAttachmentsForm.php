<?php
namespace app\models\forms;

use app\models\FindLettersImagesAttachments;
use app\models\FixLettersImagesAttachments;
use yii\base\Model;

/**
 * Class FindAttachmentsForm
 * @package app\models\forms
 */
class FindAttachmentsForm extends Model
{
    /**
     * @var int
     */
    public $findLetterImageId;

    /**
     * @var mixed
     */
    public $wroteDownAttachments;

    /**
     * @var mixed
     */
    public $wroteUpAttachments;

    /**
     * @var mixed
     */
    public $printedDownAttachments;

    /**
     * @var mixed
     */
    public $printedUpAttachments;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['findLetterImageId'], 'required'],
            [['wroteDownAttachments', 'wroteUpAttachments', 'printedDownAttachments', 'printedUpAttachments'], 'file', 'skipOnEmpty' => false, 'maxFiles' => 20],
        ];
    }

    public function attributeLabels()
    {
        return [
            'wroteDownAttachments' => 'Маленькие',
            'wroteUpAttachments' => 'Большие',
            'printedDownAttachments' => 'Маленькие',
            'printedUpAttachments' => 'Большие',
        ];
    }

    public function save($id)
    {
        foreach ($this->wroteDownAttachments as $file) {

            if(is_dir('attachmentFindLettersImage') == false){
                mkdir('attachmentFindLettersImage');
            }

            $name = \Yii::$app->security->generateRandomString();
            $path = "attachmentFindLettersImage/{$name}.{$file->extension}";

            $file->saveAs($path);

            $attach = new FindLettersImagesAttachments(['find_letter_image_id' => $id, 'type' => FindLettersImagesAttachments::TYPE_WROTE_DOWN, 'path' => $path]);
            $attach->save(false);
        }

        foreach ($this->wroteUpAttachments as $file) {

            if(is_dir('attachmentFindLettersImage') == false){
                mkdir('attachmentFindLettersImage');
            }

            $name = \Yii::$app->security->generateRandomString();
            $path = "attachmentFindLettersImage/{$name}.{$file->extension}";

            $file->saveAs($path);

            $attach = new FindLettersImagesAttachments(['find_letter_image_id' => $id, 'type' => FindLettersImagesAttachments::TYPE_WROTE_UP, 'path' => $path]);
            $attach->save(false);
        }

        foreach ($this->printedDownAttachments as $file) {

            if(is_dir('attachmentFindLettersImage') == false){
                mkdir('attachmentFindLettersImage');
            }

            $name = \Yii::$app->security->generateRandomString();
            $path = "attachmentFindLettersImage/{$name}.{$file->extension}";

            $file->saveAs($path);

            $attach = new FindLettersImagesAttachments(['find_letter_image_id' => $id, 'type' => FindLettersImagesAttachments::TYPE_PRINTED_DOWN, 'path' => $path]);
            $attach->save(false);
        }

        foreach ($this->printedUpAttachments as $file) {

            if(is_dir('attachmentFindLettersImage') == false){
                mkdir('attachmentFindLettersImage');
            }

            $name = \Yii::$app->security->generateRandomString();
            $path = "attachmentFindLettersImage/{$name}.{$file->extension}";

            $file->saveAs($path);

            $attach = new FindLettersImagesAttachments(['find_letter_image_id' => $id, 'type' => FindLettersImagesAttachments::TYPE_PRINTED_UP, 'path' => $path]);
            $attach->save(false);
        }
    }
}