<?php

namespace app\models\forms;

use app\behaviors\MailsBehavior;
use app\models\StrictEmails;
use app\models\users\User;
use Yii;
use yii\base\Model;

/**
 * Class ChangePasswordForm
 * @package app\models\forms
 */
class ChangePasswordForm extends Model
{
    const EVENT_PASSWORD_CHANGED = 'password_changed;';

    /**
     * @var string
     */
    public $oldPassword;

    /**
     * @var string
     */
    public $newPassword;

    /**
     * @var string
     */
    private $_oldPassword;

    /**
     * @var string
     */
    private $_newPassword;

    /**
     * @var User
     */
    private $_user;

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        $this->_user = Yii::$app->user->identity;
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => MailsBehavior::class,
                'mails' => [
                    self::EVENT_PASSWORD_CHANGED => [
                        'to' => function(){
                            var_dump($this->_user->login);
                            return $this->_user->login;
                        },
                        'subject' => 'Изменение пароля',
                        'htmlBody' => StrictEmails::findByKey('forget_password')->content,
                        'hasTags' => true,
                        'customTags' => function(){
                            return ['{old_password}' => $this->_oldPassword, '{new_password}' => $this->_newPassword];
                        },
                    ],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['oldPassword', 'newPassword'], 'required'],
            [['oldPassword', 'newPassword'], 'string', 'max' => 255],
            ['oldPassword', function($model){
                if($this->_user->validatePassword($this->oldPassword) == false){
                    $this->addError('oldPassword', 'Не верный пароль');
                }
            }]
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'oldPassword' => 'Старый пароль',
            'newPassword' => 'Новый пароль',
        ];
    }

    /**
     * Меняет пароль у текущего пользователя
     * @return bool
     */
    public function changePassword()
    {
        if($this->validate())
        {
            $this->_user->setPassword($this->newPassword);
            $this->_oldPassword = $this->oldPassword;
            $this->_newPassword = $this->newPassword;
            $this->oldPassword = null;
            $this->newPassword = null;
            $this->trigger(self::EVENT_PASSWORD_CHANGED);
            return true;
        }

        return false;
    }
}