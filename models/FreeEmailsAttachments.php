<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "free_emails_attachments".
 *
 * @property int $id
 * @property int $email_id Письмо
 * @property string $name Имя файла
 * @property string $path Путь до файла
 * @property string $created_at
 *
 * @property FreeEmails $email
 */
class FreeEmailsAttachments extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::class,
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => null,
                'value' => date('Y-m-d H:i:s'),
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'free_emails_attachments';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['email_id'], 'integer'],
            [['created_at'], 'safe'],
            [['name', 'path'], 'string', 'max' => 255],
            [['email_id'], 'exist', 'skipOnError' => true, 'targetClass' => FreeEmails::class, 'targetAttribute' => ['email_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'email_id' => 'Email ID',
            'name' => 'Наименование',
            'path' => 'Путь',
            'created_at' => 'Created At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEmail()
    {
        return $this->hasOne(FreeEmails::class, ['id' => 'email_id']);
    }
}
