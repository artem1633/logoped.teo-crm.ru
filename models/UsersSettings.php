<?php

namespace app\models;

use Yii;
use app\models\users\Users;
use yii\behaviors\TimestampBehavior;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "users_settings".
 *
 * @property int $id
 * @property int $user_id Пользователь
 * @property int $find_letter_data_keeping Хранение пособий «Найти букву»
 * @property int $fix_letter_data_keeping Хранение пособий «Починить букву»
 * @property int $find_letter_data_subscribe Месячное хранение пособий «Найти букву»
 * @property int $fix_letter_data_subscribe Месячное хранение пособий «Починить букву»
 * @property int $after_balance_up
 * @property int $after_letter_create
 * @property int $balance_up_reminder
 * @property int $news
 * @property string $updated_at
 * @property string $created_at
 *
 * @property Users $user
 */
class UsersSettings extends \yii\db\ActiveRecord
{
    const SETTINGS_LIST = ['find_letter_data_keeping', 'fix_letter_data_keeping', 'find_letter_data_subscribe', 'fix_letter_data_subscribe',
        'after_balance_up', 'after_letter_create', 'balance_up_reminder', 'news'];

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'users_settings';
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::class,
                'updatedAtAttribute' => 'updated_at',
                'createdAtAttribute' => 'created_at',
                'value' => date('Y-m-d H:i:s'),
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [ArrayHelper::merge(self::SETTINGS_LIST, ['user_id']), 'integer'],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => Users::class, 'targetAttribute' => ['user_id' => 'id']],
            [['updated_at', 'created_at'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'find_letter_data_keeping' => 'Find Letter Data Keeping',
            'fix_letter_data_keeping' => 'Fix Letter Data Keeping',
            'find_letter_data_subscribe' => 'Find Letter Data Subscribe',
            'fix_letter_data_subscribe' => 'Fix Letter Data Subscribe',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(Users::class, ['id' => 'user_id']);
    }

    /**
     * Возвращает влючена ли настройка или нет
     * @param string $setting
     * @return boolean
     */
    public function isEnabled($setting)
    {
        if(in_array($setting, self::SETTINGS_LIST)) {
            return $this->$setting == 1 ? true : false;
        }

        return false;
    }

    /**
     * Включает настройку
     * @param string $setting
     * @return boolean
     */
    public function switchOn($setting)
    {
        if(in_array($setting, self::SETTINGS_LIST)) {
            $this->$setting = 1;
            return $this->save(false);
        }

        return false;
    }

    /**
     * Выключает настройку
     * @param string $setting
     * @return boolean
     */
    public function switchOff($setting)
    {
        if(in_array($setting, self::SETTINGS_LIST)) {
            $this->$setting = 0;
            return $this->save(false);
        }

        return false;
    }
}
