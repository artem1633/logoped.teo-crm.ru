<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "fix_letters_images".
 *
 * @property int $id
 * @property string $symbol Символ
 *
 * @property int $commonAttachmentsCount
 * @property int $easyAttachmentsWroteCount
 * @property int $middleAttachmentsWroteCount
 * @property int $hardAttachmentsWroteCount
 * @property int $easyAttachmentsPrintedCount
 * @property int $middleAttachmentsPrintedCount
 * @property int $hardAttachmentsPrintedCount
 *
 * @property FixLettersImagesAttachments[] $fixLettersImagesAttachments
 */
class FixLettersImages extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'fix_letters_images';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['symbol'], 'string', 'max' => 10],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'symbol' => 'Символ',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFixLettersImagesAttachments()
    {
        return $this->hasMany(FixLettersImagesAttachments::className(), ['fix_letter_image_id' => 'id']);
    }

    public function getCommonAttachmentsCount()
    {
        return FixLettersImagesAttachments::find()->where(['fix_letter_image_id' => $this->id])->count();
    }


    public function getEasyAttachmentsWroteCount()
    {
        return FixLettersImagesAttachments::find()->where(['fix_letter_image_id' => $this->id, 'type' => FixLettersImagesAttachments::TYPE_EASY, 'drawing' => FixLettersImagesAttachments::DRAWING_WROTE])->count();
    }

    public function getMiddleAttachmentsWroteCount()
    {
        return FixLettersImagesAttachments::find()->where(['fix_letter_image_id' => $this->id, 'type' => FixLettersImagesAttachments::TYPE_MIDDLE, 'drawing' => FixLettersImagesAttachments::DRAWING_WROTE])->count();
    }

    public function getHardAttachmentsWroteCount()
    {
        return FixLettersImagesAttachments::find()->where(['fix_letter_image_id' => $this->id, 'type' => FixLettersImagesAttachments::TYPE_HARD, 'drawing' => FixLettersImagesAttachments::DRAWING_WROTE])->count();
    }

    public function getEasyAttachmentsPrintedCount()
    {
        return FixLettersImagesAttachments::find()->where(['fix_letter_image_id' => $this->id, 'type' => FixLettersImagesAttachments::TYPE_EASY, 'drawing' => FixLettersImagesAttachments::DRAWING_PRINTED])->count();
    }

    public function getMiddleAttachmentsPrintedCount()
    {
        return FixLettersImagesAttachments::find()->where(['fix_letter_image_id' => $this->id, 'type' => FixLettersImagesAttachments::TYPE_MIDDLE, 'drawing' => FixLettersImagesAttachments::DRAWING_PRINTED])->count();
    }

    public function getHardAttachmentsPrintedCount()
    {
        return FixLettersImagesAttachments::find()->where(['fix_letter_image_id' => $this->id, 'type' => FixLettersImagesAttachments::TYPE_HARD, 'drawing' => FixLettersImagesAttachments::DRAWING_PRINTED])->count();
    }
}
