<?php

namespace app\models;

use app\models\users\Users;
use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "balances".
 *
 * @property int $id
 * @property int $user_id
 * @property string $amount Сумма платежа
 * @property string $pay_date Дата платежа
 * @property int $promo_code_id Промокод
 * @property string $amount_bonus Сумма бонуса
 * @property string $category Категория
 * @property string $comment Описание
 */
class Balances extends \yii\db\ActiveRecord
{
    const DEBIT = 1;
    const CREDIT = 0;

    const LETTER_PURCHASE = 'letter_purchase';

    const CATEGORY_SUBSCRIPTION_FIND = 'subscription_find';
    const CATEGORY_SUBSCRIPTION_FIX = 'subscription_fix';

    const CATEGORY_STORAGE_FIND = 'storage_find';
    const CATEGORY_STORAGE_FIX = 'storage_fix';

    const CATEGORY_BONUS = 'bonus';

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'balances';
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::class,
                'updatedAtAttribute' => null,
                'createdAtAttribute' => 'pay_date',
                'value' => date('Y-m-d H:i:s')
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'promo_code_id','dk'], 'integer'],
            [['amount', 'amount_bonus'], 'number'],
            [['pay_date'], 'safe'],
            [['comment', 'category'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'amount' => 'Сумма платежа',
            'pay_date' => 'Дата платежа',
            'promo_code_id' => 'Промокод',
            'amount_bonus' => 'Сумма бонуса',
            'dk' => 'Дебет/Кредит',
            'category' => 'Категория',
            'comment' => 'Описание',
        ];
    }

    public function getUsers()
    {
        return $this->hasOne(Users::class, ['id' => 'user_id']);
    }
}
