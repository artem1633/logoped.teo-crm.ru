<?php

namespace app\models;

use app\queries\PromoCodeQuery;
use Yii;

/**
 * This is the model class for table "promo_code".
 *
 * @property int $id
 * @property string $name Название промокода
 * @property string $description Комментарий
 * @property string $start_date Дата начала действия
 * @property string $end_date Дата окончания действия
 * @property int $endless Бессрочно
 * @property int $action Действие
 *
 * @property boolean $isActive
 *
 * @property PromoCodesUsages[] $promoCodesUsages
 */
class PromoCode extends \yii\db\ActiveRecord
{
    CONST ACTION_ADD = 0;
    CONST ACTION_PERCENT = 1;

    /**
     * @var string
     */
    public $dates;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'promo_code';
    }

    /**
     * {@inheritdoc}
     */
    public static function find()
    {
        return new PromoCodeQuery(get_called_class());
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['action_value','name'], 'required'],
            ['dates', 'required', 'when' => function($model){
                return $this->endless != 1;
            }],
            [['endless', 'action'], 'integer'],
            [['name', 'dates'], 'string', 'max' => 255],
            [['description'], 'string', 'max' => 500],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название промокода',
            'description' => 'Комментарий',
            'start_date' => 'Дата начала действия',
            'end_date' => 'Дата окончания действия',
            'dates' => 'Дата начала и окончания действия',
            'endless' => 'Бессрочно',
            'action' => 'Действие',
            'action_value' => "Значение"
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function beforeSave($insert)
    {
        if($this->dates != null){
            $this->dates = explode(' - ', $this->dates);
            $this->start_date = $this->dates[0];
            $this->end_date = $this->dates[1];
        }

        return parent::beforeSave($insert);
    }

    /**
     * @return array
     * Возвращает список действий
     */
    public static function getActions()
    {
        return [
            self::ACTION_ADD => 'Добавить к пополнению руб.',
            self::ACTION_PERCENT => 'Добавить к пополнению % руб.',
            ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPromoCodesUsages()
    {
        return $this->hasMany(PromoCodesUsages::class, ['promo_code_id' => 'id']);
    }

    /**
     * @param $status
     * @return string
     * Возвращает имя статуса
     */
    public function getActionName()
    {
        switch ($this->action) {
            case self::ACTION_ADD  : 'Добавить к пополнению руб.';
            case self::ACTION_PERCENT : 'Добавить к пополнению % руб.';
            default: return "Неизвестно";
        }
    }

    /**
     * @return boolean
     */
    public function getIsActive()
    {
        if($this->endless == 1)
            return true;

        if($this->start_date == null || $this->end_date == null)
            return false;

        $now = strtotime(date('Y-m-d'));
        $start = strtotime($this->start_date);
        $end = strtotime($this->end_date);

        return (($now <= $end) && ($now >= $start));
    }
}
