<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "fonts".
 *
 * @property int $id
 * @property string $font_family Наименование
 * @property string $eot_file_path EOT путь
 * @property string $eot_ie_file_path EOT IE путь
 * @property string $woff2_file_path WOFF2 путь
 * @property string $woff_file_path WOFF путь
 * @property string $ttf_file_path TTF путь
 * @property string $svg_file_path SVG путь
 * @property string $font_weight Ширина
 * @property string $font_style Стиль
 */
class Fonts extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'fonts';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['font_family', 'eot_file_path', 'eot_ie_file_path', 'woff2_file_path', 'woff_file_path', 'ttf_file_path', 'svg_file_path', 'font_weight', 'font_style'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'font_family' => 'Font Family',
            'eot_file_path' => 'Eot File Path',
            'eot_ie_file_path' => 'Eot Ie File Path',
            'woff2_file_path' => 'Woff2 File Path',
            'woff_file_path' => 'Woff File Path',
            'ttf_file_path' => 'Ttf File Path',
            'svg_file_path' => 'Svg File Path',
            'font_weight' => 'Font Weight',
            'font_style' => 'Font Style',
        ];
    }
}
