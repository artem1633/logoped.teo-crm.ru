<?php

namespace app\models;

use app\behaviors\MailsBehavior;
use app\behaviors\PaymentBehavior;
use app\models\users\Users;
use app\validators\LetterPaymentValidator;
use php_rutils\RUtils;
use Yii;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "fix_letter".
 *
 * @property int $id
 * @property int $user_id
 * @property int $page_format Формат листа
 * @property int $page_count Количество листов
 * @property int $letters_count Количество букв
 * @property string $letters_composition Состав букв
 * @property int $complexity Сложность
 * @property string $amount Стоимость
 * @property int $save Хранить
 *
 * @property Users $user
 * @property FixLetterData[] $fixLetterData
 */
class FixLetter extends Letter
{
    public $smallLetterlist = ['а','б','в','г','д','е','ё','ж','з','и','й','к','л','м','н','о','п','р',
        'с','т','у','ф','х','ц','ч','ш','щ','ъ','ы','ь','э','ю','я'];
    public $bigLetterlist = ['А','Б','В','Г','Д','Е','Ё','Ж','З','И','Й','К','Л','М','Н','О','П','Р',
        'С','Т','У','Ф','Х','Ц','Ч','Ш','Щ','Ъ','Ы','Ь','Э','Ю','Я'];

    public $printUp = [];
    public $printLow = [];
    public $propisUp = [];
    public $propisLow = [];

    public $data = [];

    /**
     * @var float
     */
    private $_creationPrice;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'fix_letter';
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
            [
                'class' => PaymentBehavior::class,
                'writeOffEvents' => [
                    ActiveRecord::EVENT_AFTER_INSERT => [
                        'amount' => function($event){
                            return $event->sender->getCreationPrice();
                        },
                        'category' => Balances::LETTER_PURCHASE,
                        'comment' => function($event){
                            $model = $event->sender;
                            $variants = ['лист', 'листа', 'листов'];
                            return "Почини букву, {$model->page_count} ".RUtils::numeral()->choosePlural($model->page_count, $variants);
                        },
                        'condition' => function(){
                            $user = $this->user;

                            return !($user->subscription->fixDaily->typeInstance->isSubscribed || $user->subscription->fixMonth->typeInstance->isSubscribed);
                        }
                    ],
                ],
            ],
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return ArrayHelper::merge(parent::rules(), [
            [['user_id', 'letters_count', 'page_count', 'complexity', 'save'], 'integer'],
            [['amount'], 'number'],
            [['letters_composition'], 'string', 'max' => 1000],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => Users::class, 'targetAttribute' => ['user_id' => 'id']],
            [['using_end_datetime', 'data'], 'safe'],
            [['page_format'], LetterPaymentValidator::class]
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'page_format' => 'Формат листа',
            'page_count' => 'Количество листов',
            'letters_count' => 'Количество букв',
            'letters_composition' => 'Состав букв',
            'complexity' => 'Сложность',
            'amount' => 'Стоимость',
            'save' => 'Хранить',
            'using_end_datetime' => 'Дата и время окончания возможности использования'
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(Users::class, ['id' => 'user_id']);
    }

    public function beforeSave($insert)
    {
        if ($this->isNewRecord) {
            $this->user_id = Yii::$app->user->identity->id;
        }
        return parent::beforeSave($insert);
    }

    public static function getPageFormat()
    {
        return [
            self::FORMAT_PAGE_A4 => 'A4',
            self::FORMAT_PAGE_A3 => 'A3',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFixLetterData()
    {
        return $this->hasMany(FixLetterData::class, ['fix_letter_id' => 'id']);
    }

    /**
     * Возвращает цену создания пособия
     * @return string
     */
    public function getCreationPrice()
    {
        if($this->_creationPrice != null){
            return $this->_creationPrice;
        }

        $this->_creationPrice = floatval($this->page_format == Letter::FORMAT_PAGE_A4 ? Settings::findByKey('cost_allowance_fix_letterA4')->value : Settings::findByKey('cost_allowance_fix_letterA3')->value);
        return $this->_creationPrice;
    }
}
