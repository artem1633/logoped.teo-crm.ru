<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "free_email_recipients".
 *
 * @property int $id
 * @property int $email_id Запись о рассылки
 * @property string $recipient_email Почта на которую будет отправлена тело почты
 * @property string $content Содержание сообщения
 * @property int $status Статус
 * @property string $exception_text Текст исключения возникшего во премя ошибки
 * @property string $send_datetime Дата и время попытки отправки
 *
 * @property FreeEmails $email
 */
class FreeEmailRecipients extends \yii\db\ActiveRecord
{
    const STATUS_ERROR = -1;
    const STATUS_WAIT = 0;
    const STATUS_SUCCESS = 1;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'free_email_recipients';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['email_id', 'status'], 'integer'],
            [['content', 'exception_text'], 'string'],
            [['send_datetime'], 'safe'],
            [['recipient_email'], 'string', 'max' => 255],
            [['email_id'], 'exist', 'skipOnError' => true, 'targetClass' => FreeEmails::class, 'targetAttribute' => ['email_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'email_id' => 'Запись о рассылки',
            'recipient_email' => 'Почта на которую будет отправлена тело почты',
            'content' => 'Содержание сообщения',
            'status' => 'Статус',
            'exception_text' => 'Текст ошибки',
            'send_datetime' => 'Дата и время попытки отправки',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEmail()
    {
        return $this->hasOne(FreeEmails::class, ['id' => 'email_id']);
    }
}
