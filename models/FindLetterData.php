<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "find_letter_data".
 *
 * @property int $id
 * @property int $find_letter_id Пособие
 * @property string $symbol Буква
 * @property string $type Стиль написания
 * @property int $amount Кол-во
 *
 * @property FindLetter $findLetter
 */
class FindLetterData extends \yii\db\ActiveRecord
{
    const TYPE_PRINT_UP = 0;
    const TYPE_PRINT_LOW = 1;
    const TYPE_RECIPE_UP = 2;
    const TYPE_RECIPE_LOW = 3;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'find_letter_data';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['find_letter_id', 'amount'], 'integer'],
            [['symbol'], 'string', 'max' => 10],
            [['type'], 'string', 'max' => 255],
            [['find_letter_id'], 'exist', 'skipOnError' => true, 'targetClass' => FindLetter::class, 'targetAttribute' => ['find_letter_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'find_letter_id' => 'Find Letter ID',
            'symbol' => 'Symbol',
            'type' => 'Type',
            'amount' => 'Amount',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFindLetter()
    {
        return $this->hasOne(FindLetter::class, ['id' => 'find_letter_id']);
    }
}
