<?php

namespace app\models;

use app\behaviors\MailsBehavior;
use app\models\users\Users;
use app\queries\MessagesQuery;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "messages".
 *
 * @property int $id
 * @property string $text_message Текст сообщения
 * @property string $start_date Дата начала показа
 * @property string $end_date Дата конца показа
 * @property string $button1 Кнопка 1 
 * @property string $button2 Кнопка 2 
 */
class Messages extends \yii\db\ActiveRecord
{
    /**
     * @var string
     */
    public $dates;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'messages';
    }

    /**
     * {@inheritdoc}
     */
    public static function find()
    {
        return new MessagesQuery(get_called_class());
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => MailsBehavior::class,
                'mails' => [
                    self::EVENT_AFTER_INSERT => [
                        'subject' => 'Новость',
                        'htmlBody' => function(){
                            return $this->text_message;
                        },
                        'to' => ArrayHelper::getColumn(Users::find()->joinWith('usersSettings')->where(['users_settings.news' => 1])->all(), 'login'),
                        'hasTags' => true,
                        'condition' => function($event, $user){
                            /** @var Users $user */
                            return $user->settings->isEnabled('news');
                        }
                    ],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['dates', 'start_date', 'end_date'], 'safe'],
            [['button1', 'button2'], 'string', 'max' => 255],
            [['text_message'], 'string']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'text_message' => 'Текст сообщения',
            'start_date' => 'Дата начала показа',
            'end_date' => 'Дата конца показа',
            'dates' => 'Срок показа',
            'button1' => 'Кнопка 1',
            'button2' => 'Кнопка 2',
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function beforeSave($insert)
    {
        if($this->dates != null){
            $this->dates = explode(' - ', $this->dates);
            $this->start_date = $this->dates[0];
            $this->end_date = $this->dates[1];
        }

        return parent::beforeSave($insert);
    }
}
