<?php

namespace app\models;

use app\behaviors\MailsBehavior;
use Yii;
use yii\base\Model;
use app\models\users\Users;
use app\models\users\Account;
use yii\helpers\Html;

/**
 * Class RegisterForm
 * @package app\models
 */
class RegisterForm extends Model
{
    const EVENT_REGISTERED = 'event_registered';

    public $fio;
    public $login;
    public $password;
    public $town;
    public $telephone;
    public $telegram;
    public $agree;

    /**
     * @var \app\models\users\Users;
     */
    private $_user;

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => MailsBehavior::class,
                'mails' => [
                    self::EVENT_REGISTERED => [
                        'subject' => 'Успешная регистрация',
                        'to' => function(){
                            return $this->_user->login;
                        },
                        'htmlBody' => StrictEmails::findByKey('register')->content,
                        'hasTags' => true,
                        'customTags' => function(){
                            return ['{email_confirm}' => $this->_user->emailConfirmLinkHTML];
                        }
                    ],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['fio', 'login', 'password', 'agree'], 'required'],
            [['telephone', 'town'], 'string'],
            [['password', 'telegram'], 'string', 'max' => 255],
            [['login'], 'email'],
            [['agree'], 'integer'],
            [['login'], 'unique', 'targetClass' => Users::class],
            ['agree', 'validateAgree'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'fio' => 'ФИО',
            'login' => 'E-mail',
            'password' => 'Пароль',
            'telephone' => 'Телефон',
            'telegram' => 'Ид чат телеграма',
            'town' => 'Город',
            'agree' => 'Я согласен с обработкой персональных данных',
        ];
    }

    public function validateAgree($attribute)
    { 
        if($this->agree != 1) $this->addError($attribute, 'Необходимо Ваше согласие');
    }

    /**
     * Регистрирует нового пользователя
     * @param int $type
     * @return Users|null
     */
    public function register($ref, $type = 1)
    {
        if($this->validate() === false){
            return null;
        }
        $user_id = null;
        if( $ref !== null ) {
            foreach (Users::find()->all() as $user) {
                if($ref == md5($user->id)) $user_id = $user->id;
            }
        }

        $user = new Users();
        $userAccount = new Account();
        $user->fio = $this->fio;
        $user->login = $this->login;
        $user->telephone = $this->telephone;
        $user->password = $this->password;
        $user->telegram_id = $this->telegram;
        $user->type = $type;
        $userSaveResult = $user->save(false);

        $userAccount->user_id = $user->id;
        $userAccount->balance = Settings::find()->where(['key' => 'register_bonus'])->one()->value;
        $userAccount->town = $this->town;
        $userAccount->save(false);

        if($userSaveResult) {
            $this->_user = $user;
            $this->trigger(self::EVENT_REGISTERED);
            return $user;
        }

        return null;
    }
}