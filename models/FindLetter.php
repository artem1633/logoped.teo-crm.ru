<?php

namespace app\models;

use app\behaviors\MailsBehavior;
use app\behaviors\PaymentBehavior;
use app\models\users\EmailMarkers;
use app\models\users\Users;
use app\validators\LetterPaymentValidator;
use php_rutils\RUtils;
use Yii;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "find_letter".
 *
 * @property int $id
 * @property int $user_id
 * @property int $page_format Формат листа
 * @property int $page_count Количество листов
 * @property int $letters_count Количество букв
 * @property string $letters_composition Состав букв
 * @property int $complexity Сложность
 * @property string $amount Стоимость
 * @property int $save Хранить
 * @property string $symbols_data JSON данные о символах
 *
 * @property array $dataAmount
 *
 * @property Users $user
 * @property FindLetterData[] $findLetterData
 */
class FindLetter extends Letter
{
    public $smallLetterlist = ['а','б','в','г','д','е','ё','ж','з','и','й','к','л','м','н','о','п','р',
                          'с','т','у','ф','х','ц','ч','ш','щ','ъ','ы','ь','э','ю','я'];
    public $bigLetterlist = ['А','Б','В','Г','Д','Е','Ё','Ж','З','И','Й','К','Л','М','Н','О','П','Р',
        'С','Т','У','Ф','Х','Ц','Ч','Ш','Щ','Ъ','Ы','Ь','Э','Ю','Я'];

    public $printUp = [];
    public $printLow = [];
    public $recipeUp = [];
    public $recipeLow = [];

    /**
     * @var float
     */
    private $_creationPrice;

    /**
     * @var array
     */
    private $_dataAmount;

    private $isSymbolsDataSaved = false;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'find_letter';
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
            [
                'class' => PaymentBehavior::class,
                'writeOffEvents' => [
                    ActiveRecord::EVENT_AFTER_INSERT => [
                        'amount' => function($event){
                            return $event->sender->getCreationPrice();
                        },
                        'category' => Balances::LETTER_PURCHASE,
                        'comment' => function($event){
                            $model = $event->sender;
                            $variants = ['лист', 'листа', 'листов'];
                            return "Найди букву, {$model->page_count} ".RUtils::numeral()->choosePlural($model->page_count, $variants);
                        },
                        'condition' => function(){
                            $user = $this->user;

                            return !($user->subscription->findDaily->typeInstance->isSubscribed || $user->subscription->findMonth->typeInstance->isSubscribed);
                        }
                    ],
                ],
            ],
            [
                'class' => MailsBehavior::class,
                'mails' => [
                    self::EVENT_AFTER_INSERT => [
                        'to' => function(){
                            return $this->user->login;
                        },
                        'subject' => 'Пособие «Найди букву»',
                        'htmlBody' => StrictEmails::findByKey('letter')->content,
                        'hasTags' => true,
                        'attach' => function(){
                            return $this->pdf_file;
                        },
                    ],
                ],
            ],
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return ArrayHelper::merge(parent::rules(), [
            [['user_id', 'page_format', 'page_count', 'letters_count', 'complexity', 'save'], 'integer'],
            [['amount'], 'number'],
            [['letters_composition'], 'string', 'max' => 1000],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => Users::class, 'targetAttribute' => ['user_id' => 'id']],
            [['using_end_datetime'], 'safe'],
            [['symbols_data', 'printUp', 'printLow', 'recipeUp', 'recipeLow'], 'safe'],
            [['page_format'], LetterPaymentValidator::class],
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'page_format' => 'Формат листа',
            'page_count' => 'Количество листов',
            'letters_count' => 'Количество букв',
            'letters_composition' => 'Состав букв',
            'complexity' => 'Сложность',
            'amount' => 'Стоимость',
            'save' => 'Хранить',
            'using_end_datetime' => 'Дата и время окончания возможности использования'
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(Users::class, ['id' => 'user_id']);
    }

    /**
     * {@inheritdoc}
     */
    public function beforeSave($insert)
    {
        if ($this->isNewRecord) {
            $this->user_id = Yii::$app->user->identity->id;
        }
        return parent::beforeSave($insert);
    }

    /**
     * {@inheritdoc}
     */
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

        if($this->isSymbolsDataSaved == false)
        {
            $thinLetterData = FindLetterData::find()->where(['find_letter_id' => $this->id, 'type' => FindLetterData::TYPE_RECIPE_LOW])->andFilterWhere(['>','amount', 0])->all();
            $lettersImages = \app\models\FindLettersImages::find()->all();
            $imagesSymbols = [];

            foreach ($lettersImages as $image){
                $attachmentsThin = \yii\helpers\ArrayHelper::getColumn(\app\models\FindLettersImagesAttachments::find()->where(['find_letter_image_id' => $image->id, 'type' => \app\models\FindLettersImagesAttachments::TYPE_WROTE_DOWN])->all(), 'path');

                $currentData = null;
                foreach ($thinLetterData as $data){
                    if($data->symbol == $image->symbol) {
                        $currentData = $data;
                    }
                }

                if($currentData == null){
                    continue;
                }

                $imagesSymbols[$image->symbol] = [
                    'types' => [
                        \app\models\FindLettersImagesAttachments::TYPE_WROTE_DOWN => [
                            'count' => $currentData->amount,
                            'paths' => $attachmentsThin,
                        ],
                    ],
                ];
            }

            $this->symbols_data = json_encode($imagesSymbols);
            $this->isSymbolsDataSaved = true;
            $this->save(false);
        }

    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFindLetterData()
    {
        return $this->hasMany(FindLetterData::class, ['find_letter_id' => 'id']);
    }

    /**
     * Возвращает информацию о буквах
     * @return array
     */
    public function getDataAmount()
    {
        if($this->_dataAmount != null){
            return $this->_dataAmount;
        } else {
            $this->_dataAmount = [
                FindLetterData::TYPE_PRINT_UP => ArrayHelper::map(FindLetterData::find()->where(['find_letter_id' => $this->id, 'type' => FindLetterData::TYPE_PRINT_UP])->all(), 'symbol', 'amount'),
                FindLetterData::TYPE_PRINT_LOW => ArrayHelper::map(FindLetterData::find()->where(['find_letter_id' => $this->id, 'type' => FindLetterData::TYPE_PRINT_LOW])->all(), 'symbol', 'amount'),
                FindLetterData::TYPE_RECIPE_UP => ArrayHelper::map(FindLetterData::find()->where(['find_letter_id' => $this->id, 'type' => FindLetterData::TYPE_RECIPE_UP])->all(), 'symbol', 'amount'),
                FindLetterData::TYPE_RECIPE_LOW => ArrayHelper::map(FindLetterData::find()->where(['find_letter_id' => $this->id, 'type' => FindLetterData::TYPE_RECIPE_LOW])->all(), 'symbol', 'amount'),
            ];
        }

        return $this->_dataAmount;
    }

    /**
     * Возвращает цену создания пособия
     * @return string
     */
    public function getCreationPrice()
    {
        if($this->_creationPrice != null){
            return $this->_creationPrice;
        }

        $this->_creationPrice = floatval($this->page_format == Letter::FORMAT_PAGE_A4 ? Settings::findByKey('cost_allowance_find_letterA4')->value : Settings::findByKey('cost_allowance_find_letterA3')->value);
        return $this->_creationPrice;
    }
}
