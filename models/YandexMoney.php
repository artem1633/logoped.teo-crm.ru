<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "yandex_money".
 *
 * @property int $id
 * @property string $secret Секрет
 * @property string $wallet Кошелек
 */
class YandexMoney extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'yandex_money';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['secret'], 'string', 'max' => 50],
            [['wallet'], 'string', 'max' => 20],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'secret' => 'Секрет',
            'wallet' => 'Кошелек',
        ];
    }
}
