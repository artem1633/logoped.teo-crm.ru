<?php

namespace app\validators;

use app\models\FindLetter;
use app\models\FixLetter;
use app\models\users\Users;
use Yii;
use yii\validators\Validator;

/**
 * Class LetterPaymentValidator
 * @package app\validators
 */
class LetterPaymentValidator extends Validator
{
    /**
     * @var string
     */
    public $message = 'Недостаточно средств';

    /**
     * {@inheritdoc}
     */
    public function validateAttribute($model, $attribute)
    {
        if($model instanceof FindLetter){
            $comparePrice = $model->getCreationPrice();
        } else if ($model instanceof FixLetter) {
            $comparePrice = $model->getCreationPrice();
        } else {
            $comparePrice = 0;
        }

        $user = Users::findOne(Yii::$app->user->getId());
        if($user->wallet->hasAmount($comparePrice) == false){
            $model->addError($attribute, $this->message);
        }
    }
}