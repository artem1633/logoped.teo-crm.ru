<?php

namespace app\controllers;

use app\behaviors\UpdateOnlineBehavior;
use app\models\FindLetterSearch;
use app\models\FixLetterSearch;
use app\models\FixLettersImages;
use app\models\forms\ChangePasswordForm;
use app\models\users\Users;
use Yii;
use app\models\users\Account;
use app\models\users\AccountSearch;
use yii\helpers\VarDumper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * AccountController implements the CRUD actions for Account model.
 */
class AccountController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'online' => [
                'class' => UpdateOnlineBehavior::class
            ],
            'access' => [
                'class' => \yii\filters\AccessControl::class,
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Account models.
     * @param $id int
     * @throws NotFoundHttpException
     * @return mixed
     */
    public function actionIndex($id = null)
    {
        $request = Yii::$app->request;
        if($id != null && Yii::$app->user->identity->isSuperAdmin()){
            /** @var Users $model */
            $model = Users::findOne($id);
            if($model == null){
                throw new NotFoundHttpException('Пользователь не найден');
            }
        } else {
            /** @var Users $model */
            $model = Yii::$app->user->identity;
        }
        $changePasswordForm = new ChangePasswordForm();
        $account = $model->account;
        if (!$account) {
            $account = new Account();
        }

        if($account->load($request->post())){
            $account->save(false);
        }

        if($model->load($request->post())){
            $model->save(false);
        }

        if($changePasswordForm->load($request->post()) && $changePasswordForm->changePassword()){
            Yii::$app->session->setFlash('success_password_changed', 'Пароль был успешно изменен.');
        }

        $findSearchModel = new FindLetterSearch();
        $findDataProvider = $findSearchModel->search(Yii::$app->request->queryParams);
        $findDataProvider->query->andFilterWhere(['user_id' => $model->id]);

        $fixSearchModel = new FixLetterSearch();
        $fixDataProvider = $fixSearchModel->search(Yii::$app->request->queryParams);
        $fixDataProvider->query->andFilterWhere(['user_id' => $model->id]);


        return $this->render('index', [
            'model' => $model,
            'account' => $account,
            'changePasswordForm' => $changePasswordForm,
            'findSearchModel' => $findSearchModel,
            'findDataProvider' => $findDataProvider,
            'fixSearchModel' => $fixSearchModel,
            'fixDataProvider' => $fixDataProvider,
        ]);
    }

    /**
     * Displays a single Account model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Account model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Account();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Account model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Account model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Account model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Account the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Account::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
