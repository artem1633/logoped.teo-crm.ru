<?php
/**
 * Created by PhpStorm.
 * User: Ilusha
 * Date: 24.02.2019
 * Time: 0:44
 */

namespace app\controllers;

use app\behaviors\UpdateOnlineBehavior;
use yii\web\Controller;
use yii\helpers\Html;

/**
 * Class LetterController
 * @package app\controllers
 * Родительский контроллер для FindLetterController и FixLetterController
 */
class LetterController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'online' => [
                'class' => UpdateOnlineBehavior::class
            ],
            'access' => [
                'class' => \yii\filters\AccessControl::class,
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    /**
     * @param \app\models\Letter $model
     * @return array
     */
    protected function getUsingExpiredResponse($model)
    {
        return [
            'title'=> "Внимание",
            'content'=>'<span class="text-danger">Хранение пособия не оплачено, пополните баланс  и повторите попытку</span>',
            'footer'=> Html::button('Отмена',['class'=>'btn btn-primary pull-left','data-dismiss'=>"modal"]).
                Html::a('Пополнить баланс', ['balance/index'], ['class' => 'btn btn-primary', 'data-pjax' => 0]).
                Html::a('Купить подписку',['balance/index'],['class'=>'btn btn-info', 'data-pjax' => 0]).
                Html::a('Удалить',['delete', 'id' => $model->id],['class'=>'btn btn-danger','role'=>'modal-remote',
                    'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                    'data-request-method'=>'post',
                    'data-toggle'=>'tooltip',
                    'data-confirm-title'=>'Вы уверенны?',
                    'data-confirm-message'=>'Вы действительно хотите удалить пособие']),
        ];
    }
}