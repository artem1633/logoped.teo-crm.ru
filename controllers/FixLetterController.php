<?php

namespace app\controllers;

use app\models\FixLetterData;
use app\models\Letter;
use kartik\form\ActiveForm;
use kartik\mpdf\Pdf;
use Mpdf\Config\ConfigVariables;
use Mpdf\Config\FontVariables;
use Mpdf\Mpdf;
use Yii;
use app\models\FixLetter;
use app\models\FixLetterSearch;
use yii\helpers\ArrayHelper;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use \yii\web\Response;
use yii\helpers\Html;

/**
 * FixLetterController implements the CRUD actions for FixLetter model.
 */
class FixLetterController extends LetterController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['post'],
                    'bulk-delete' => ['post'],
                ],
            ],
        ]);
    }



    /**
     * Lists all FixLetter models.
     * @return mixed
     */
    public function actionIndex()
    {    
        $searchModel = new FixLetterSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }


    /**
     * Displays a single Messages model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);
        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;

            if($model->isUsingExpired){
                return $this->getUsingExpiredResponse($model);
            }

            return [
                'title'=> "пособие #".$model->id,
                'content'=>$this->renderAjax('view', [
                    'model' => $model,
                ]),
                'footer'=> Html::button('Омена',['class'=>'btn btn-primary pull-left','data-dismiss'=>"modal"]).
                    Html::a('Изменить',['update','id'=>$id],['class'=>'btn btn-info','role'=>'modal-remote'])
            ];
        }else{
            return $this->render('view', [
                'model' => $this->findModel($id),
            ]);
        }
    }

    public function actionTestGeneration($id)
    {
        $model = $this->findModel($id);
        $content = $this->renderPartial('@app/views/_pdf/fix_letter', [
            'model' => $model
        ]);

        $defaultConfig = (new ConfigVariables())->getDefaults();
        $fontDirs = $defaultConfig['fontDir'];

        $defaultFontConfig = (new FontVariables())->getDefaults();
        $fontData = $defaultFontConfig['fontdata'];

        if($model->page_format == Letter::FORMAT_PAGE_A3){
            $format = Pdf::FORMAT_A3;
        } else if($model->page_format == Letter::FORMAT_PAGE_A4){
            $format = Pdf::FORMAT_A4;
        }


        $pdf = new Mpdf([
            'fontDir' => array_merge($fontDirs, [
                __DIR__.'/fonts'
            ]),
            'fontData' => [
                'ProximaNova' => [
                    'R' => 'ProximaNova-Regular.ttf',
                    'I' => 'ProximaNova-Regular.ttf',
                ],
                'RobotoRegular' => [
                    'R' => 'RobotoRegular.ttf',
                    'I' => 'RobotoRegular.ttf',
                ],
            ],
            'default_font' => 'RobotoRegular',
            'mode' => Pdf::MODE_UTF8,
            'format' => $format,
            'orientation' => Pdf::ORIENT_PORTRAIT,
            'destination' => Pdf::DEST_BROWSER,
            'content' => $content,
            'cssFile' => '@vendor/kartik-v/yii2-mpdf/assets/kv-mpdf-bootstrap.min.css',
            'cssInline' => file_get_contents('css/pdf/fix.css'),
        ]);
        $pdf->WriteHTML($content);

        return $pdf->Output();
    }

    /**
     * @param int $id
     * @return mixed
     */
    public function actionSendToEmail($id)
    {
        $model = $this->findModel($id);
        $user = $model->user;

        if($model->pdf_file != null){
            $result = Yii::$app->mailer->compose()
                ->setFrom('logoped.service@mail.ru')
                ->setTo($user->login)
                ->setSubject('Пособие')
                ->setHtmlBody('Пособие «Найди букву»')
                ->attach($model->pdf_file)
                ->send();
            if($result){
                Yii::$app->session->setFlash('success', 'Успешно отправлено');
            } else {
                Yii::$app->session->setFlash('error', 'Во время отправки произошла ошибка');
            }
        } else {
            Yii::$app->session->setFlash('error', 'Файл не найден');
        }

        return $this->redirect(['index']);
    }


    /**
     * Creates a new Messages model.
     * For ajax request will return json object
     * and for non-ajax request if creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $request = Yii::$app->request;
        $model = new FixLetter();
        $model->page_count = 1;

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> "Создать новое пособие",
                    'content'=>$this->renderAjax('create', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Отмена',['class'=>'btn btn-primary pull-left','data-dismiss'=>"modal"]).
                        Html::button('Сохранить',['class'=>'btn btn-info','type'=>"submit"])

                ];
            }else if($model->load($request->post()) && $model->save()){

                foreach ($model->data as $index => $row) {
                    $data = new FixLetterData([
                        'fix_letter_id' => $model->id,
                        'symbol' => $model->smallLetterlist[intval($row['\'symbol\''])],
                        'type' => $row['\'type\''],
                    ]);
                    $data->save(false);
                }

                $model->pdfGenerator->generate();

                return [
                    'forceReload'=>'#crud-datatable-pjax',
                    'title'=> "Создание нового пособия",
                    'content'=>'<span class="text-success">Новое пособие успешно создано</span>',
                    'footer'=> Html::button('Отмена',['class'=>'btn btn-primary pull-left','data-dismiss'=>"modal"]).
                        Html::a('Создать еще',['create'],['class'=>'btn btn-info','role'=>'modal-remote'])

                ];
            }else{
                return [
                    'title'=> "Создать новое пособие",
                    'content'=>$this->renderAjax('create', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Отмена',['class'=>'btn btn-primary pull-left','data-dismiss'=>"modal"]).
                        Html::button('Сохранить',['class'=>'btn btn-info','type'=>"submit"])
                ];
            }
        }else{
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('create', [
                    'model' => $model,
                ]);
            }
        }

    }

    /**
     * Updates an existing Messages model.
     * For ajax request will return json object
     * and for non-ajax request if update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> "Изменить пособие #".$id,
                    'content'=>$this->renderAjax('update', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Отмена',['class'=>'btn btn-primary pull-left','data-dismiss'=>"modal"]).
                        Html::button('Сохранить',['class'=>'btn btn-info','type'=>"submit"])
                ];
            }else if($model->load($request->post()) && $model->save()){
                return [
                    'forceReload'=>'#crud-datatable-pjax',
                    'title'=> "Пособие #".$id,
                    'content'=>$this->renderAjax('view', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Отмена',['class'=>'btn btn-primary pull-left','data-dismiss'=>"modal"]).
                        Html::a('Изменить',['update','id'=>$id],['class'=>'btn btn-warning','role'=>'modal-remote'])
                ];
            }else{
                return [
                    'title'=> "Изменить пособие #".$id,
                    'content'=>$this->renderAjax('update', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Отмена',['class'=>'btn btn-primary pull-left','data-dismiss'=>"modal"]).
                        Html::button('Сохранить',['class'=>'btn btn-info','type'=>"submit"])
                ];
            }
        }else{
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('update', [
                    'model' => $model,
                ]);
            }
        }
    }

    /**
     * @param int $lettersCount
     * @return string
     */
    public function actionLetterForm($lettersCount)
    {
        $model = new FixLetter();
        $form = ActiveForm::begin();

        return $this->renderAjax('_form_data',[
            'form' => $form,
            'lettersCount' => $lettersCount,
            'model' => $model
        ]);
    }

    /**
     * Delete an existing FixLetter model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $request = Yii::$app->request;
        $this->findModel($id)->delete();

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose'=>true,'forceReload'=>'#crud-datatable-pjax'];
        }else{
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }


    }

     /**
     * Delete multiple existing FixLetter model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionBulkDelete()
    {        
        $request = Yii::$app->request;
        $pks = explode(',', $request->post( 'pks' )); // Array or selected records primary keys
        foreach ( $pks as $pk ) {
            $model = $this->findModel($pk);
            $model->delete();
        }

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose'=>true,'forceReload'=>'#crud-datatable-pjax'];
        }else{
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }
       
    }

    /**
     * Finds the FixLetter model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return FixLetter the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = FixLetter::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
