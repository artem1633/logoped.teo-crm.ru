<?php

namespace app\controllers;

use app\behaviors\UpdateOnlineBehavior;
use app\models\Settings;
use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;

/**
 * BalancesController implements the CRUD actions for Balances model.
 */
class SettingsController extends Controller
{

    public function behaviors()
    {
        return [
            'online' => [
                'class' => UpdateOnlineBehavior::class
            ],
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    'bulkdelete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Список параметров
     * @return mixed
     */
    public function actionIndex()
    {

        $request = Yii::$app->request;

        if($request->isPost)
        {
            $data = $request->post();
            $checked = [];

            foreach ($data['Settings'] as $key => $value) {
                $setting = Settings::findByKey($key);

                if($setting->type == Settings::TYPE_CHECKBOX){
                    $checked[] = $setting->id;
                }

                if ($setting != null) {
                    $setting->value = $value;
                    $setting->save();
                    Yii::$app->session->setFlash('success', 'Настройки успешно сохранены');
                }
            }


            $notChecked = Settings::find()->where(['type' => Settings::TYPE_CHECKBOX]);

            foreach ($checked as $id){
                $notChecked->andWhere(['!=', 'id', $id]);
            }

            $notChecked = $notChecked->all();

            foreach ($notChecked as $checkboxSetting){
                $checkboxSetting->value = '0';
                $checkboxSetting->save(false);
            }
        }

        $settings = Settings::find()->all();

        return $this->render('index', [
            'settings' => $settings,
        ]);
    }



}
