<?php

namespace app\controllers;

use app\behaviors\UpdateOnlineBehavior;
use app\models\forms\PhoneConfirmForm;
use app\models\Messages;
use app\models\PhonesConfirmations;
use app\models\users\Users;
use app\services\events\SmsError;
use app\services\SmsRu;
use Yii;
use app\models\users\UsersSearch;
use yii\helpers\Url;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\web\HttpException;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use \yii\web\Response;
use yii\helpers\Html;
use yii\web\UploadedFile;
use app\models\Settings;
use app\modules\api\controllers\BotinfoController;

/**
 * UsersController implements the CRUD actions for Users model.
 */
class UsersController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'online' => [
                'class' => UpdateOnlineBehavior::class
            ],
            'access' => [
                'class' => \yii\filters\AccessControl::class,
                'rules' => [
                   [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    'bulkdelete' => ['post'],
                ],
            ],
        ];
    }

    public function actionDashboard()
    {
        $now = date('Y-m-d');
        $messages = Messages::find()->active()->orderBy('id desc')->all();

        return $this->render('dashboard', [
            'questionaryCount' => 0,
            'resumeCount' => 0,
            'resumeTodayCount' => 0,
            'sendCount' => 0,
            'messages' => $messages,
            'days' => 0,
            'values' => [],
            'questionaries' => [],
        ]);  
    }

    /**
     * Lists all Users models.
     * @return mixed
     */
    public function actionIndex()
    {    
        $searchModel = new UsersSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->sort->defaultOrder = ['id' => SORT_DESC];

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * @param string $confirmToken
     * @throws NotFoundHttpException
     * @return mixed
     */
    public function actionConfirmEmail($confirmToken)
    {
        $token = explode('$ep', $confirmToken);

        if(count($token) == 2){
            $user = Users::findOne($token[0]);
            if($user == null){
                throw new NotFoundHttpException('Страница не найдена');
            }

            if(Yii::$app->security->validatePassword($user->login, $token[1])){
                $user->email_confirmed = 1;
                $user->save(false);

                return $this->renderContent('Ваш E-mail успешно подтвержден. '.Html::a('На главную', Url::home()));
            } else {
                throw new NotFoundHttpException('Страница не найдена');
            }
        }

        throw new NotFoundHttpException('Страница не найдена');
    }

    /**
     * @param int $again
     * @return mixed
     */
    public function actionConfirmPhone($again = 0)
    {
        /** @var Users $user */
        $user = Yii::$app->user->identity;
        $model = new PhoneConfirmForm(['user' => $user]);
        /** @var PhonesConfirmations $code */
        $code = PhonesConfirmations::find()->where(['user_id' => $user->id])->one();
        $error = false;
        $errorCode = '';

        if($code->status == PhonesConfirmations::STATUS_CONFIRMED){ // Если код подтрвежден
            return $this->redirect(['account/index']);
        }

        if($code == null){ // Если код не отправлен, то отправляем его
            $code = PhonesConfirmations::generate($user->id);
            $response = Yii::$app->smsRu->send($user->telephone, "Ваш код подтверждения: {$code->code}");
            if($response['status'] != SmsRu::SUCCESS_STATUS){
                $error = true;
                $errorCode = strval($response['status_code']);
            }
        }

        if($again == 1 && $code->isActive == false){ // Отправляем ещё раз если пользователь нажал на кнопку «Отправить ещё» и срок дейсвтия кода истек
            $code->delete();
            $code = PhonesConfirmations::generate($user->id);
            $response = Yii::$app->smsRu->send($user->telephone, "Ваш код подтверждения: {$code->code}");
            if($response['status'] != SmsRu::SUCCESS_STATUS){
                $error = true;
                $errorCode = strval($response['status_code']);
            }
        }

        if($model->load(Yii::$app->request->post()) && $model->confirm())
        {
            return $this->redirect(['account/index']);
        } else {
            return $this->render('confirm-phone', [
                'model' => $model,
                'code' => $code,
                'error' => $error,
                'errorCode' => $errorCode
            ]);
        }
    }

    /**
     * @test
     * @return string
     */
    public function actionTestEmailHash()
    {
        /** @var Users $user */
        $user = Yii::$app->user->identity;

        $string = "{$user->id}\$ep";

        $string .= Yii::$app->security->generatePasswordHash($user->login);

        return $string;
    }

    public function actionInstruction()
    {    
        $setting = Settings::find()->where(['key' => 'instruction_editor'])->one();

        return $this->render('instruction', [
            'setting' => $setting,
        ]);
    }

    /**
     * Displays a single Users model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id) 
    {   
        $request = Yii::$app->request;
        $model = $this->findModel($id);
        $chat = new Chat();
        $chat->chat_id = '#users-'.$id;

        if ( $request->post() ) {
            if($request->post()['text'] != ''){
                $chat->text = $request->post()['text'];
                $chat->save();
                if ($model->connect_to_telegram) {
                    BotinfoController::getReq('sendMessage', ['chat_id' => $model->telegram_id, 'parse_mode'=>'HTML', 'text' => $request->post()['text']]);
                }
                return $this->redirect(['/users/view', 'id' => $id]);
            }
        }

        $chatText = Chat::find()->where(['chat_id' => '#users-'.$id ])->all();

        return $this->render('view', [
            'model' => $model,
            'chatText' => $chatText,
        ]);
    }

    /**
     * Displays a single Users model.
     * @param integer $id
     * @return mixed
     */
    public function actionViewInTable($id)
    {   
        $request = Yii::$app->request;
        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                    'title'=> "ФИО : " . $this->findModel($id)->fio,
                    'content'=>$this->renderAjax('view_in_table', [
                        'model' => $this->findModel($id),
                    ]),
                    'footer'=> Html::button('Отмена',['class'=>'btn btn-primary pull-left','data-dismiss'=>"modal"]).
                            Html::a('Изменить',['update','id'=>$id],['class'=>'btn btn-info','role'=>'modal-remote'])
                ];    
        }else{
            return $this->render('view_in_table', [
                'model' => $this->findModel($id),
            ]);
        }
    }

    /**
     * Creates a new Users model.
     * For ajax request will return json object
     * and for non-ajax request if creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $request = Yii::$app->request;
        $model = new Users();  

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> "Создать",
                    'size' => 'large',
                    'content'=>$this->renderAjax('create', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Отмена',['class'=>'btn btn-primary pull-left','data-dismiss'=>"modal"]).
                                Html::button('Сохранить',['class'=>'btn btn-info','type'=>"submit"])
        
                ];         
            }else if($model->load($request->post()) && $model->save()){
                return [
                    'forceReload'=>'#crud-datatable-pjax',
                    'size' => 'normal',
                    'title'=> "Создать",
                    'content'=>'<span class="text-success">Успешно выпольнено</span>',
                    'footer'=> Html::button('Ок',['class'=>'btn btn-primary pull-left','data-dismiss'=>"modal"]).
                            Html::a('Создать ещё',['create'],['class'=>'btn btn-info','role'=>'modal-remote'])
        
                ];         
            }else{           
                return [
                    'title'=> "Создать",
                    'size' => 'large',
                    'content'=>$this->renderAjax('create', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Отмена',['class'=>'btn btn-primary pull-left','data-dismiss'=>"modal"]).
                                Html::button('Сохранить',['class'=>'btn btn-info','type'=>"submit"])
        
                ];         
            }
        }else{
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('create', [
                    'model' => $model,
                ]);
            }
        }
       
    }

    /**
     * Updates an existing Users model.
     * For ajax request will return json object
     * and for non-ajax request if update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);
        $account = $model->account;

        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;

            if(($model->load($request->post()) && $model->save()) && ($account->load($request->post()) && $account->save())) {
              return ['forceClose'=>true,'forceReload'=>'#crud-datatable-pjax'];   
            }else{
                 return [
                    'title'=> 'Изменить',
                    'size' => 'large',
                    'content'=>$this->renderAjax('update', [
                        'model' => $model,
                        'account' => $account,
                    ]),
                    'footer'=> Html::button('Отмена',['class'=>'btn btn-primary pull-left','data-dismiss'=>"modal"]).
                                Html::button('Сохранить',['class'=>'btn btn-info','type'=>"submit"])
                ];        
            }
        }else{
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('update', [
                    'model' => $model,
                ]);
            }
        }
    }

    /**
     * Creates a new Users model.
     * For ajax request will return json object
     * and for non-ajax request if creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionAvatar()
    {
        $request = Yii::$app->request;
        $id = Yii::$app->user->identity->id;
        $model = Users::findOne(Yii::$app->user->identity->id);  

        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($model->load($request->post()) && $model->validate()){

                $model->file = UploadedFile::getInstance($model, 'file');
                if(!empty($model->file)){
                    $model->file->saveAs('avatars/'.$id.'.'.$model->file->extension);
                    Yii::$app->db->createCommand()->update('users', ['foto' => $id.'.'.$model->file->extension], [ 'id' => $id ])->execute();
                }

                return [
                    'forceReload'=>'#profile-pjax',
                    'size' => 'normal',
                    'title'=> "Аватар",
                    'forceClose' => true,     
                ];         
            }else{           
                return [
                    'title'=> "Аватар",
                    'size' => 'small',
                    'content'=>$this->renderAjax('avatar', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Отмена',['class'=>'btn btn-primary pull-left','data-dismiss'=>"modal"]).
                                Html::button('Сохранить',['class'=>'btn btn-info','type'=>"submit"])
        
                ];         
            }
        }
       
    }

    public function actionChange($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);       

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($model->load($request->post()) && $model->save()){
                
                return [
                    'forceClose' => true,
                    'forceReload'=>'#profile-pjax',
                ];    
            }else{
                 return [
                    'title'=> "Изменить",
                    'size' => "large",
                    'content'=>$this->renderAjax('change', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Отмена',['class'=>'btn btn-primary pull-left','data-dismiss'=>"modal"]).
                                Html::button('Сохранить',['class'=>'btn btn-info','type'=>"submit"])
                ];        
            }
        }
        else{
            echo "ddd";
        }
    }

    public function actionProfile()
    {
        $request = Yii::$app->request;
        return $this->render('profile', [
        ]);        
    }

    /**
     * Delete an existing Users model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $request = Yii::$app->request;
        if($id != 1) $this->findModel($id)->delete();

        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose'=>true,'forceReload'=>'#crud-datatable-pjax'];
        }else{
            return $this->redirect(['index']);
        }
    }

    /**
     * Delete multiple existing Vacancy model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionBulkDelete()
    {        
        $request = Yii::$app->request;
        $pks = explode(',', $request->post( 'pks' )); // Array or selected records primary keys
        foreach ( $pks as $pk ) {
            $model = $this->findModel($pk);
            if($pk != 1) $model->delete();
        }

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose'=>true,'forceReload'=>'#crud-datatable-pjax'];
        }else{
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }
       
    }

    /**
     * @param $userId int
     */
    public function actionSetSettingValueAjax($userId = null)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
       if(isset($_POST['attribute']) == false || isset($_POST['value']) == false){
           if($_POST['attribute'] == '1'){
               //TODO: MAKE GROSS
           }
           throw new BadRequestHttpException();
       }

       $attribute = $_POST['attribute'];
       $value = $_POST['value'];

       if($userId == null){
           $settings = Yii::$app->user->identity->settings;
       } else {
           $user = Users::findOne($userId);
           if($user == null){
               throw new NotFoundHttpException('Пользователь не найден');
           }
           $settings = $user->settings;
       }

//
//
//       var_dump($settings);

       if(isset($settings->$attribute))
       {
           $settings->$attribute = $value;
           $result = $settings->save(false);

            if($value == 1)
            {
                if($userId == null){
                    /** @var Users $user */
                    $user = Yii::$app->user->identity;
                } else {
                    /** @var Users $user */
                    $user = Users::findOne($userId);
                }

                if($attribute == 'find_letter_data_keeping')
                {
                    if($user->subscription->findDaily->typeInstance->getIsSubscribed() && $user->subscription->findMonth->typeInstance->getIsSubscribed() == false)
                    {
                        if( $user->subscription->findDaily->typeInstance->getIsPayed() == false)
                        {
                            $user->subscription->findDaily->typeInstance->pay();
                        }
                    }
                } else if($attribute == 'fix_letter_data_keeping')
                {
                    if($user->subscription->fixDaily->typeInstance->getIsSubscribed() && $user->subscription->fixMonth->typeInstance->getIsSubscribed() == false)
                    {
                        if($user->subscription->fixDaily->typeInstance->getIsPayed() == false)
                        {
                            $user->subscription->fixDaily->typeInstance->pay();
                        }
                    }
                } else if($attribute == 'find_letter_data_subscribe')
                {
                    if($user->subscription->findMonth->typeInstance->getIsSubscribed())
                    {
                        if($user->subscription->findDaily->typeInstance->isSubscribed)
                            $user->subscription->findDaily->typeInstance->unSubscribe();

                        if($user->subscription->findMonth->typeInstance->getIsPayed() == false)
                        {
                            $user->subscription->findMonth->typeInstance->pay();
                        }
                    }
                } else if($attribute == 'fix_letter_data_subscribe')
                {
                    if($user->subscription->fixMonth->typeInstance->getIsSubscribed())
                    {
                        if($user->subscription->fixDaily->typeInstance->isSubscribed)
                            $user->subscription->fixDaily->typeInstance->unSubscribe();

                        if($user->subscription->fixMonth->typeInstance->getIsPayed() == false)
                        {
                            $user->subscription->fixMonth->typeInstance->pay();
                        }
                    }
                }
            }
       } else {
           throw new BadRequestHttpException('Invalid attribute');
       }

       if($result == false) {
           if(isset($settings->errors[0]))
           {
               throw new BadRequestHttpException($settings->errors[0]);
           } else {
               throw new HttpException(500);
           }
       }

       return [];
    }

    /**
     * Finds the Users model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Users the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Users::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
