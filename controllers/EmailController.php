<?php

namespace app\controllers;

use app\behaviors\UpdateOnlineBehavior;
use app\components\helpers\TagHelper;
use app\models\Balances;
use app\models\forms\AddAttachmentsForm;
use app\models\FreeEmailRecipientsSearch;
use app\models\FreeEmails;
use app\models\FreeEmailsSearch;
use app\models\PhonesConfirmations;
use app\models\PromoCode;
use app\models\StrictEmails;
use app\models\users\Users;
use app\services\SmsRu;
use kartik\file\FileInput;
use PharIo\Manifest\Email;
use Yii;
use yii\helpers\ArrayHelper;
use yii\validators\EmailValidator;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\UploadedFile;

/**
 * Class EmailController
 * @package app\controllers
 */
class EmailController extends Controller
{

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'online' => [
                'class' => UpdateOnlineBehavior::class
            ],
            'access' => [
                'class' => \yii\filters\AccessControl::class,
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    /**
     * @return mixed
     */
    public function actionIndex()
    {
        $emails = StrictEmails::find()->all();

        return $this->render('index', [
            'emails' => $emails,
        ]);
    }

    public function actionTestEmail()
    {
    }

    /**
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $request = Yii::$app->request;
        $model = $this->findStrictModel($id);

        if ($model->load($request->post()) && $model->save()) {
            return $this->redirect(['index']);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Lists all FreeEmails models.
     * @return mixed
     */
    public function actionFree()
    {
        $searchModel = new FreeEmailsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('@app/views/free-emails/index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Creates a new FreeEmails model.
     * For ajax request will return json object
     * and for non-ajax request if creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionWriteNew()
    {
        $request = Yii::$app->request;
//        $model = FreeEmails::find()->where(['send_datetime' => null])->one();
//        if($model == null){
            $model = new FreeEmails();
//        }

        if ($model->load($request->post()) && $model->start()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('@app/views/free-emails/create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * @param int $id
     * @return mixed
     */
    public function actionView($id)
    {
        $model = $this->findFreeModel($id);
        $searchModel = new FreeEmailRecipientsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('@app/views/free-emails/view.php', [
            'model' => $model,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Creates a new FreeEmails model.
     * For ajax request will return json object
     * and for non-ajax request if creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionEditWritten()
    {
        $request = Yii::$app->request;
        $model = new FreeEmails();

        if ($model->load($request->post()) && $model->save()) {
            return $this->redirect(['index']);
        } else {
            return $this->render('@app/views/free-emails/update', [
                'model' => $model,
            ]);
        }
    }

    public function actionDelete($attachmentId)
    {
        $attachment = Attachment::findOne($attachmentId);
        if ($attachment) {
            $attachment->delete();
        }

        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        return ['success' => true];
    }

    public function actionUploadAttachment($id)
    {
        $model = $this->findFreeModel($id);
        $attachments = new AddAttachmentsForm(['freeEmailId' => $id]);
        $attachments->files = UploadedFile::getInstances($model, 'attachment');
        $attachments->save();

        if (count($attachments->getAttached()) > 0) {

            $emailAttachments = ArrayHelper::getColumn($model->freeEmailsAttachments, 'path');


            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            return ['success' => true];

        } else {
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            return ['success' => false];
        }
    }

    /**
     * Finds the StrictEmails model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @re turn StrictEmails the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findStrictModel($id)
    {
        if (($model = StrictEmails::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

     /**
      * Finds the FreeEmails model based on its primary key value.
      * If the model is not found, a 404 HTTP exception will be thrown.
      * @param integer $id
      * @return FreeEmails the loaded model
      * @throws NotFoundHttpException if the model cannot be found
      */
    protected function findFreeModel($id)
    {
        if (($model = FreeEmails::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}