<?php

namespace app\controllers;

use app\models\FindLetterData;
use app\models\FindLettersImagesAttachments;
use app\models\Letter;
use kartik\mpdf\Pdf;
use Mpdf\Config\ConfigVariables;
use Mpdf\Config\FontVariables;
use Mpdf\Mpdf;
use Yii;
use app\models\FindLetter;
use app\models\FindLetterSearch;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use \yii\web\Response;
use yii\helpers\Html;

/**
 * FindLetterController implements the CRUD actions for FindLetter model.
 */
class FindLetterController extends LetterController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['post'],
                    'bulk-delete' => ['post'],
                ],
            ],
        ]);
    }

    /**
     * Lists all FindLetter models.
     * @return mixed
     */
    public function actionIndex()
    {    
        $searchModel = new FindLetterSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }


    /**
     * Displays a single Messages model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);
        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;

            if($model->isUsingExpired){
                return $this->getUsingExpiredResponse($model);
            }

            return [
                'title'=> "пособие #".$model->id,
                'content'=>$this->renderAjax('view', [
                    'model' => $model,
                ]),
                'footer'=> Html::button('Омена',['class'=>'btn btn-primary pull-left','data-dismiss'=>"modal"]).
                    Html::a('Изменить',['update','id'=>$id],['class'=>'btn btn-info','role'=>'modal-remote'])
            ];
        }else{
            return $this->render('view', [
                'model' => $this->findModel($id),
            ]);
        }
    }

    /**
     * Creates a new Messages model.
     * For ajax request will return json object
     * and for non-ajax request if creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $request = Yii::$app->request;
        $model = new FindLetter();
        $model->page_count = 1;

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> "Создать новое пособие",
                    'content'=>$this->renderAjax('create', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Отмена',['class'=>'btn btn-primary pull-left','data-dismiss'=>"modal"]).
                        Html::button('Создать',['class'=>'btn btn-info','type'=>"submit"])

                ];
            }else if($model->load($request->post()) && $model->save()){

                $this->saveLetterData($model);

                $model->pdfGenerator->generate();

                return [
                    'forceReload'=>'#crud-datatable-pjax',
                    'title'=> "Создание нового пособия",
                    'content'=>'<span class="text-success">Новое пособие успешно создано</span>',
                    'footer'=> Html::button('Отмена',['class'=>'btn btn-primary pull-left','data-dismiss'=>"modal"]).
                        Html::a('Создать еще',['create'],['class'=>'btn btn-info','role'=>'modal-remote'])

                ];
            }else{
                return [
                    'title'=> "Создать новое пособие",
                    'content'=>$this->renderAjax('create', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Отмена',['class'=>'btn btn-primary pull-left','data-dismiss'=>"modal"]).
                        Html::button('Создать',['class'=>'btn btn-info','type'=>"submit"])

                ];
            }
        }else{
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('create', [
                    'model' => $model,
                ]);
            }
        }
    }


    /**
     * @return string
     */
    public function actionTestGen()
    {
        $content = '<span style="font-size: 30mm;">Привет</span>';

//        echo __DIR__ . '/../web/fonts/';
//        exit;

        $defaultConfig = (new \Mpdf\Config\ConfigVariables())->getDefaults();
        $fontDirs = $defaultConfig['fontDir'];

        $defaultFontConfig = (new \Mpdf\Config\FontVariables())->getDefaults();
        $fontData = $defaultFontConfig['fontdata'];

        $pdf = new Mpdf([
            'mode' => Pdf::MODE_UTF8,
            'format' => Pdf::FORMAT_A4,
            'orientation' => Pdf::ORIENT_PORTRAIT,
            'destination' => Pdf::DEST_BROWSER,
            'content' => $content,
            'cssFile' => '@vendor/kartik-v/yii2-mpdf/assets/kv-mpdf-bootstrap.min.css',
            'cssInline' => file_get_contents('css/pdf/find.css'),

            'fontDir' => array_merge($fontDirs, [
                __DIR__ . '/../web/fonts',
            ]),
            'fontdata' => $fontData + [
                    'propisi' => [
                        'R' => 'Propisi.ttf',
                        'I' => 'Propisi.ttf',
                    ]
                ],
            'default_font' => 'propisi'
        ]);
        $pdf->WriteHTML($content);

        return $pdf->Output();
    }

    public function actionSecondTest($id)
    {
        $model = $this->findModel($id);
        $content = $this->renderPartial('@app/views/_pdf/find_letter', [
            'model' => $model,
//            'thinLetterData' => [],
//            'imagesSymbols' => [],
        ]);

        $defaultConfig = (new \Mpdf\Config\ConfigVariables())->getDefaults();
        $fontDirs = $defaultConfig['fontDir'];

        $defaultFontConfig = (new \Mpdf\Config\FontVariables())->getDefaults();
        $fontData = $defaultFontConfig['fontdata'];

        $pdf = new Mpdf([
            'mode' => Pdf::MODE_UTF8,
            'format' => Pdf::FORMAT_A4,
            'orientation' => Pdf::ORIENT_PORTRAIT,
            'destination' => Pdf::DEST_BROWSER,
            'content' => $content,
            'cssFile' => '@vendor/kartik-v/yii2-mpdf/assets/kv-mpdf-bootstrap.min.css',
            'cssInline' => file_get_contents('css/pdf/find.css'),
            'fontDir' => array_merge($fontDirs, [
                __DIR__ . '/../web/fonts',
            ]),
            'fontdata' => $fontData + [
                    'method' => [
                        'R' => 'Method.ttf',
                        'I' => 'Method.ttf',
                    ],
                    'propisi' => [
                        'R' => 'Propisi.ttf',
                        'I' => 'Propisi.ttf',
                    ]
                ],
            'default_font' => 'method'
        ]);
        $pdf->WriteHTML($content);

        return $pdf->Output();
    }

    /**
     * @param $id
     * @return string
     */
    public function actionTestGeneration($id)
    {
        $model = $this->findModel($id);
        $wroteDownLetterData = FindLetterData::find()->where(['find_letter_id' => $model->id, 'type' => FindLetterData::TYPE_RECIPE_LOW])->andFilterWhere(['>','amount', 0])->all();
        $wroteUpLetterData = FindLetterData::find()->where(['find_letter_id' => $model->id, 'type' => FindLetterData::TYPE_RECIPE_UP])->andFilterWhere(['>','amount', 0])->all();
        $printedUpLetterData = FindLetterData::find()->where(['find_letter_id' => $model->id, 'type' => FindLetterData::TYPE_PRINT_UP])->andFilterWhere(['>','amount', 0])->all();
        $printedDownLetterData = FindLetterData::find()->where(['find_letter_id' => $model->id, 'type' => FindLetterData::TYPE_PRINT_LOW])->andFilterWhere(['>','amount', 0])->all();
        $lettersImages = \app\models\FindLettersImages::find()->all();
        $imagesSymbols = [];

        foreach ($lettersImages as $image){
            $attachmentsWroteDown = \yii\helpers\ArrayHelper::getColumn(\app\models\FindLettersImagesAttachments::find()->where(['find_letter_image_id' => $image->id, 'type' => \app\models\FindLettersImagesAttachments::TYPE_WROTE_DOWN])->all(), 'path');
            $attachmentsWroteUp = \yii\helpers\ArrayHelper::getColumn(\app\models\FindLettersImagesAttachments::find()->where(['find_letter_image_id' => $image->id, 'type' => \app\models\FindLettersImagesAttachments::TYPE_WROTE_UP])->all(), 'path');
            $attachmentsPrintedUp = \yii\helpers\ArrayHelper::getColumn(\app\models\FindLettersImagesAttachments::find()->where(['find_letter_image_id' => $image->id, 'type' => \app\models\FindLettersImagesAttachments::TYPE_PRINTED_UP])->all(), 'path');
            $attachmentsPrintedDown = \yii\helpers\ArrayHelper::getColumn(\app\models\FindLettersImagesAttachments::find()->where(['find_letter_image_id' => $image->id, 'type' => \app\models\FindLettersImagesAttachments::TYPE_PRINTED_DOWN])->all(), 'path');

            $wroteDownData = null;
            foreach ($wroteDownLetterData as $data){
                if($data->symbol == $image->symbol) {
                    $wroteDownData = $data;
                }
            }
            $wroteUpData = null;
            foreach ($wroteUpLetterData as $data){
                if($data->symbol == $image->symbol) {
                    $wroteUpData = $data;
                }
            }
            $printedUpData = null;
            foreach ($printedUpLetterData as $data){
                if($data->symbol == $image->symbol) {
                    $printedUpData = $data;
                }
            }
            $printedDownData = null;
            foreach ($printedDownLetterData as $data){
                if($data->symbol == $image->symbol) {
                    $printedDownData = $data;
                }
            }

            $types = [];

            if($wroteDownData != null){
                $types[FindLettersImagesAttachments::TYPE_WROTE_DOWN] = [
                    'count' => $wroteDownData->amount,
                    'paths' => $attachmentsWroteDown,
                ];
            }
            if($wroteUpData != null){
                $types[FindLettersImagesAttachments::TYPE_WROTE_UP] = [
                    'count' => $wroteUpData->amount,
                    'paths' => $attachmentsWroteUp,
                ];
            }
            if($printedUpData != null){
                $types[FindLettersImagesAttachments::TYPE_PRINTED_UP] = [
                    'count' => $printedUpData->amount,
                    'paths' => $attachmentsPrintedUp,
                ];
            }
            if($printedDownData != null){
                $types[FindLettersImagesAttachments::TYPE_PRINTED_DOWN] = [
                    'count' => $printedDownData->amount,
                    'paths' => $attachmentsPrintedDown,
                ];
            }

//            $imagesSymbols[$image->symbol] = [
//                'types' => [
//                    \app\models\FindLettersImagesAttachments::TYPE_WROTE_DOWN => [
//                        'count' => $wroteDownData->amount,
//                        'paths' => $attachmentsWroteDown,
//                    ],
//                ],
//            ];
            $imagesSymbols[$image->symbol] = [
                'types' => $types,
            ];
        }
        $content = $this->renderPartial('@app/views/_pdf/find_letter', [
            'model' => $model,
//            'thinLetterData' => $wroteDownLetterData,
            'imagesSymbols' => $imagesSymbols,
        ]);

        $defaultConfig = (new \Mpdf\Config\ConfigVariables())->getDefaults();
        $fontDirs = $defaultConfig['fontDir'];

        $defaultFontConfig = (new \Mpdf\Config\FontVariables())->getDefaults();
        $fontData = $defaultFontConfig['fontdata'];

        if($model->page_format == Letter::FORMAT_PAGE_A3){
            $format = Pdf::FORMAT_A3;
        } else if($model->page_format == Letter::FORMAT_PAGE_A4){
            $format = Pdf::FORMAT_A4;
        }

        $pdf = new Mpdf([
            'mode' => Pdf::MODE_UTF8,
            'format' => $format,
            'orientation' => Pdf::ORIENT_LANDSCAPE,
            'destination' => Pdf::DEST_BROWSER,
            'content' => $content,
            'cssFile' => '@vendor/kartik-v/yii2-mpdf/assets/kv-mpdf-bootstrap.min.css',
            'cssInline' => file_get_contents('css/pdf/find.css'),
            'fontDir' => array_merge($fontDirs, [
                __DIR__ . '/../web/fonts',
            ]),
            'fontdata' => $fontData + [
                    'method' => [
                        'R' => 'Method.ttf',
                        'I' => 'Method.ttf',
                    ],
                    'propisi' => [
                        'R' => 'Propisi.ttf',
                        'I' => 'Propisi.ttf',
                    ]
                ],
            'default_font' => 'method'
        ]);
        $pdf->WriteHTML($content);

        return $pdf->Output();
    }

    /**
     * @param int $id
     * @return mixed
     */
    public function actionSendToEmail($id)
    {
       $model = $this->findModel($id);
       $user = $model->user;

       if($model->pdf_file != null){
           $result = Yii::$app->mailer->compose()
               ->setFrom('logoped.service@mail.ru')
               ->setTo($user->login)
               ->setSubject('Пособие')
               ->setHtmlBody('Пособие «Найди букву»')
               ->attach($model->pdf_file)
               ->send();

           if($result){
               Yii::$app->session->setFlash('success', 'Успешно отправлено');
           } else {
               Yii::$app->session->setFlash('error', 'Во время отправки произошла ошибка');
           }
       } else {
           Yii::$app->session->setFlash('error', 'Файл не найден');
       }

       return $this->redirect(['index']);
    }

    /**
     * Updates an existing Messages model.
     * For ajax request will return json object
     * and for non-ajax request if update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> "Изменить пособие #".$id,
                    'content'=>$this->renderAjax('update', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Отмена',['class'=>'btn btn-primary pull-left','data-dismiss'=>"modal"]).
                        Html::button('Сохранить',['class'=>'btn btn-info','type'=>"submit"])
                ];
            }else if($model->load($request->post()) && $model->save()){

                $this->saveLetterData($model);

                return [
                    'forceReload'=>'#crud-datatable-pjax',
                    'title'=> "Пособие #".$id,
                    'content'=>$this->renderAjax('view', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Отмена',['class'=>'btn btn-primary pull-left','data-dismiss'=>"modal"]).
                        Html::a('Изменить',['update','id'=>$id],['class'=>'btn btn-warning','role'=>'modal-remote'])
                ];
            }else{
                return [
                    'title'=> "Изменить пособие #".$id,
                    'content'=>$this->renderAjax('update', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Отмена',['class'=>'btn btn-primary pull-left','data-dismiss'=>"modal"]).
                        Html::button('Сохранить',['class'=>'btn btn-info','type'=>"submit"])
                ];
            }
        }else{
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('update', [
                    'model' => $model,
                ]);
            }
        }
    }

    /**
     * Delete an existing FindLetter model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $request = Yii::$app->request;
        $this->findModel($id)->delete();

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose'=>true,'forceReload'=>'#crud-datatable-pjax'];
        }else{
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }


    }

     /**
     * Delete multiple existing FindLetter model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionBulkDelete()
    {        
        $request = Yii::$app->request;
        $pks = explode(',', $request->post( 'pks' )); // Array or selected records primary keys
        foreach ( $pks as $pk ) {
            $model = $this->findModel($pk);
            $model->delete();
        }

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose'=>true,'forceReload'=>'#crud-datatable-pjax'];
        }else{
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }
       
    }

    /**
     * @param \app\models\FindLetter $model
     */
    protected function saveLetterData($model)
    {
        $printUpData = ArrayHelper::map(FindLetterData::find()->where(['find_letter_id' => $model->id, 'type' => FindLetterData::TYPE_PRINT_UP])->all(), 'symbol', 'id');
        $printLowData = ArrayHelper::map(FindLetterData::find()->where(['find_letter_id' => $model->id, 'type' => FindLetterData::TYPE_PRINT_LOW])->all(), 'symbol', 'id');
        $recipeUpData = ArrayHelper::map(FindLetterData::find()->where(['find_letter_id' => $model->id, 'type' => FindLetterData::TYPE_RECIPE_UP])->all(), 'symbol', 'id');
        $recipeLowData = ArrayHelper::map(FindLetterData::find()->where(['find_letter_id' => $model->id, 'type' => FindLetterData::TYPE_RECIPE_LOW])->all(), 'symbol', 'id');



        foreach ($model->printUp as $key => $value){
            $symbol = $model->smallLetterlist[$key];
            if(isset($printUpData[$symbol])){
                if($value == null){
                    FindLetterData::deleteAll(['id' => $printUpData[$symbol]]);
                } else {
                    FindLetterData::updateAll(['amount' => $value], ['id' => $printUpData[$symbol]]);
                }
            } else {
                if($value != null){
                    $data = new FindLetterData([
                        'find_letter_id' => $model->id,
                        'symbol' => $symbol,
                        'type' => FindLetterData::TYPE_PRINT_UP,
                        'amount' => $value,
                    ]);
                    $result = $data->save(false);
                }
            }
        }

        foreach ($model->printLow as $key => $value){
            $symbol = $model->smallLetterlist[$key];
            if(isset($printLowData[$symbol])){
                if($value == null){
                    FindLetterData::deleteAll(['id' => $printLowData[$symbol]]);
                } else {
                    FindLetterData::updateAll(['amount' => $value], ['id' => $printLowData[$symbol]]);
                }
            } else {
                if($value != null){
                    $data = new FindLetterData([
                        'find_letter_id' => $model->id,
                        'symbol' => $symbol,
                        'type' => FindLetterData::TYPE_PRINT_LOW,
                        'amount' => $value,
                    ]);
                    $result = $data->save(false);
                }
            }
        }

        foreach ($model->recipeUp as $key => $value){
            $symbol = $model->smallLetterlist[$key];
            if(isset($recipeUpData[$symbol])){
                if($value == null){
                    FindLetterData::deleteAll(['id' => $recipeUpData[$symbol]]);
                } else {
                    FindLetterData::updateAll(['amount' => $value], ['id' => $recipeUpData[$symbol]]);
                }
            } else {
                if($value != null){
                    $data = new FindLetterData([
                        'find_letter_id' => $model->id,
                        'symbol' => $symbol,
                        'type' => FindLetterData::TYPE_RECIPE_UP,
                        'amount' => $value,
                    ]);
                    $result = $data->save(false);
                }
            }
        }

        foreach ($model->recipeLow as $key => $value){
            $symbol = $model->smallLetterlist[$key];
            if(isset($recipeLowData[$symbol])){
                if($value == null){
                    FindLetterData::deleteAll(['id' => $recipeLowData[$symbol]]);
                } else {
                    FindLetterData::updateAll(['amount' => $value], ['id' => $recipeLowData[$symbol]]);
                }
            } else {
                if($value != null){
                    $data = new FindLetterData([
                        'find_letter_id' => $model->id,
                        'symbol' => $symbol,
                        'type' => FindLetterData::TYPE_RECIPE_LOW,
                        'amount' => $value,
                    ]);
                    $result = $data->save(false);
                }
            }
        }
    }

    /**
     * Finds the FindLetter model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return FindLetter the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = FindLetter::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
