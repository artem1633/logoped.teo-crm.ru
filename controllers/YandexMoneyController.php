<?php

namespace app\controllers;

use app\behaviors\UpdateOnlineBehavior;
use app\models\Companies;
use app\models\CompanySettings;
//use Faker\Provider\Company;
use app\models\Statuses;
use app\models\statuses\StatusesSearch;
use app\models\YandexMoney;
use Yii;
use yii\web\Controller;
use app\models\Settings;
use yii\web\NotFoundHttpException;

/**
 * SettingsController implements the CRUD actions for Users model.
 */
class YandexMoneyController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'online' => [
                'class' => UpdateOnlineBehavior::class
            ],
            'access' => [
                'class' => \yii\filters\AccessControl::class,
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    /**
     * Отображет главную страницу настроек
     * @return mixed
     */
    public function actionIndex()
    {
        if (!Yii::$app->user->identity->isSuperAdmin()) {
           $this->goHome();
        }
        $configure = $this->findModel(1);

        if (Yii::$app->request->post() && $configure->load(Yii::$app->request->post())) {
            $configure->save();
        }

        return $this->render('index', [
            'configure' => $configure,
        ]);
    }

    /**
     * @param $id
     * @return null|static
     * @throws NotFoundHttpException
     */
    protected function findModel($id)
    {
        if (($model = YandexMoney::findOne($id)) !== null) {
            return $model;
        } else {
            return new YandexMoney();
        }
    }

}